#! /bin/sh
echo ------------------------------------------------------
echo 'Launching jobs YAY!'
echo ------------------------------------------------------

while read channel; do
	while read charge; do
	echo ------------------------------------------------------
	echo 'Running on Systematic named' $syst_dir
	echo ------------------------------------------------------
	syst_dir="nominal"
	echo ------------------------------------------------------
	echo 'Running on Channel named' $channel
	echo ------------------------------------------------------
	echo ------------------------------------------------------
	echo 'Running on Channel named' $charge
	echo ------------------------------------------------------
	#nohup time run_Looper 91 data_MC16a/AllYear_15.txt 1 0 $syst_dir $channel&>log/log_DATA91&
	nohup time run_Looper 92 data_MC16a/AllYear_16.txt 1 0 $syst_dir $channel $charge&>log/log_DATA92&
	nohup time run_Looper 93 data_MC16d/AllYear_17.txt 0 1 $syst_dir $channel $charge&>log/log_DATA93&
	done <charge.list
done <channel.list
