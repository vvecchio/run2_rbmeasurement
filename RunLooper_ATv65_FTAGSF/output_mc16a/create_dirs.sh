echo '------ NOMINAL DIR ------'
mkdir nominal
cd nominal
mkdir sampleType_1 sampleType_12 sampleType_13 sampleType_14 sampleType_15 sampleType_2 sampleType_3 sampleType_40 sampleType_41 sampleType_42 sampleType_5 sampleType_6 sampleType_91 sampleType_92 sampleType_93 sampleType_94 sampleType_21 sampleType_22
cd ..

echo '------ ISR/FSR DIR ------'
mkdir RadHi
cd RadHi
mkdir sampleType_2 sampleType_16
cd ..
mkdir RadLo
cd RadLo
mkdir sampleType_2 sampleType_1
cd ..
mkdir FSRHi
cd FSRHi
mkdir sampleType_2 sampleType_1
cd ..
mkdir FSRLo
cd FSRLo
mkdir sampleType_2 sampleType_1
cd ..


echo '------ SYST WEIGHTDIR ------'
while read syst_dir; do
  mkdir $syst_dir
  echo '--- Making syst dir '$syst_dir '-----'
  cd $syst_dir
  mkdir sampleType_1 sampleType_2 sampleType_3 sampleType_40 sampleType_41 sampleType_42 sampleType_5 sampleType_6
  cd ..
done<syst_list.list

echo '------ SYST WEIGHT DIR ------'
while read syst_dir; do
  mkdir $syst_dir
  echo '--- Making syst dir '$syst_dir '-----'
  cd $syst_dir
  mkdir sampleType_1 sampleType_2 sampleType_3 sampleType_40 sampleType_41 sampleType_42 sampleType_5 sampleType_6
  cd ..
done<syst_weight.list

while read syst_dir; do
  mkdir $syst_dir
  echo '--- Making syst dir '$syst_dir '-----'
  cd $syst_dir
  mkdir sampleType_1
  cd ..
done<PFDERRORSET.list
