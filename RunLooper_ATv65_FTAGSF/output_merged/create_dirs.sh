echo '------ NOMINAL DIR ------'
mkdir nominal

echo '------ ISR/FSR DIR ------'
mkdir RadHi
mkdir RadLo
mkdir FSRHi
mkdir FSRLo

echo '------ SYST WEIGHTDIR ------'
while read syst_dir; do
  mkdir $syst_dir
  echo '--- Making syst dir '$syst_dir '-----'
done<syst_list.list

echo '------ SYST WEIGHT DIR ------'
while read syst_dir; do
  mkdir $syst_dir
  echo '--- Making syst dir '$syst_dir '-----'
done<syst_weight.list

while read syst_dir; do
  mkdir $syst_dir
  echo '--- Making syst dir '$syst_dir '-----'
done<PFDERRORSET.list
