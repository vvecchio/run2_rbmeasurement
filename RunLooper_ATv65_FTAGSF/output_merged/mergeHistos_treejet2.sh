echo ------------------------------------------------------
echo 'Merging simulation tree JET2 syst'
echo ------------------------------------------------------
while read syst_dir; do
hadd -f $syst_dir/output_1.root  ../output_mc16a/$syst_dir/sampleType_1/*.root ../output_mc16d/$syst_dir/sampleType_1/*.root ../output_mc16e/$syst_dir/sampleType_1/*.root
hadd -f $syst_dir/output_2.root  ../output_mc16a/$syst_dir/sampleType_2/*.root ../output_mc16d/$syst_dir/sampleType_2/*.root ../output_mc16e/$syst_dir/sampleType_2/*.root
hadd -f $syst_dir/output_3.root  ../output_mc16a/$syst_dir/sampleType_3/*.root  ../output_mc16d/$syst_dir/sampleType_3/*.root ../output_mc16e/$syst_dir/sampleType_3/*.root
hadd -f $syst_dir/output_40.root ../output_mc16a/$syst_dir/sampleType_40/*.root ../output_mc16d/$syst_dir/sampleType_40/*.root ../output_mc16e/$syst_dir/sampleType_40/*.root
hadd -f $syst_dir/output_41.root ../output_mc16a/$syst_dir/sampleType_41/*.root ../output_mc16d/$syst_dir/sampleType_41/*.root ../output_mc16e/$syst_dir/sampleType_41/*.root
hadd -f $syst_dir/output_42.root ../output_mc16a/$syst_dir/sampleType_42/*.root ../output_mc16d/$syst_dir/sampleType_42/*.root ../output_mc16e/$syst_dir/sampleType_42/*.root
hadd -f $syst_dir/output_5.root  ../output_mc16a/$syst_dir/sampleType_5/*.root  ../output_mc16d/$syst_dir/sampleType_5/*.root ../output_mc16e/$syst_dir/sampleType_5/*.root
hadd -f $syst_dir/output_6.root  ../output_mc16a/$syst_dir/sampleType_6/*.root  ../output_mc16d/$syst_dir/sampleType_6/*.root ../output_mc16e/$syst_dir/sampleType_6/*.root
hadd -f $syst_dir/output_Vjets.root $syst_dir/output_40.root $syst_dir/output_41.root $syst_dir/output_42.root $syst_dir/output_5.root 
done<LISTS/syst_treejet2.list
