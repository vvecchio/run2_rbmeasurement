echo ------------------------------------------------------
echo 'Merging ttbar PDF List2'
echo ------------------------------------------------------
while read syst_dir; do
	hadd -f $syst_dir/output_1.root  ../output_mc16a/$syst_dir/sampleType_1/*.root ../output_mc16d/$syst_dir/sampleType_1/*.root ../output_mc16e/$syst_dir/sampleType_1/*.root
done<LISTS/PFDERRORSET_list2_err
