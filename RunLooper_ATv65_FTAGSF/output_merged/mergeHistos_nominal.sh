echo ------------------------------------------------------
echo 'Merging simulation nominal'
echo ------------------------------------------------------
syst_dir='nominal'
hadd -f $syst_dir/output_1.root  ../output_mc16a/$syst_dir/sampleType_1/*.root ../output_mc16d/$syst_dir/sampleType_1/*.root ../output_mc16e/$syst_dir/sampleType_1/*.root
hadd -f $syst_dir/output_13.root  ../output_mc16a/$syst_dir/sampleType_13/*.root ../output_mc16d/$syst_dir/sampleType_13/*.root ../output_mc16e/$syst_dir/sampleType_13/*.root
hadd -f $syst_dir/output_2.root  ../output_mc16a/$syst_dir/sampleType_2/*.root ../output_mc16d/$syst_dir/sampleType_2/*.root ../output_mc16e/$syst_dir/sampleType_2/*.root
hadd -f $syst_dir/output_21.root  ../output_mc16a/$syst_dir/sampleType_21/*.root ../output_mc16d/$syst_dir/sampleType_21/*.root ../output_mc16e/$syst_dir/sampleType_21/*.root
hadd -f $syst_dir/output_22.root  ../output_mc16a/$syst_dir/sampleType_22/*.root ../output_mc16d/$syst_dir/sampleType_22/*.root ../output_mc16e/$syst_dir/sampleType_22/*.root
hadd -f $syst_dir/output_3.root  ../output_mc16a/$syst_dir/sampleType_3/*.root  ../output_mc16d/$syst_dir/sampleType_3/*.root ../output_mc16e/$syst_dir/sampleType_3/*.root
hadd -f $syst_dir/output_40.root ../output_mc16a/$syst_dir/sampleType_40/*.root ../output_mc16d/$syst_dir/sampleType_40/*.root ../output_mc16e/$syst_dir/sampleType_40/*.root
hadd -f $syst_dir/output_41.root ../output_mc16a/$syst_dir/sampleType_41/*.root ../output_mc16d/$syst_dir/sampleType_41/*.root ../output_mc16e/$syst_dir/sampleType_41/*.root
hadd -f $syst_dir/output_42.root ../output_mc16a/$syst_dir/sampleType_42/*.root ../output_mc16d/$syst_dir/sampleType_42/*.root ../output_mc16e/$syst_dir/sampleType_42/*.root
hadd -f $syst_dir/output_5.root  ../output_mc16a/$syst_dir/sampleType_5/*.root  ../output_mc16d/$syst_dir/sampleType_5/*.root ../output_mc16e/$syst_dir/sampleType_5/*.root
hadd -f $syst_dir/output_6.root  ../output_mc16a/$syst_dir/sampleType_6/*.root  ../output_mc16d/$syst_dir/sampleType_6/*.root ../output_mc16e/$syst_dir/sampleType_6/*.root
hadd -f $syst_dir/output_Vjets.root $syst_dir/output_40.root $syst_dir/output_41.root $syst_dir/output_42.root $syst_dir/output_5.root 

hadd -f $syst_dir/output_MC.root $syst_dir/output_1.root $syst_dir/output_2.root $syst_dir/output_3.root $syst_dir/output_40.root $syst_dir/output_41.root $syst_dir/output_42.root $syst_dir/output_5.root $syst_dir/output_6.root
hadd -f $syst_dir/output_BKG.root $syst_dir/output_2.root $syst_dir/output_3.root $syst_dir/output_40.root $syst_dir/output_41.root $syst_dir/output_42.root $syst_dir/output_5.root $syst_dir/output_6.root

hadd -f $syst_dir/output_data.root ../output_mc16a/$syst_dir/sampleType_91/*.root ../output_mc16a/$syst_dir/sampleType_92/*.root ../output_mc16d/$syst_dir/sampleType_93/*.root ../output_mc16e/$syst_dir/sampleType_94/*.root
