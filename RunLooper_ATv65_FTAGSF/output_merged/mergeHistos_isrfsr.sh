echo ------------------------------------------------------
echo 'Merging ttbar hdamp=3mtop RadHi syst'
echo ------------------------------------------------------
syst_dir_RadHi="RadHi"
hadd -f $syst_dir_RadHi/output_16.root  ../output_mc16a/$syst_dir_RadHi/sampleType_16/output_*.root ../output_mc16d/$syst_dir_RadHi/sampleType_16/output_*.root ../output_mc16e/$syst_dir_RadHi/sampleType_16/output_*.root
hadd -f $syst_dir_RadHi/output_2.root  ../output_mc16a/$syst_dir_RadHi/sampleType_2/output_*.root ../output_mc16d/$syst_dir_RadHi/sampleType_2/output_*.root ../output_mc16e/$syst_dir_RadHi/sampleType_2/output_*.root
echo ------------------------------------------------------
echo 'Merging ttbar nominal modeling syst'
echo ------------------------------------------------------
while read syst_dir_ttbar; do
echo $syst_dir_ttbar
hadd -f $syst_dir_ttbar/output_1.root  ../output_mc16a/$syst_dir_ttbar/sampleType_1/output_*.root ../output_mc16d/$syst_dir_ttbar/sampleType_1/output_*.root ../output_mc16e/$syst_dir_ttbar/sampleType_1/output_*.root
hadd -f $syst_dir_ttbar/output_2.root  ../output_mc16a/$syst_dir_ttbar/sampleType_2/output_*.root ../output_mc16d/$syst_dir_ttbar/sampleType_2/output_*.root ../output_mc16e/$syst_dir_ttbar/sampleType_2/output_*.root
done<LISTS/ttbar_var.list
