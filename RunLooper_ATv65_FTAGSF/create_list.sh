echo '---------------------------------'
echo 'Creating list for MC16a'

echo '--- ttbar'
cp data_MC16a/410472 data_MC16a/list_ttbar.txt

echo '--- singletop'
cat data_MC16a/410658 data_MC16a/410659 data_MC16a/410648 data_MC16a/410649 >>data_MC16a/list_singletop.txt

echo '--- diboson'
cat data_MC16a/364250 data_MC16a/364253 data_MC16a/364254 data_MC16a/364255 data_MC16a/364283 data_MC16a/364284 data_MC16a/364285 data_MC16a/364287 data_MC16a/364288 data_MC16a/364289 data_MC16a/364290 data_MC16a/345705 data_MC16a/345706  data_MC16a/345723 >> data_MC16a/list_diboson.txt

echo '--- Ztt'
cat data_MC16a/364128 data_MC16a/364130 data_MC16a/364131 data_MC16a/364132 data_MC16a/364133 data_MC16a/364134 data_MC16a/364135 data_MC16a/364136 data_MC16a/364137 data_MC16a/364138 data_MC16a/364139 data_MC16a/364140 data_MC16a/364141 data_MC16a/364210 data_MC16a/364211 data_MC16a/364212 data_MC16a/364213 data_MC16a/364214 data_MC16a/364215 data_MC16a/364129 >>data_MC16a/list_Ztt.txt

echo '--- Zee'
cat data_MC16a/364114 data_MC16a/364115 data_MC16a/364116 data_MC16a/364117 data_MC16a/364118 data_MC16a/364119 data_MC16a/364120 data_MC16a/364121 data_MC16a/364122 data_MC16a/364123 data_MC16a/364125 data_MC16a/364126 data_MC16a/364127 data_MC16a/364204 data_MC16a/364205 data_MC16a/364206 data_MC16a/364207 data_MC16a/364208 data_MC16a/364209 >>data_MC16a/list_Zee.txt

echo '--- Zmm'
cat data_MC16a/364100 data_MC16a/364101 data_MC16a/364102 data_MC16a/364103 data_MC16a/364104 data_MC16a/364105 data_MC16a/364106 data_MC16a/364107 data_MC16a/364108 data_MC16a/364109 data_MC16a/364110 data_MC16a/364111 data_MC16a/364112 data_MC16a/364113 data_MC16a/364198 data_MC16a/364199 data_MC16a/364200 data_MC16a/364201 data_MC16a/364202 data_MC16a/364203 >>data_MC16a/list_Zmm.txt

echo '--- Wjets'
cat data_MC16a/364170 data_MC16a/364171 data_MC16a/364172 data_MC16a/364173 data_MC16a/364175 data_MC16a/364176 data_MC16a/364177 data_MC16a/364178 data_MC16a/364179 data_MC16a/364181 data_MC16a/364182 data_MC16a/364183 data_MC16a/364156 data_MC16a/364157 data_MC16a/364158 data_MC16a/364159 data_MC16a/364160 data_MC16a/364161 data_MC16a/364162 data_MC16a/364163 data_MC16a/364164 data_MC16a/364165 data_MC16a/364166 data_MC16a/364167 data_MC16a/364168 data_MC16a/364184 data_MC16a/364185 data_MC16a/364186 data_MC16a/364187 data_MC16a/364188 data_MC16a/364189 data_MC16a/364190 data_MC16a/364191 data_MC16a/364192 data_MC16a/364193 data_MC16a/364194 data_MC16a/364196 data_MC16a/364197 >>data_MC16a/list_Wjets.txt

echo '--- tt+X'
cat data_MC16a/345940 data_MC16a/410155 data_MC16a/410156 data_MC16a/410157 data_MC16a/410218 data_MC16a/410219 data_MC16a/410220>> data_MC16a/list_ttX.txt
#cat data_MC16a/410491 data_MC16a/410492>>data_MC16a/list_ttbarVts.txt

echo '---------------------------------'
echo 'Creating list for MC16d'

echo '--- ttbar'
cp data_MC16d/410472 data_MC16d/list_ttbar.txt

echo '--- singletop'
cat data_MC16d/410658 data_MC16d/410659 data_MC16d/410648 data_MC16d/410649 >>data_MC16d/list_singletop.txt

echo '--- diboson'
cat data_MC16d/364250 data_MC16d/364253 data_MC16d/364254 data_MC16d/364255 data_MC16d/364283 data_MC16d/364284 data_MC16d/364285 data_MC16d/364287 data_MC16d/364288 data_MC16d/364289 data_MC16d/364290 data_MC16d/345705 data_MC16d/345706  data_MC16d/345723 >> data_MC16d/list_diboson.txt

echo '--- Ztt'
cat data_MC16d/364128 data_MC16d/364130 data_MC16d/364131 data_MC16d/364132 data_MC16d/364133 data_MC16d/364134 data_MC16d/364135 data_MC16d/364136 data_MC16d/364137 data_MC16d/364138 data_MC16d/364139 data_MC16d/364140 data_MC16d/364141 data_MC16d/364210 data_MC16d/364211 data_MC16d/364212 data_MC16d/364213 data_MC16d/364214 data_MC16d/364215 data_MC16d/364129 >>data_MC16d/list_Ztt.txt

echo '--- Zee'
cat data_MC16d/364114 data_MC16d/364115 data_MC16d/364116 data_MC16d/364117 data_MC16d/364118 data_MC16d/364119 data_MC16d/364120 data_MC16d/364121 data_MC16d/364122 data_MC16d/364123 data_MC16d/364125 data_MC16d/364126 data_MC16d/364127 data_MC16d/364204 data_MC16d/364205 data_MC16d/364206 data_MC16d/364207 data_MC16d/364208 data_MC16d/364209 >>data_MC16d/list_Zee.txt

echo '--- Zmm'
cat data_MC16d/364100 data_MC16d/364101 data_MC16d/364102 data_MC16d/364103 data_MC16d/364104 data_MC16d/364105 data_MC16d/364106 data_MC16d/364107 data_MC16d/364108 data_MC16d/364109 data_MC16d/364110 data_MC16d/364111 data_MC16d/364112 data_MC16d/364113 data_MC16d/364198 data_MC16d/364199 data_MC16d/364200 data_MC16d/364201 data_MC16d/364202 data_MC16d/364203 >>data_MC16d/list_Zmm.txt

echo '--- Wjets'
cat data_MC16d/364170 data_MC16d/364171 data_MC16d/364172 data_MC16d/364173 data_MC16d/364175 data_MC16d/364176 data_MC16d/364177 data_MC16d/364178 data_MC16d/364179 data_MC16d/364181 data_MC16d/364182 data_MC16d/364183 data_MC16d/364156 data_MC16d/364157 data_MC16d/364158 data_MC16d/364159 data_MC16d/364160 data_MC16d/364161 data_MC16d/364162 data_MC16d/364163 data_MC16d/364164 data_MC16d/364165 data_MC16d/364166 data_MC16d/364167 data_MC16d/364168 data_MC16d/364184 data_MC16d/364185 data_MC16d/364186 data_MC16d/364187 data_MC16d/364188 data_MC16d/364189 data_MC16d/364190 data_MC16d/364191 data_MC16d/364192 data_MC16d/364193 data_MC16d/364194 data_MC16d/364196 data_MC16d/364197 >>data_MC16d/list_Wjets.txt

echo '--- tt+X'
cat data_MC16d/345940 data_MC16d/410155 data_MC16d/410156 data_MC16d/410157 data_MC16d/410218 data_MC16d/410219 data_MC16d/410220>> data_MC16d/list_ttX.txt
#cat data_MC16d/410491 data_MC16d/410492>>data_MC16d/list_ttbarVts.txt

echo '---------------------------------'
echo 'Creating list for MC16e'

echo '--- ttbar'
cp data_MC16e/410472 data_MC16e/list_ttbar.txt

echo '--- singletop'
cat data_MC16e/410658 data_MC16e/410659 data_MC16e/410648 data_MC16e/410649 >>data_MC16e/list_singletop.txt

echo '--- diboson'
cat data_MC16e/364250 data_MC16e/364253 data_MC16e/364254 data_MC16e/364255 data_MC16e/364283 data_MC16e/364284 data_MC16e/364285 data_MC16e/364287 data_MC16e/364288 data_MC16e/364289 data_MC16e/364290 data_MC16e/345705 data_MC16e/345706  data_MC16e/345723 >> data_MC16e/list_diboson.txt

echo '--- Ztt'
cat data_MC16e/364128 data_MC16e/364130 data_MC16e/364131 data_MC16e/364132 data_MC16e/364133 data_MC16e/364134 data_MC16e/364135 data_MC16e/364136 data_MC16e/364137 data_MC16e/364138 data_MC16e/364139 data_MC16e/364140 data_MC16e/364141 data_MC16e/364210 data_MC16e/364211 data_MC16e/364212 data_MC16e/364213 data_MC16e/364214 data_MC16e/364215 data_MC16e/364129 >>data_MC16e/list_Ztt.txt

echo '--- Zee'
cat data_MC16e/364114 data_MC16e/364115 data_MC16e/364116 data_MC16e/364117 data_MC16e/364118 data_MC16e/364119 data_MC16e/364120 data_MC16e/364121 data_MC16e/364122 data_MC16e/364123 data_MC16e/364125 data_MC16e/364126 data_MC16e/364127 data_MC16e/364204 data_MC16e/364205 data_MC16e/364206 data_MC16e/364207 data_MC16e/364208 data_MC16e/364209 >>data_MC16e/list_Zee.txt

echo '--- Zmm'
cat data_MC16e/364100 data_MC16e/364101 data_MC16e/364102 data_MC16e/364103 data_MC16e/364104 data_MC16e/364105 data_MC16e/364106 data_MC16e/364107 data_MC16e/364108 data_MC16e/364109 data_MC16e/364110 data_MC16e/364111 data_MC16e/364112 data_MC16e/364113 data_MC16e/364198 data_MC16e/364199 data_MC16e/364200 data_MC16e/364201 data_MC16e/364202 data_MC16e/364203 >>data_MC16e/list_Zmm.txt

echo '--- Wjets'
cat data_MC16e/364170 data_MC16e/364171 data_MC16e/364172 data_MC16e/364173 data_MC16e/364175 data_MC16e/364176 data_MC16e/364177 data_MC16e/364178 data_MC16e/364179 data_MC16e/364181 data_MC16e/364182 data_MC16e/364183 data_MC16e/364156 data_MC16e/364157 data_MC16e/364158 data_MC16e/364159 data_MC16e/364160 data_MC16e/364161 data_MC16e/364162 data_MC16e/364163 data_MC16e/364164 data_MC16e/364165 data_MC16e/364166 data_MC16e/364167 data_MC16e/364168 data_MC16e/364184 data_MC16e/364185 data_MC16e/364186 data_MC16e/364187 data_MC16e/364188 data_MC16e/364189 data_MC16e/364190 data_MC16e/364191 data_MC16e/364192 data_MC16e/364193 data_MC16e/364194 data_MC16e/364196 data_MC16e/364197 >>data_MC16e/list_Wjets.txt

echo '--- tt+X'
cat data_MC16e/345940 data_MC16e/410155 data_MC16e/410156 data_MC16e/410157 data_MC16e/410218 data_MC16e/410219 data_MC16e/410220>> data_MC16e/list_ttX.txt
#cat data_MC16e/410491 data_MC16e/410492>>data_MC16e/list_ttbarVts.txt
