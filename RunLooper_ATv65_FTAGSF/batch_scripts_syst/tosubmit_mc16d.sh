#!/bin/bash
#    Job name (default is name of pbs script file)
#PBS -N RB_syst_mc16d
#
#    Resource limits: max. wall clock time during which job can be running
#PBS -l walltime=25:00:00
#
#          The directive below directs that the standard output and
#          error streams are to be merged, intermixed, as standard
#          output. 
#PBS -j oe
#
##########################################

echo 'Job is running on node(s): '
cat $PBS_NODEFILE
echo ------------------------------------------------------
echo PBS: qsub is running on $PBS_O_HOST
echo PBS: executing queue is $PBS_QUEUE
echo PBS: execution mode is $PBS_ENVIRONMENT
echo PBS: job identifier is $PBS_JOBID
echo PBS: job name is $PBS_JOBNAME
echo PBS: current home directory is $PBS_O_HOME
echo PBS: PATH = $PBS_O_PATH
echo ------------------------------------------------------

echo ------------------------------------------------------
echo 'Input syst tree name'
echo ------------------------------------------------------
echo ${SYSTTREE}

WORKDIR=/storage/local/home/atlas/vvecchio/UserArea/CODE_TOPRB/RbMeaurement/RunLooper
cd ${WORKDIR};
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
setupATLAS
cd ../build
asetup --restore
cd ../
source Setup.sh
cd ${WORKDIR};


run_Looper 1  data_MC16d/list_ttbar.txt 0 1 $SYSTTREE
#run_Looper 2  data_MC16d/list_singletop.txt 0 1 $SYSTTREE
#run_Looper 3  data_MC16d/list_diboson.txt 0 1 $SYSTTREE
#run_Looper 40 data_MC16d/list_Zee.txt 0 1 $SYSTTREE
#run_Looper 41 data_MC16d/list_Zmm.txt 0 1 $SYSTTREE
#run_Looper 42 data_MC16d/list_Ztt.txt 0 1 $SYSTTREE
#run_Looper 5  data_MC16d/list_Wjets.txt 0 1 $SYSTTREE
#run_Looper 6  data_MC16d/list_ttX.txt 0 1 $SYSTTREE
