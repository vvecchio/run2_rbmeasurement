#!/bin/bash
#    Job name (default is name of pbs script file)
#PBS -N RB_syst_mc16a
#
#    Resource limits: max. wall clock time during which job can be running
#PBS -l walltime=10:00:00
#
#          The directive below directs that the standard output and
#          error streams are to be merged, intermixed, as standard
#          output. 
#PBS -j oe
#
##########################################

echo 'Job is running on node(s): '
cat $PBS_NODEFILE
echo ------------------------------------------------------
echo PBS: qsub is running on $PBS_O_HOST
echo PBS: executing queue is $PBS_QUEUE
echo PBS: execution mode is $PBS_ENVIRONMENT
echo PBS: job identifier is $PBS_JOBID
echo PBS: job name is $PBS_JOBNAME
echo PBS: current home directory is $PBS_O_HOME
echo PBS: PATH = $PBS_O_PATH
echo ------------------------------------------------------

echo ------------------------------------------------------
echo 'Input syst tree name'
echo ------------------------------------------------------
echo ${SYSTTREE}

echo ------------------------------------------------------
echo 'Input channel name'
echo ------------------------------------------------------
echo ${CHANNEL}
echo ------------------------------------------------------
echo 'Input charge name'
echo ------------------------------------------------------
echo ${CHARGE}

WORKDIR=/storage/local/home/atlas/vvecchio/UserArea/CODE_TOPRB/RbMeaurement/RunLooper_ATv65_FTAGSF
cd ${WORKDIR};
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
setupATLAS
cd ../build
asetup --restore
cd ../
source Setup.sh
cd ${WORKDIR};


run_Looper 14  data_MC16a/list_ttbarVts.txt 1 0 $SYSTTREE $CHANNEL $CHARGE
