#include "TF1.h"
#include "TLegend.h"
#include "TFitResult.h"
#include "TH1F.h"
#include "TMath.h"
#include "TFile.h"
#include "TGraphErrors.h"
#include "TMultiGraph.h"
#include "TCanvas.h"
#include "TLatex.h"
#include <iostream>
#include <sstream>
using namespace std;
bool isMC16a = false;
int GetTotaEventsWeighted_ttbarvar(int dsid);
double GetTotaEventsWeighted(int dsid);

void TotalEventsWeighted() {

//double EventsWeighted = GetTotaEventsWeighted(410472);
//cerr<<"EventsWeighted	"<<EventsWeighted<<endl;
//double EventsWeighted_RadHi = GetTotaEventsWeighted_ttbarvar(410482,"RadHi");
//cerr<<"EventsWeighted_RadHi	"<<EventsWeighted_RadHi<<endl;
int EventsWeighted_RadLo = GetTotaEventsWeighted_ttbarvar(410472);
//cerr<<"ttbar variation run on "<<EventsWeighted_RadLo<<" root files"<<endl;

}

double GetTotaEventsWeighted(int dsid){

  double sampleNEvt = 0.;
  int numero_roots = 0;

  string string_dsid = std::to_string(dsid);
  TString name_list_dsid;
  if(isMC16a) name_list_dsid = "data_MC16a/"+string_dsid+".txt";
  else name_list_dsid = "data_MC16d/"+string_dsid+".txt";

  ifstream filein(name_list_dsid);
  if(!filein) { cout << "error opening input file " <<endl;
  } else {
    while(filein.good()){
    TString name_root;
    filein>>name_root;
    if(name_root=="") break;
    numero_roots++;
   
     TFile *xsecf = TFile::Open(name_root);  
     TTree *totnorm = (TTree*)xsecf->Get("sumWeights");
     Float_t totalEventsWeighted;
     totnorm->SetBranchAddress("totalEventsWeighted",&totalEventsWeighted);
               
     for (int ent = 0; ent < totnorm->GetEntries(); ent++){
     totnorm->GetEntry(ent);
     sampleNEvt += totalEventsWeighted;
     }
     delete totnorm;
     xsecf->Close();
    }
  }
 
  cerr<<"numero_roots	"<<numero_roots<<endl;
  filein.close();

return sampleNEvt;
}

int GetTotaEventsWeighted_ttbarvar(int dsid){

  double sampleNEvt_RadLo = 0.;
  double sampleNEvt_FSRHi = 0.;
  double sampleNEvt_FSRLo = 0.;
  double sampleNEvt_PDF_90900 = 0;
  double sampleNEvt_PDF_90901 = 0;
  double sampleNEvt_PDF_90902 = 0;
  double sampleNEvt_PDF_90903 = 0;
  double sampleNEvt_PDF_90904 = 0;
  double sampleNEvt_PDF_90905 = 0;
  double sampleNEvt_PDF_90906 = 0;
  double sampleNEvt_PDF_90907 = 0;
  double sampleNEvt_PDF_90908 = 0;
  double sampleNEvt_PDF_90909 = 0;
  double sampleNEvt_PDF_90910 = 0;
  double sampleNEvt_PDF_90911 = 0;
  double sampleNEvt_PDF_90912 = 0;
  double sampleNEvt_PDF_90913 = 0;
  double sampleNEvt_PDF_90914 = 0;
  double sampleNEvt_PDF_90915 = 0;
  double sampleNEvt_PDF_90916 = 0;
  double sampleNEvt_PDF_90917 = 0;
  double sampleNEvt_PDF_90918 = 0;
  double sampleNEvt_PDF_90919 = 0;
  double sampleNEvt_PDF_90920 = 0;
  double sampleNEvt_PDF_90921 = 0;
  double sampleNEvt_PDF_90922 = 0;
  double sampleNEvt_PDF_90923 = 0;
  double sampleNEvt_PDF_90924 = 0;
  double sampleNEvt_PDF_90925 = 0;
  double sampleNEvt_PDF_90926 = 0;
  double sampleNEvt_PDF_90927 = 0;
  double sampleNEvt_PDF_90928 = 0;
  double sampleNEvt_PDF_90929 = 0;
  double sampleNEvt_PDF_90930 = 0;
  double sampleNEvt_PDF_260001 = 0;
  double sampleNEvt_PDF_260002 = 0;
  double sampleNEvt_PDF_260003 = 0;
  double sampleNEvt_PDF_260004 = 0;
  double sampleNEvt_PDF_260005 = 0;
  double sampleNEvt_PDF_260006 = 0;
  double sampleNEvt_PDF_260007 = 0;
  double sampleNEvt_PDF_260008 = 0;
  double sampleNEvt_PDF_260009 = 0;
  double sampleNEvt_PDF_260010 = 0;
  double sampleNEvt_PDF_260011 = 0;
  double sampleNEvt_PDF_260012 = 0;
  double sampleNEvt_PDF_260013 = 0;
  double sampleNEvt_PDF_260014 = 0;
  double sampleNEvt_PDF_260015 = 0;
  double sampleNEvt_PDF_260016 = 0;
  double sampleNEvt_PDF_260017 = 0;
  double sampleNEvt_PDF_260018 = 0;
  double sampleNEvt_PDF_260019 = 0;
  double sampleNEvt_PDF_260020 = 0;
  double sampleNEvt_PDF_260021 = 0;
  double sampleNEvt_PDF_260022 = 0;
  double sampleNEvt_PDF_260023 = 0;
  double sampleNEvt_PDF_260024 = 0;
  double sampleNEvt_PDF_260025 = 0;
  double sampleNEvt_PDF_260026 = 0;
  double sampleNEvt_PDF_260027 = 0;
  double sampleNEvt_PDF_260028 = 0;
  double sampleNEvt_PDF_260029 = 0;
  double sampleNEvt_PDF_260030 = 0;
  double sampleNEvt_PDF_260031 = 0;
  double sampleNEvt_PDF_260032 = 0;
  double sampleNEvt_PDF_260033 = 0;
  double sampleNEvt_PDF_260034 = 0;
  double sampleNEvt_PDF_260035 = 0;
  double sampleNEvt_PDF_260036 = 0;
  double sampleNEvt_PDF_260037 = 0;
  double sampleNEvt_PDF_260038 = 0;
  double sampleNEvt_PDF_260039 = 0;
  double sampleNEvt_PDF_260040 = 0;
  double sampleNEvt_PDF_260041 = 0;
  double sampleNEvt_PDF_260042 = 0;
  double sampleNEvt_PDF_260043 = 0;
  double sampleNEvt_PDF_260044 = 0;
  double sampleNEvt_PDF_260045 = 0;
  double sampleNEvt_PDF_260046 = 0;
  double sampleNEvt_PDF_260047 = 0;
  double sampleNEvt_PDF_260048 = 0;
  double sampleNEvt_PDF_260049 = 0;
  double sampleNEvt_PDF_260050 = 0;
  double sampleNEvt_PDF_260051 = 0;
  double sampleNEvt_PDF_260052 = 0;
  double sampleNEvt_PDF_260053 = 0;
  double sampleNEvt_PDF_260054 = 0;
  double sampleNEvt_PDF_260055 = 0;
  double sampleNEvt_PDF_260056 = 0;
  double sampleNEvt_PDF_260057 = 0;
  double sampleNEvt_PDF_260058 = 0;
  double sampleNEvt_PDF_260059 = 0;
  double sampleNEvt_PDF_260060 = 0;
  double sampleNEvt_PDF_260061 = 0;
  double sampleNEvt_PDF_260062 = 0;
  double sampleNEvt_PDF_260063 = 0;
  double sampleNEvt_PDF_260064 = 0;
  double sampleNEvt_PDF_260065 = 0;
  double sampleNEvt_PDF_260066 = 0;
  double sampleNEvt_PDF_260067 = 0;
  double sampleNEvt_PDF_260068 = 0;
  double sampleNEvt_PDF_260069 = 0;
  double sampleNEvt_PDF_260070 = 0;
  double sampleNEvt_PDF_260071 = 0;
  double sampleNEvt_PDF_260072 = 0;
  double sampleNEvt_PDF_260073 = 0;
  double sampleNEvt_PDF_260074 = 0;
  double sampleNEvt_PDF_260075 = 0;
  double sampleNEvt_PDF_260076 = 0;
  double sampleNEvt_PDF_260077 = 0;
  double sampleNEvt_PDF_260078 = 0;
  double sampleNEvt_PDF_260079 = 0;
  double sampleNEvt_PDF_260080 = 0;
  double sampleNEvt_PDF_260081 = 0;
  double sampleNEvt_PDF_260082 = 0;
  double sampleNEvt_PDF_260083 = 0;
  double sampleNEvt_PDF_260084 = 0;
  double sampleNEvt_PDF_260085 = 0;
  double sampleNEvt_PDF_260086 = 0;
  double sampleNEvt_PDF_260087 = 0;
  double sampleNEvt_PDF_260088 = 0;
  double sampleNEvt_PDF_260089 = 0;
  double sampleNEvt_PDF_260090 = 0;
  double sampleNEvt_PDF_260091 = 0;
  double sampleNEvt_PDF_260092 = 0;
  double sampleNEvt_PDF_260093 = 0;
  double sampleNEvt_PDF_260094 = 0;
  double sampleNEvt_PDF_260095 = 0;
  double sampleNEvt_PDF_260096 = 0;
  double sampleNEvt_PDF_260097 = 0;
  double sampleNEvt_PDF_260098 = 0;
  double sampleNEvt_PDF_260099 = 0;
  double sampleNEvt_PDF_260100 = 0;

  int numero_roots = 0;

  string string_dsid = std::to_string(dsid);
  TString name_list_dsid;
  if(isMC16a) name_list_dsid = "data_MC16a/"+string_dsid+".txt";
  else name_list_dsid = "data_MC16d/"+string_dsid+".txt";

  ifstream filein(name_list_dsid);
  if(!filein) { cout << "error opening input file " <<endl;
  } else {
    while(filein.good()){
    TString name_root;
    filein>>name_root;
    if(name_root=="") break;
    numero_roots++;
   
     TFile *xsecf = TFile::Open(name_root);  
     TTree *totnorm = (TTree*)xsecf->Get("sumWeights");
     vector<float> *totalEventsWeighted_mc_generator_weights;
     TBranch       *b_totalEventsWeighted_mc_generator_weights;   //!
     totalEventsWeighted_mc_generator_weights = 0;
     totnorm->SetBranchAddress("totalEventsWeighted_mc_generator_weights", &totalEventsWeighted_mc_generator_weights, &b_totalEventsWeighted_mc_generator_weights);
               
     for (int ent = 0; ent < totnorm->GetEntries(); ent++){
     totnorm->GetEntry(ent);
     sampleNEvt_RadLo += (totalEventsWeighted_mc_generator_weights->at(6)*totalEventsWeighted_mc_generator_weights->at(194))/totalEventsWeighted_mc_generator_weights->at(0);
     sampleNEvt_FSRHi += (totalEventsWeighted_mc_generator_weights->at(198));
     sampleNEvt_FSRLo += (totalEventsWeighted_mc_generator_weights->at(199));

	sampleNEvt_PDF_90900+=     totalEventsWeighted_mc_generator_weights->at(11);
        sampleNEvt_PDF_90901+=     totalEventsWeighted_mc_generator_weights->at(115);
        sampleNEvt_PDF_90902+=     totalEventsWeighted_mc_generator_weights->at(116);
        sampleNEvt_PDF_90903+=     totalEventsWeighted_mc_generator_weights->at(117);
        sampleNEvt_PDF_90904+=     totalEventsWeighted_mc_generator_weights->at(118);
        sampleNEvt_PDF_90905+=     totalEventsWeighted_mc_generator_weights->at(119);
        sampleNEvt_PDF_90906+=     totalEventsWeighted_mc_generator_weights->at(120);
        sampleNEvt_PDF_90907+=     totalEventsWeighted_mc_generator_weights->at(121);
        sampleNEvt_PDF_90908+=     totalEventsWeighted_mc_generator_weights->at(122);
        sampleNEvt_PDF_90909+=     totalEventsWeighted_mc_generator_weights->at(123);
        sampleNEvt_PDF_90910+=     totalEventsWeighted_mc_generator_weights->at(124);
        sampleNEvt_PDF_90911+=     totalEventsWeighted_mc_generator_weights->at(125);
        sampleNEvt_PDF_90912+=     totalEventsWeighted_mc_generator_weights->at(126);
        sampleNEvt_PDF_90913+=     totalEventsWeighted_mc_generator_weights->at(127);
        sampleNEvt_PDF_90914+=     totalEventsWeighted_mc_generator_weights->at(128);
        sampleNEvt_PDF_90915+=     totalEventsWeighted_mc_generator_weights->at(129);
        sampleNEvt_PDF_90916+=     totalEventsWeighted_mc_generator_weights->at(130);
        sampleNEvt_PDF_90917+=     totalEventsWeighted_mc_generator_weights->at(131);
        sampleNEvt_PDF_90918+=     totalEventsWeighted_mc_generator_weights->at(132);
        sampleNEvt_PDF_90919+=     totalEventsWeighted_mc_generator_weights->at(133);
        sampleNEvt_PDF_90920+=     totalEventsWeighted_mc_generator_weights->at(134);
        sampleNEvt_PDF_90921+=     totalEventsWeighted_mc_generator_weights->at(135);
        sampleNEvt_PDF_90922+=     totalEventsWeighted_mc_generator_weights->at(136);
        sampleNEvt_PDF_90923+=     totalEventsWeighted_mc_generator_weights->at(137);
        sampleNEvt_PDF_90924+=     totalEventsWeighted_mc_generator_weights->at(138);
        sampleNEvt_PDF_90925+=     totalEventsWeighted_mc_generator_weights->at(139);
        sampleNEvt_PDF_90926+=     totalEventsWeighted_mc_generator_weights->at(140);
        sampleNEvt_PDF_90927+=     totalEventsWeighted_mc_generator_weights->at(141);
        sampleNEvt_PDF_90928+=     totalEventsWeighted_mc_generator_weights->at(142);
        sampleNEvt_PDF_90929+=     totalEventsWeighted_mc_generator_weights->at(143);
        sampleNEvt_PDF_90930+=     totalEventsWeighted_mc_generator_weights->at(144);
        sampleNEvt_PDF_260001+=     totalEventsWeighted_mc_generator_weights->at(15);
        sampleNEvt_PDF_260002+=     totalEventsWeighted_mc_generator_weights->at(16);
        sampleNEvt_PDF_260003+=     totalEventsWeighted_mc_generator_weights->at(17);
        sampleNEvt_PDF_260004+=     totalEventsWeighted_mc_generator_weights->at(18);
        sampleNEvt_PDF_260005+=     totalEventsWeighted_mc_generator_weights->at(19);
        sampleNEvt_PDF_260006+=     totalEventsWeighted_mc_generator_weights->at(20);
        sampleNEvt_PDF_260007+=     totalEventsWeighted_mc_generator_weights->at(21);
        sampleNEvt_PDF_260008+=     totalEventsWeighted_mc_generator_weights->at(22);
        sampleNEvt_PDF_260009+=     totalEventsWeighted_mc_generator_weights->at(23);
        sampleNEvt_PDF_260010+=     totalEventsWeighted_mc_generator_weights->at(24);
        sampleNEvt_PDF_260011+=     totalEventsWeighted_mc_generator_weights->at(25);
        sampleNEvt_PDF_260012+=     totalEventsWeighted_mc_generator_weights->at(26);
        sampleNEvt_PDF_260013+=     totalEventsWeighted_mc_generator_weights->at(27);
        sampleNEvt_PDF_260014+=     totalEventsWeighted_mc_generator_weights->at(28);
        sampleNEvt_PDF_260015+=     totalEventsWeighted_mc_generator_weights->at(29);
        sampleNEvt_PDF_260016+=     totalEventsWeighted_mc_generator_weights->at(30);
        sampleNEvt_PDF_260017+=     totalEventsWeighted_mc_generator_weights->at(31);
        sampleNEvt_PDF_260018+=     totalEventsWeighted_mc_generator_weights->at(32);
        sampleNEvt_PDF_260019+=     totalEventsWeighted_mc_generator_weights->at(33);
        sampleNEvt_PDF_260020+=     totalEventsWeighted_mc_generator_weights->at(34);
        sampleNEvt_PDF_260021+=     totalEventsWeighted_mc_generator_weights->at(35);
        sampleNEvt_PDF_260022+=     totalEventsWeighted_mc_generator_weights->at(36);
        sampleNEvt_PDF_260023+=     totalEventsWeighted_mc_generator_weights->at(37);
        sampleNEvt_PDF_260024+=     totalEventsWeighted_mc_generator_weights->at(38);
        sampleNEvt_PDF_260025+=     totalEventsWeighted_mc_generator_weights->at(39);
        sampleNEvt_PDF_260026+=     totalEventsWeighted_mc_generator_weights->at(40);
        sampleNEvt_PDF_260027+=     totalEventsWeighted_mc_generator_weights->at(41);
        sampleNEvt_PDF_260028+=     totalEventsWeighted_mc_generator_weights->at(42);
        sampleNEvt_PDF_260029+=     totalEventsWeighted_mc_generator_weights->at(43);
        sampleNEvt_PDF_260030+=     totalEventsWeighted_mc_generator_weights->at(44);
        sampleNEvt_PDF_260031+=     totalEventsWeighted_mc_generator_weights->at(45);
        sampleNEvt_PDF_260032+=     totalEventsWeighted_mc_generator_weights->at(46);
        sampleNEvt_PDF_260033+=     totalEventsWeighted_mc_generator_weights->at(47);
        sampleNEvt_PDF_260034+=     totalEventsWeighted_mc_generator_weights->at(48);
        sampleNEvt_PDF_260035+=     totalEventsWeighted_mc_generator_weights->at(49);
        sampleNEvt_PDF_260036+=     totalEventsWeighted_mc_generator_weights->at(50);
        sampleNEvt_PDF_260037+=     totalEventsWeighted_mc_generator_weights->at(51);
        sampleNEvt_PDF_260038+=     totalEventsWeighted_mc_generator_weights->at(52);
        sampleNEvt_PDF_260039+=     totalEventsWeighted_mc_generator_weights->at(53);
        sampleNEvt_PDF_260040+=     totalEventsWeighted_mc_generator_weights->at(54);
        sampleNEvt_PDF_260041+=     totalEventsWeighted_mc_generator_weights->at(55);
        sampleNEvt_PDF_260042+=     totalEventsWeighted_mc_generator_weights->at(56);
        sampleNEvt_PDF_260043+=     totalEventsWeighted_mc_generator_weights->at(57);
        sampleNEvt_PDF_260044+=     totalEventsWeighted_mc_generator_weights->at(58);
        sampleNEvt_PDF_260045+=     totalEventsWeighted_mc_generator_weights->at(59);
        sampleNEvt_PDF_260046+=     totalEventsWeighted_mc_generator_weights->at(60);
        sampleNEvt_PDF_260047+=     totalEventsWeighted_mc_generator_weights->at(61);
        sampleNEvt_PDF_260048+=     totalEventsWeighted_mc_generator_weights->at(62);
        sampleNEvt_PDF_260049+=     totalEventsWeighted_mc_generator_weights->at(63);
        sampleNEvt_PDF_260050+=     totalEventsWeighted_mc_generator_weights->at(64);
        sampleNEvt_PDF_260051+=     totalEventsWeighted_mc_generator_weights->at(65);
        sampleNEvt_PDF_260052+=     totalEventsWeighted_mc_generator_weights->at(66);
        sampleNEvt_PDF_260053+=     totalEventsWeighted_mc_generator_weights->at(67);
        sampleNEvt_PDF_260054+=     totalEventsWeighted_mc_generator_weights->at(68);
        sampleNEvt_PDF_260055+=     totalEventsWeighted_mc_generator_weights->at(69);
        sampleNEvt_PDF_260056+=     totalEventsWeighted_mc_generator_weights->at(70);
        sampleNEvt_PDF_260057+=     totalEventsWeighted_mc_generator_weights->at(71);
        sampleNEvt_PDF_260058+=     totalEventsWeighted_mc_generator_weights->at(72);
        sampleNEvt_PDF_260059+=     totalEventsWeighted_mc_generator_weights->at(73);
        sampleNEvt_PDF_260060+=     totalEventsWeighted_mc_generator_weights->at(74);
        sampleNEvt_PDF_260061+=     totalEventsWeighted_mc_generator_weights->at(75);
        sampleNEvt_PDF_260062+=     totalEventsWeighted_mc_generator_weights->at(76);
        sampleNEvt_PDF_260063+=     totalEventsWeighted_mc_generator_weights->at(77);
        sampleNEvt_PDF_260064+=     totalEventsWeighted_mc_generator_weights->at(78);
        sampleNEvt_PDF_260065+=     totalEventsWeighted_mc_generator_weights->at(79);
        sampleNEvt_PDF_260066+=     totalEventsWeighted_mc_generator_weights->at(80);
        sampleNEvt_PDF_260067+=     totalEventsWeighted_mc_generator_weights->at(81);
        sampleNEvt_PDF_260068+=     totalEventsWeighted_mc_generator_weights->at(82);
        sampleNEvt_PDF_260069+=     totalEventsWeighted_mc_generator_weights->at(83);
        sampleNEvt_PDF_260070+=     totalEventsWeighted_mc_generator_weights->at(84);
        sampleNEvt_PDF_260071+=     totalEventsWeighted_mc_generator_weights->at(85);
        sampleNEvt_PDF_260072+=     totalEventsWeighted_mc_generator_weights->at(86);
        sampleNEvt_PDF_260073+=     totalEventsWeighted_mc_generator_weights->at(87);
        sampleNEvt_PDF_260074+=     totalEventsWeighted_mc_generator_weights->at(88);
        sampleNEvt_PDF_260075+=     totalEventsWeighted_mc_generator_weights->at(89);
        sampleNEvt_PDF_260076+=     totalEventsWeighted_mc_generator_weights->at(90);
        sampleNEvt_PDF_260077+=     totalEventsWeighted_mc_generator_weights->at(91);
        sampleNEvt_PDF_260078+=     totalEventsWeighted_mc_generator_weights->at(92);
        sampleNEvt_PDF_260079+=     totalEventsWeighted_mc_generator_weights->at(93);
        sampleNEvt_PDF_260080+=     totalEventsWeighted_mc_generator_weights->at(94);
        sampleNEvt_PDF_260081+=     totalEventsWeighted_mc_generator_weights->at(95);
        sampleNEvt_PDF_260082+=     totalEventsWeighted_mc_generator_weights->at(96);
        sampleNEvt_PDF_260083+=     totalEventsWeighted_mc_generator_weights->at(97);
        sampleNEvt_PDF_260084+=     totalEventsWeighted_mc_generator_weights->at(98);
        sampleNEvt_PDF_260085+=     totalEventsWeighted_mc_generator_weights->at(99);
        sampleNEvt_PDF_260086+=     totalEventsWeighted_mc_generator_weights->at(100);
        sampleNEvt_PDF_260087+=     totalEventsWeighted_mc_generator_weights->at(101);
        sampleNEvt_PDF_260088+=     totalEventsWeighted_mc_generator_weights->at(102);
        sampleNEvt_PDF_260089+=     totalEventsWeighted_mc_generator_weights->at(103);
        sampleNEvt_PDF_260090+=     totalEventsWeighted_mc_generator_weights->at(104);
        sampleNEvt_PDF_260091+=     totalEventsWeighted_mc_generator_weights->at(105);
        sampleNEvt_PDF_260092+=     totalEventsWeighted_mc_generator_weights->at(106);
        sampleNEvt_PDF_260093+=     totalEventsWeighted_mc_generator_weights->at(107);
        sampleNEvt_PDF_260094+=     totalEventsWeighted_mc_generator_weights->at(108);
        sampleNEvt_PDF_260095+=     totalEventsWeighted_mc_generator_weights->at(109);
        sampleNEvt_PDF_260096+=     totalEventsWeighted_mc_generator_weights->at(110);
        sampleNEvt_PDF_260097+=     totalEventsWeighted_mc_generator_weights->at(111);
        sampleNEvt_PDF_260098+=     totalEventsWeighted_mc_generator_weights->at(112);
        sampleNEvt_PDF_260099+=     totalEventsWeighted_mc_generator_weights->at(113);
        sampleNEvt_PDF_260100+=     totalEventsWeighted_mc_generator_weights->at(114);

     }
     delete totnorm;
     xsecf->Close();
    }
  }
 
  cerr<<"sampleNEvt_RadLo	"<<sampleNEvt_RadLo<<endl;
  cerr<<"sampleNEvt_FSRHi	"<<sampleNEvt_FSRHi<<endl;
  cerr<<"sampleNEvt_FSRLo	"<<sampleNEvt_FSRLo<<endl;
	cerr<<"PDF_90900	"<<sampleNEvt_PDF_90900<<endl;
        cerr<<"PDF_90901	"<<sampleNEvt_PDF_90901<<endl;
        cerr<<"PDF_90902	"<<sampleNEvt_PDF_90902<<endl;
        cerr<<"PDF_90903	"<<sampleNEvt_PDF_90903<<endl;
        cerr<<"PDF_90904	"<<sampleNEvt_PDF_90904<<endl;
        cerr<<"PDF_90905	"<<sampleNEvt_PDF_90905<<endl;
        cerr<<"PDF_90906	"<<sampleNEvt_PDF_90906<<endl;
        cerr<<"PDF_90907	"<<sampleNEvt_PDF_90907<<endl;
        cerr<<"PDF_90908	"<<sampleNEvt_PDF_90908<<endl;
        cerr<<"PDF_90909	"<<sampleNEvt_PDF_90909<<endl;
        cerr<<"PDF_90910	"<<sampleNEvt_PDF_90910<<endl;
        cerr<<"PDF_90911	"<<sampleNEvt_PDF_90911<<endl;
        cerr<<"PDF_90912	"<<sampleNEvt_PDF_90912<<endl;
        cerr<<"PDF_90913	"<<sampleNEvt_PDF_90913<<endl;
        cerr<<"PDF_90914	"<<sampleNEvt_PDF_90914<<endl;
        cerr<<"PDF_90915	"<<sampleNEvt_PDF_90915<<endl;
        cerr<<"PDF_90916	"<<sampleNEvt_PDF_90916<<endl;
        cerr<<"PDF_90917	"<<sampleNEvt_PDF_90917<<endl;
        cerr<<"PDF_90918	"<<sampleNEvt_PDF_90918<<endl;
        cerr<<"PDF_90919	"<<sampleNEvt_PDF_90919<<endl;
        cerr<<"PDF_90920	"<<sampleNEvt_PDF_90920<<endl;
        cerr<<"PDF_90921	"<<sampleNEvt_PDF_90921<<endl;
        cerr<<"PDF_90922	"<<sampleNEvt_PDF_90922<<endl;
        cerr<<"PDF_90923	"<<sampleNEvt_PDF_90923<<endl;
        cerr<<"PDF_90924	"<<sampleNEvt_PDF_90924<<endl;
        cerr<<"PDF_90925	"<<sampleNEvt_PDF_90925<<endl;
        cerr<<"PDF_90926	"<<sampleNEvt_PDF_90926<<endl;
        cerr<<"PDF_90927	"<<sampleNEvt_PDF_90927<<endl;
        cerr<<"PDF_90928	"<<sampleNEvt_PDF_90928<<endl;
        cerr<<"PDF_90929	"<<sampleNEvt_PDF_90929<<endl;
        cerr<<"PDF_90930	"<<sampleNEvt_PDF_90930<<endl;
        cerr<<"PDF_260001	"<<sampleNEvt_PDF_260001<<endl;
        cerr<<"PDF_260002	"<<sampleNEvt_PDF_260002<<endl;
        cerr<<"PDF_260003	"<<sampleNEvt_PDF_260003<<endl;
        cerr<<"PDF_260004	"<<sampleNEvt_PDF_260004<<endl;
        cerr<<"PDF_260005	"<<sampleNEvt_PDF_260005<<endl;
        cerr<<"PDF_260006	"<<sampleNEvt_PDF_260006<<endl;
        cerr<<"PDF_260007	"<<sampleNEvt_PDF_260007<<endl;
        cerr<<"PDF_260008	"<<sampleNEvt_PDF_260008<<endl;
        cerr<<"PDF_260009	"<<sampleNEvt_PDF_260009<<endl;
        cerr<<"PDF_260010	"<<sampleNEvt_PDF_260010<<endl;
        cerr<<"PDF_260011	"<<sampleNEvt_PDF_260011<<endl;
        cerr<<"PDF_260012	"<<sampleNEvt_PDF_260012<<endl;
        cerr<<"PDF_260013	"<<sampleNEvt_PDF_260013<<endl;
        cerr<<"PDF_260014	"<<sampleNEvt_PDF_260014<<endl;
        cerr<<"PDF_260015	"<<sampleNEvt_PDF_260015<<endl;
        cerr<<"PDF_260016	"<<sampleNEvt_PDF_260016<<endl;
        cerr<<"PDF_260017	"<<sampleNEvt_PDF_260017<<endl;
        cerr<<"PDF_260018	"<<sampleNEvt_PDF_260018<<endl;
        cerr<<"PDF_260019	"<<sampleNEvt_PDF_260019<<endl;
        cerr<<"PDF_260020	"<<sampleNEvt_PDF_260020<<endl;
        cerr<<"PDF_260021	"<<sampleNEvt_PDF_260021<<endl;
        cerr<<"PDF_260022	"<<sampleNEvt_PDF_260022<<endl;
        cerr<<"PDF_260023	"<<sampleNEvt_PDF_260023<<endl;
        cerr<<"PDF_260024	"<<sampleNEvt_PDF_260024<<endl;
        cerr<<"PDF_260025	"<<sampleNEvt_PDF_260025<<endl;
        cerr<<"PDF_260026	"<<sampleNEvt_PDF_260026<<endl;
        cerr<<"PDF_260027	"<<sampleNEvt_PDF_260027<<endl;
        cerr<<"PDF_260028	"<<sampleNEvt_PDF_260028<<endl;
        cerr<<"PDF_260029	"<<sampleNEvt_PDF_260029<<endl;
        cerr<<"PDF_260030	"<<sampleNEvt_PDF_260030<<endl;
        cerr<<"PDF_260031	"<<sampleNEvt_PDF_260031<<endl;
        cerr<<"PDF_260032	"<<sampleNEvt_PDF_260032<<endl;
        cerr<<"PDF_260033	"<<sampleNEvt_PDF_260033<<endl;
        cerr<<"PDF_260034	"<<sampleNEvt_PDF_260034<<endl;
        cerr<<"PDF_260035	"<<sampleNEvt_PDF_260035<<endl;
        cerr<<"PDF_260036	"<<sampleNEvt_PDF_260036<<endl;
        cerr<<"PDF_260037	"<<sampleNEvt_PDF_260037<<endl;
        cerr<<"PDF_260038	"<<sampleNEvt_PDF_260038<<endl;
        cerr<<"PDF_260039	"<<sampleNEvt_PDF_260039<<endl;
        cerr<<"PDF_260040	"<<sampleNEvt_PDF_260040<<endl;
        cerr<<"PDF_260041	"<<sampleNEvt_PDF_260041<<endl;
        cerr<<"PDF_260042	"<<sampleNEvt_PDF_260042<<endl;
        cerr<<"PDF_260043	"<<sampleNEvt_PDF_260043<<endl;
        cerr<<"PDF_260044	"<<sampleNEvt_PDF_260044<<endl;
        cerr<<"PDF_260045	"<<sampleNEvt_PDF_260045<<endl;
        cerr<<"PDF_260046	"<<sampleNEvt_PDF_260046<<endl;
        cerr<<"PDF_260047	"<<sampleNEvt_PDF_260047<<endl;
        cerr<<"PDF_260048	"<<sampleNEvt_PDF_260048<<endl;
        cerr<<"PDF_260049	"<<sampleNEvt_PDF_260049<<endl;
        cerr<<"PDF_260050	"<<sampleNEvt_PDF_260050<<endl;
        cerr<<"PDF_260051	"<<sampleNEvt_PDF_260051<<endl;
        cerr<<"PDF_260052	"<<sampleNEvt_PDF_260052<<endl;
        cerr<<"PDF_260053	"<<sampleNEvt_PDF_260053<<endl;
        cerr<<"PDF_260054	"<<sampleNEvt_PDF_260054<<endl;
        cerr<<"PDF_260055	"<<sampleNEvt_PDF_260055<<endl;
        cerr<<"PDF_260056	"<<sampleNEvt_PDF_260056<<endl;
        cerr<<"PDF_260057	"<<sampleNEvt_PDF_260057<<endl;
        cerr<<"PDF_260058	"<<sampleNEvt_PDF_260058<<endl;
        cerr<<"PDF_260059	"<<sampleNEvt_PDF_260059<<endl;
        cerr<<"PDF_260060	"<<sampleNEvt_PDF_260060<<endl;
        cerr<<"PDF_260061	"<<sampleNEvt_PDF_260061<<endl;
        cerr<<"PDF_260062	"<<sampleNEvt_PDF_260062<<endl;
        cerr<<"PDF_260063	"<<sampleNEvt_PDF_260063<<endl;
        cerr<<"PDF_260064	"<<sampleNEvt_PDF_260064<<endl;
        cerr<<"PDF_260065	"<<sampleNEvt_PDF_260065<<endl;
        cerr<<"PDF_260066	"<<sampleNEvt_PDF_260066<<endl;
        cerr<<"PDF_260067	"<<sampleNEvt_PDF_260067<<endl;
        cerr<<"PDF_260068	"<<sampleNEvt_PDF_260068<<endl;
        cerr<<"PDF_260069	"<<sampleNEvt_PDF_260069<<endl;
        cerr<<"PDF_260070	"<<sampleNEvt_PDF_260070<<endl;
        cerr<<"PDF_260071	"<<sampleNEvt_PDF_260071<<endl;
        cerr<<"PDF_260072	"<<sampleNEvt_PDF_260072<<endl;
        cerr<<"PDF_260073	"<<sampleNEvt_PDF_260073<<endl;
        cerr<<"PDF_260074	"<<sampleNEvt_PDF_260074<<endl;
        cerr<<"PDF_260075	"<<sampleNEvt_PDF_260075<<endl;
        cerr<<"PDF_260076	"<<sampleNEvt_PDF_260076<<endl;
        cerr<<"PDF_260077	"<<sampleNEvt_PDF_260077<<endl;
        cerr<<"PDF_260078	"<<sampleNEvt_PDF_260078<<endl;
        cerr<<"PDF_260079	"<<sampleNEvt_PDF_260079<<endl;
        cerr<<"PDF_260080	"<<sampleNEvt_PDF_260080<<endl;
        cerr<<"PDF_260081	"<<sampleNEvt_PDF_260081<<endl;
        cerr<<"PDF_260082	"<<sampleNEvt_PDF_260082<<endl;
        cerr<<"PDF_260083	"<<sampleNEvt_PDF_260083<<endl;
        cerr<<"PDF_260084	"<<sampleNEvt_PDF_260084<<endl;
        cerr<<"PDF_260085	"<<sampleNEvt_PDF_260085<<endl;
        cerr<<"PDF_260086	"<<sampleNEvt_PDF_260086<<endl;
        cerr<<"PDF_260087	"<<sampleNEvt_PDF_260087<<endl;
        cerr<<"PDF_260088	"<<sampleNEvt_PDF_260088<<endl;
        cerr<<"PDF_260089	"<<sampleNEvt_PDF_260089<<endl;
        cerr<<"PDF_260090	"<<sampleNEvt_PDF_260090<<endl;
        cerr<<"PDF_260091	"<<sampleNEvt_PDF_260091<<endl;
        cerr<<"PDF_260092	"<<sampleNEvt_PDF_260092<<endl;
        cerr<<"PDF_260093	"<<sampleNEvt_PDF_260093<<endl;
        cerr<<"PDF_260094	"<<sampleNEvt_PDF_260094<<endl;
        cerr<<"PDF_260095	"<<sampleNEvt_PDF_260095<<endl;
        cerr<<"PDF_260096	"<<sampleNEvt_PDF_260096<<endl;
        cerr<<"PDF_260097	"<<sampleNEvt_PDF_260097<<endl;
        cerr<<"PDF_260098	"<<sampleNEvt_PDF_260098<<endl;
        cerr<<"PDF_260099	"<<sampleNEvt_PDF_260099<<endl;
        cerr<<"PDF_260100	"<<sampleNEvt_PDF_260100<<endl;

  //cerr<<"numero_roots	"<<numero_roots<<endl;
  filein.close();

return numero_roots;
}
