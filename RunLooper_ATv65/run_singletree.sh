#! /bin/sh
echo ------------------------------------------------------
echo 'Launching jobs YAY!'
echo ------------------------------------------------------

#echo ------------------------------------------------------
#echo 'Launching nominal jobs'
#echo ------------------------------------------------------
#while read channel; do
#	syst_dir="nominal"
#	charge="OS"
#	echo ------------------------------------------------------
#	echo 'Running on Systematic==' $syst_dir 'channel==' $channel 'charge==' $charge
#	echo ------------------------------------------------------
#	qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16a_ttbar.sh
#	qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16a_ttbarVts.sh
#	qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16a_singlet.sh
#	qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16a_VV.sh
#	qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16a_Zjets.sh
#	qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16a_Wjets.sh
#	qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16a_ttX.sh
#	qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16d_ttbar.sh
#	qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16d_ttbarVts.sh
#	qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16d_singlet.sh
#	qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16d_VV.sh
#	qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16d_Zjets.sh
#	qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16d_Wjets.sh
#	qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16d_ttX.sh
#done <channel.list

#echo ------------------------------------------------------
#echo 'Launching ttbar variations jobs'
#echo ------------------------------------------------------
#while read channel; do
#	syst_dir="RadHi"
#	charge="OS"
#	echo ------------------------------------------------------
#	echo 'Running on Systematic==' $syst_dir 'channel==' $channel 'charge==' $charge
#	echo ------------------------------------------------------
#	qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16a_RadHi.sh
#	qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16d_RadHi.sh
#done <channel.list
#
#while read channel; do
#	while read syst_dir; do
#		charge="OS"
#		echo ------------------------------------------------------
#		echo 'Running on Systematic==' $syst_dir 'channel==' $channel 'charge==' $charge
#		echo ------------------------------------------------------
#		qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16a_ttbar.sh
#		qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16d_ttbar.sh
#	done <ttbarvar.list
#done <channel.list


#echo ------------------------------------------------------
#echo 'Launching detector variations jobs'
#echo ------------------------------------------------------
#while read channel; do
#	while read syst_dir; do
#		charge="OS"
#		echo ------------------------------------------------------
#		echo 'Running on Systematic==' $syst_dir 'channel==' $channel 'charge==' $charge
#		echo ------------------------------------------------------
#		qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16a_ttbar.sh
#		qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16a_ttbarVts.sh
#		qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16a_singlet.sh
#		qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16a_VV.sh
#		qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16a_Zjets.sh
#		qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16a_Wjets.sh
#		qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16a_ttX.sh
#		qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16d_ttbar.sh
#		qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16d_ttbarVts.sh
#		qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16d_singlet.sh
#		qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16d_VV.sh
#		qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16d_Zjets.sh
#		qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16d_Wjets.sh
#		qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16d_ttX.sh
#	done <syst.list
#done <channel.list

#echo ------------------------------------------------------
#echo 'Launching detector weight jobs'
#echo ------------------------------------------------------
#while read channel; do
#	while read syst_dir; do
#		charge="OS"
#		echo ------------------------------------------------------
#		echo 'Running on Systematic==' $syst_dir 'channel==' $channel 'charge==' $charge
#		echo ------------------------------------------------------
#		qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16a_ttbar.sh
#		qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16a_singlet.sh
#		qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16a_VV.sh
#		qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16a_Zjets.sh
#		qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16a_Wjets.sh
#		qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16a_ttX.sh
#		qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16d_ttbar.sh
#		qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16d_singlet.sh
#		qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16d_VV.sh
#		qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16d_Zjets.sh
#		qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16d_Wjets.sh
#		qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16d_ttX.sh
#	done <syst_weight.list
#done <channel.list

#echo ------------------------------------------------------
#echo 'Launching singletop Diagram subtraction and H7'
#echo ------------------------------------------------------
#while read channel; do
#	syst_dir="nominal"
#	charge="OS"
#	echo ------------------------------------------------------
#	echo 'Running on Systematic==' $syst_dir 'channel==' $channel 'charge==' $charge
#	echo ------------------------------------------------------
#	qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16a_singlet_DS.sh
#	qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16d_singlet_DS.sh
#	qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16a_singlet_H7.sh
#	qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16d_singlet_H7.sh
#done <channel.list


#echo ------------------------------------------------------
#echo 'Launching singletop variations jobs'
#echo ------------------------------------------------------
#while read channel; do
#	syst_dir="RadLo"
#	charge="OS"
#	echo ------------------------------------------------------
#	echo 'Running on Systematic==' $syst_dir 'channel==' $channel 'charge==' $charge
#	echo ------------------------------------------------------
#	qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16a_singlet.sh
#	qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16d_singlet.sh
#done <channel.list
#
#while read channel; do
#	syst_dir="RadHi"
#	charge="OS"
#	echo ------------------------------------------------------
#	echo 'Running on Systematic==' $syst_dir 'channel==' $channel 'charge==' $charge
#	echo ------------------------------------------------------
#	qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16a_singlet.sh
#	qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16d_singlet.sh
#done <channel.list

#echo ------------------------------------------------------
#echo 'Launching ttbar PDF variation'
#echo ------------------------------------------------------
#while read channel; do
#	while read syst_dir; do
#		charge="OS"
#		echo ------------------------------------------------------
#		echo 'Running on Systematic==' $syst_dir 'channel==' $channel 'charge==' $charge
#		echo ------------------------------------------------------
#		qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16a_ttbar.sh
#		qsub -v SYSTTREE=$syst_dir,CHANNEL=$channel,CHARGE=$charge batch_scripts_syst/tosubmit_mc16d_ttbar.sh
#	done <ttbar_PDF.list
#done <channel.list
