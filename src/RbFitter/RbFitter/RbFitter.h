#ifndef _RbFitter_H_
#define _RbFitter_H_

#include <iostream>
#include "TMinuit.h"
#include "TRandom3.h"
#include "TFile.h"
#include "TH1.h"
#include "TString.h"
#include "math.h"


using namespace std;
// Header file for the classes stored in the TTree if any.
class RbFitter {
    public:
      RbFitter();
      TRandom3 *randGen;
      TFile *RootFile;
      //void fitToData(TH1F *dataHist, TH1F *bHist, TH1F *cHist, TH1F *lHist, int doChi2Fit, int forceFraction, float startvalue, int fixBFraction, int fitLCRatio, float lcRatioUpper, float lcRatioLower);
      void fitToData(TH1F *dataBTagHist, TH1F *dataNjetsHist, double alpha_value[3], double epsilon_b_value, double epsilon_q_value);
      //TH1F *data;
      //TH1F *bFitted;
      //TH1F *cFitted;
      //TH1F *lFitted;       
      //TH1F *bFracFitted;
      //TH1F *cFracFitted;
      //TH1F *lFracFitted;       
      TH1F *FittedParameters;       
      TH1F *FittedHist;
      void setOutput(TString output);
      void setupArrays(); 
      void runPEs(int numberOfPEs, int doChi2Fit, int forceFraction, int fitLCRatio);
      void setupParameters(int inputFixlToCharmRatio, double inputLToCharmRatio, int inputNumberOfPE, int inputFirstBinToFit, int inputLastBinToFit);
      void fillArray(TH1F *dataHist, TH1F *bHist, TH1F *cHist, TH1F *lHist);  
      void fillFitParameterHists(TH1F *bTagHisto, TH1F* njetsHisto, double alpha_0, double alpha_1, double alpha_2, double epsilon_b, double epsilon_q, double Rb_fit, double Rb_fit_err);
      void finalize();
      ~RbFitter();
};

class Tools {
  public:
    vector<string> getStringSegments(string input, char delim);
    Tools(){};
    virtual ~Tools(){};
};
#endif
