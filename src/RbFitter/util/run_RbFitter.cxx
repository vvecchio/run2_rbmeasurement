#include "RbFitter/RbFitter.h"
#include<iostream>
#include<fstream>
using namespace std;

int main(int argc, char **argv)
{
  if(argc < 2) {
    printf("More than two arguments are needed, aborting!\n");
    exit(0);
  }
 
  char * inputConfig = argv[1];
  string inputConfigName = string(inputConfig);
  cout<<"Input config file: "<<inputConfigName<<endl; 

  char * tree = argv[2];
  TString treename = string(tree);
  cerr<<"Fitting on treename: "<<treename<<endl;
  
  ifstream inputConfigFile;
  inputConfigFile.open(inputConfigName.c_str(), ios::in);
 
  /////////////////////////////////////////////////////////
  //  Get values and info from the external config file  //
  /////////////////////////////////////////////////////////
 
  string message;
  string dataFilePath;
  string dataFileName;
  string btagHistName;
  string ModelHistName;
  double epsilon_b_value;
  double epsilon_q_value;
  double alpha0_config;
  double alpha1_config;
  double alpha2_config;
 
  inputConfigFile >> message >> dataFilePath;
  inputConfigFile >> message >> dataFileName;
  inputConfigFile >> message >> btagHistName;
  inputConfigFile >> message >> ModelHistName;
  inputConfigFile >> message >> epsilon_b_value;
  inputConfigFile >> message >> epsilon_q_value;
  inputConfigFile >> message >> alpha0_config;
  inputConfigFile >> message >> alpha1_config;
  inputConfigFile >> message >> alpha2_config;

  const char* data_path_const_char = dataFilePath.c_str();
  TString data_path_string = (TString)data_path_const_char;
  const char* data_name_const_char = dataFileName.c_str();
  TString data_name_string = (TString)data_name_const_char;

  TFile dataInputFile(data_path_string+""+treename+"/"+data_name_string);
 
  TH1F  *HIST_bkgmodel 	= (TH1F*)dataInputFile.Get(ModelHistName.c_str());
  TH1F  *HIST_data 	= (TH1F*)dataInputFile.Get("h_Mlplusj");
  
  double alpha_value[3] = {0.,0.,0.};
  double alpha = 0.;
  if(alpha0_config == -1 && alpha1_config == -1 && alpha2_config == -1){
  Double_t int_data = HIST_data->Integral()/2.;
  Int_t index_200 = HIST_bkgmodel->FindBin(200);
  Double_t f = HIST_bkgmodel->Integral(index_200,10000)/(HIST_bkgmodel->Integral());
  double f_err = f*0.02;
  //double f_err = sqrt(f*(1-f)/HIST_bkgmodel->Integral());
  Double_t Nmis_200 = HIST_data->Integral(index_200,10000);
  Double_t Nmis = Nmis_200/f;
  cout<<"f	"<<f<<"		pm	"<<f_err<<endl;
  cout<<"Nmis	"<<Nmis<<"		pm	"<<sqrt(Nmis)<<endl;
  cout<<"Nevents	"<<int_data<<"		pm	"<<sqrt(int_data)<<endl;
  cout<<"N_corr	"<<(int_data*2-Nmis)<<"		pm	"<<sqrt(int_data*2-Nmis)<<endl;
  HIST_bkgmodel->Scale(Nmis/HIST_bkgmodel->Integral());
  double alpha_err = (1/(f*int_data))*sqrt((pow(Nmis_200*sqrt(int_data)/int_data,2))+(pow(Nmis_200*f_err/f,2))+(Nmis_200));
  alpha     = (int_data*2-Nmis)/int_data;
  alpha_value[0]    = (1-alpha)*(1-alpha);
  alpha_value[1]    = 2*alpha*(1-alpha);
  alpha_value[2]    = alpha*alpha;
  } else {
  alpha_value[0] = alpha0_config;
  alpha_value[1] = alpha1_config;
  alpha_value[2] = alpha2_config;
  }
  cout<<"alpha	"<<alpha<<endl;
  cout<<"alpha0	"<<alpha_value[0]<<endl;
  cout<<"alpha1	"<<alpha_value[1]<<endl;
  cout<<"alpha2	"<<alpha_value[2]<<endl;

  TH1F* dataBTagHist  = (TH1F*)dataInputFile.Get(btagHistName.c_str());
  TH1F* dataNjetsHist = (TH1F*)dataInputFile.Get("h_njet");
  
  RbFitter fitter;
  //fitter.setupParameters(fixlToCharmRatio, lToCharmRatio, numberOfPE, firstBinToFit, lastBinToFit);
  fitter.setOutput("FitOut_"+treename+".root");
  fitter.fitToData(dataBTagHist, dataNjetsHist, alpha_value, epsilon_b_value, epsilon_q_value);
  fitter.finalize(); 
  
  return 0;
}
