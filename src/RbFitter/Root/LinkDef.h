//#include "RbFitter/Looper.h"
#include "RbFitter/RbFitter.h"
//#include "RbFitter/Channel.h"

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#endif

#ifdef __CINT__
//#pragma link C++ class Looper+;
#pragma link C++ class Fitter+;
//#pragma link C++ class Channel+;
#endif
