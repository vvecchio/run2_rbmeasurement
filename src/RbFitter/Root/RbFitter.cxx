#include "RbFitter/RbFitter.h"
#include "RbFitter/RbFitter.h"
#include <TMath.h>
#include <TGraph.h>

using namespace std;
double  startValue; 
int     doChi2Fit;
int     numberOfPE;
double N_hat_0btag, N_hat_1btag, N_hat_2btag;
TH1F *bTagHisto;
TH1F *njetsHisto;
double alpha_0;
double alpha_1;
double alpha_2;
double epsilon_b;
double epsilon_q;
void fcnLLH_Rb(int &npar, double *gin, double &f, double *par, int iflag);
void fcnLLH_Rb_PE(int &npar, double *gin, double &f, double *par, int iflag);
void fcnChi2_pTrel(int &npar, double *gin, double &f, double *par, int iflag);
void fcnChi2_pTrel_PE(int &npar, double *gin, double &f, double *par, int iflag);
int iter = 10000;
int index_iter = 0;
TGraph *llh_gra = new TGraph(iter);

RbFitter::RbFitter() {}
RbFitter::~RbFitter() {}

void RbFitter::fitToData(TH1F *dataBTagHist, TH1F *dataNjetsHist, double alpha_value[3], double epsilon_b_value, double epsilon_q_value){
  
  bTagHisto  = (TH1F*)dataBTagHist->Clone();  
  njetsHisto = (TH1F*)dataNjetsHist->Clone(); 
  alpha_0 = alpha_value[0];
  alpha_1 = alpha_value[1];
  alpha_2 = alpha_value[2];
  epsilon_b = epsilon_b_value;
  epsilon_q = epsilon_q_value;

  cerr<<"########"<<endl;
  cerr<<"Number of Events with 2 jets:	"<<njetsHisto->GetBinContent(njetsHisto->FindBin(2))<<endl;
  cerr<<"Number of Events with 0 b-jets:	"<<bTagHisto->GetBinContent(bTagHisto->FindBin(0))<<endl;
  cerr<<"Number of Events with 1 b-jets:	"<<bTagHisto->GetBinContent(bTagHisto->FindBin(1))<<endl;
  cerr<<"Number of Events with 2 b-jets:	"<<bTagHisto->GetBinContent(bTagHisto->FindBin(2))<<endl;
  cerr<<"#### FIT PARAMETERS	####"<<endl;
  cerr<<"epsilon_b:	"<<epsilon_b<<endl;
  cerr<<"epsilon_q:	"<<epsilon_q<<endl;
  cerr<<"alpha0:	"<<alpha_0<<endl;
  cerr<<"alpha1:	"<<alpha_1<<endl;
  cerr<<"alpha2:	"<<alpha_2<<endl;
  cerr<<"########"<<endl;

  TMinuit *myMinuit;
  
  doChi2Fit = 0;
  startValue = 1.001;

  // Three parameter Minuit
  myMinuit = new TMinuit(1); //arg is Int_t maxpar
  myMinuit->SetPrintLevel(1); //= -1 quiet (also suppress all warnings) = 0 normal = 1 verbose
  myMinuit->SetFCN(fcnLLH_Rb);
  myMinuit->SetErrorDef(1); //SetErrorDef (Double_t up) To get the n-sigma contour the error def parameter "up" has to set to n^2. 

  int ierflag = 0;
  float startStepSize = 0.001;
  float minValue = 0.5;
  float maxValue = 1.5;
   
  myMinuit->mnparm(0, "Rb",  startValue, startStepSize, minValue, maxValue, ierflag);
  myMinuit->Migrad(); 
  myMinuit->mnmnos();
  myMinuit->SetMaxIterations(iter);
 
  double Rb_fit, Rb_fit_error;
  myMinuit->GetParameter(0, Rb_fit, Rb_fit_error);

  cerr<<"########"<<endl;
  cerr<<"N(0bjets) from fit:	"<<N_hat_0btag<<"	Ratio with observed:	"<<N_hat_0btag/bTagHisto->GetBinContent(bTagHisto->FindBin(0))<<endl;
  cerr<<"N(1bjets) from fit:	"<<N_hat_1btag<<"	Ratio with observed:	"<<N_hat_1btag/bTagHisto->GetBinContent(bTagHisto->FindBin(1))<<endl;
  cerr<<"N(2bjets) from fit:	"<<N_hat_2btag<<"	Ratio with observed:	"<<N_hat_2btag/bTagHisto->GetBinContent(bTagHisto->FindBin(2))<<endl;
  cerr<<"########"<<endl;

  fillFitParameterHists(bTagHisto, njetsHisto, alpha_0, alpha_1, alpha_2, epsilon_b, epsilon_q, Rb_fit, Rb_fit_error);
  
  if (numberOfPE) {
    cout<<"Start pseudo experiments:"<<endl;
    //runPEs(numberOfPE, doChi2Fit, forceFraction, fitLCRatio);   
  } 

}
 
void RbFitter::setOutput(TString output) {
    Tools tool;
    TString ffile = "fitterOutput/"+output;
    cout << "\n -> Output file is =" << ffile << endl;
    RootFile = new TFile(ffile, "RECREATE", "RbFitter results file");
    RootFile->cd();
}

void RbFitter::setupArrays() {
}

void RbFitter::setupParameters(int inputFixlToCharmRatio, double inputLToCharmRatio, int inputNumberOfPE, int inputFirstBinToFit, int inputLastBinToFit) {
}
 
void RbFitter::fillArray(TH1F *dataHist, TH1F *bHist, TH1F *cHist, TH1F *lHist) {
} 

void RbFitter::runPEs(int numberOfPEs, int doChi2Fit, int forceFraction, int fitLCRatio) {
}

void RbFitter::fillFitParameterHists(TH1F *bTagHisto, TH1F* njetsHisto, double alpha_0, double alpha_1, double alpha_2, double epsilon_b, double epsilon_q, double Rb_fit, double Rb_fit_err){
  TH1F *FittedParameters = new TH1F("fitted_parameters", "fitted_parameters", 15, 0, 15);
  TH1F *FittedHist = (TH1F*) bTagHisto->Clone();
  FittedParameters->GetXaxis()->SetBinLabel(1, "N_0btag"); 
  FittedParameters->SetBinContent(1, bTagHisto->GetBinContent(bTagHisto->FindBin(0))); 
  FittedParameters->GetXaxis()->SetBinLabel(2, "N_1btag"); 
  FittedParameters->SetBinContent(2, bTagHisto->GetBinContent(bTagHisto->FindBin(1))); 
  FittedParameters->GetXaxis()->SetBinLabel(3, "N_2btag"); 
  FittedParameters->SetBinContent(3, bTagHisto->GetBinContent(bTagHisto->FindBin(2))); 
  FittedParameters->GetXaxis()->SetBinLabel(4, "alpha_0"); 
  FittedParameters->SetBinContent(4, alpha_0);
  FittedParameters->GetXaxis()->SetBinLabel(5, "alpha_1"); 
  FittedParameters->SetBinContent(5, alpha_1);
  FittedParameters->GetXaxis()->SetBinLabel(6, "alpha_2"); 
  FittedParameters->SetBinContent(6, alpha_2);
  FittedParameters->GetXaxis()->SetBinLabel(7, "epsilon_b"); 
  FittedParameters->SetBinContent(7, epsilon_b);
  FittedParameters->GetXaxis()->SetBinLabel(8, "epsilon_q"); 
  FittedParameters->SetBinContent(8, epsilon_q);
  //FittedParameters->SetBinContent(9, chi2); 
  //FittedParameters->SetBinContent(9, 0); 
  FittedParameters->GetXaxis()->SetBinLabel(10, "Rb_fit"); 
  FittedParameters->SetBinContent(10, Rb_fit); 
  FittedParameters->GetXaxis()->SetBinLabel(11, "Rb_fit_err"); 
  FittedParameters->SetBinContent(11, Rb_fit_err); 

  llh_gra->SetMarkerStyle(30);
  llh_gra->Write();
} 

void RbFitter::finalize() {
  RootFile->Write();
  RootFile->Close();
}

void fcnLLH_Rb(int &npar, double *gin, double &f, double *par, int iflag) {

  double llh = 0;

  //P_I_J_K where I = number of btags, J = number of jets, K = number of jets from top
  double P_2_2_0 = alpha_0*(epsilon_q*epsilon_q);
  double P_2_2_1 = alpha_1*((par[0]*epsilon_b*epsilon_q ) + ((1-par[0])*pow(epsilon_q,2)));
  double P_2_2_2 = alpha_2*(pow(par[0]*epsilon_b,2) + 2*par[0]*(1-par[0])*epsilon_b*epsilon_q + pow((1-par[0])*epsilon_q,2));
  double P_2_2 = P_2_2_2 + P_2_2_1 + P_2_2_0; 

  double P_1_2_0 = alpha_0*2*epsilon_q*(1-epsilon_q);
  double P_1_2_1 = alpha_1*( (par[0]*epsilon_b*(1-epsilon_q)) + (par[0]*(1-epsilon_b)*epsilon_q) + (2*(1-par[0])*epsilon_q*(1-epsilon_q)) );
  double P_1_2_2 = alpha_2*( (2*pow(par[0],2)*epsilon_b*(1-epsilon_b)) + (2*par[0]*(1-par[0])*( epsilon_b - 2*epsilon_b*epsilon_q + epsilon_q )) + (2*pow(1-par[0],2)*epsilon_q*(1-epsilon_q)) );
  double P_1_2 = P_1_2_2 + P_1_2_1 + P_1_2_0;

  double P_0_2_0 = alpha_0*pow(1-epsilon_q,2);
  double P_0_2_1 = alpha_1*( (par[0]*(1-epsilon_b)*(1-epsilon_q)) + ((1-par[0])*pow(1-epsilon_q,2)) );
  double P_0_2_2 = alpha_2*( (pow(par[0]*(1-epsilon_b),2)) + (2*par[0]*(1-par[0])*(1-epsilon_b)*(1-epsilon_q)) + (pow((1-par[0])*(1-epsilon_q),2)) );
  double P_0_2 = P_0_2_2 + P_0_2_1 + P_0_2_0;

  N_hat_0btag = njetsHisto->GetBinContent(njetsHisto->FindBin(2))*P_0_2;
  N_hat_1btag = njetsHisto->GetBinContent(njetsHisto->FindBin(2))*P_1_2;
  N_hat_2btag = njetsHisto->GetBinContent(njetsHisto->FindBin(2))*P_2_2;

  double N_obs_0btag, N_obs_1btag, N_obs_2btag;
  N_obs_0btag = bTagHisto->GetBinContent(bTagHisto->FindBin(0));
  N_obs_1btag = bTagHisto->GetBinContent(bTagHisto->FindBin(1));
  N_obs_2btag = bTagHisto->GetBinContent(bTagHisto->FindBin(2));

  double likelihood_0btag = N_obs_0btag*log(N_hat_0btag)-N_hat_0btag;
  double likelihood_1btag = N_obs_1btag*log(N_hat_1btag)-N_hat_1btag;
  double likelihood_2btag = N_obs_2btag*log(N_hat_2btag)-N_hat_2btag;
  //likelihood_0btag = 0.;
  //likelihood_1btag = 0.;
  //likelihood_2btag = 0.;
  llh = likelihood_0btag+likelihood_1btag+likelihood_2btag;
  f = -llh;

  cerr<<"-llh	"<<f<<"	Rb	"<<par[0]<<endl;
  llh_gra->SetPoint(index_iter, par[0], f);
  index_iter++;
}

void fcnLLH_Rb_PE(int &npar, double *gin, double &f, double *par, int iflag) {
  double llh = 0;
  double P_0_2, P_1_2, P_2_2;
  P_2_2 = alpha_2*(pow(par[0]*epsilon_b,2) + 2*par[0]*(1-par[0])*epsilon_b*epsilon_q + pow((1-par[0])*epsilon_q,2))+
          alpha_1*(2*pow(par[0],2)*epsilon_b*epsilon_q+2*par[0]*(1-par[0])*(epsilon_b+epsilon_q)*epsilon_q+2*pow((1-par[0])*epsilon_q,2))+
	  alpha_0*(epsilon_q*epsilon_q);
  double N_hat_0btag, N_hat_1btag, N_hat_2btag;
  N_hat_0btag = njetsHisto->GetBinContent(njetsHisto->FindBin(2))*P_0_2;
  N_hat_1btag = njetsHisto->GetBinContent(njetsHisto->FindBin(2))*P_1_2;
  N_hat_2btag = njetsHisto->GetBinContent(njetsHisto->FindBin(2))*P_2_2;
  double N_obs_0btag, N_obs_1btag, N_obs_2btag;
  N_obs_0btag = bTagHisto->GetBinContent(bTagHisto->FindBin(0));
  N_obs_1btag = bTagHisto->GetBinContent(bTagHisto->FindBin(1));
  N_obs_2btag = bTagHisto->GetBinContent(bTagHisto->FindBin(2));
  double likelihood_0btag = N_obs_0btag*log(N_hat_0btag)-N_hat_0btag;
  double likelihood_1btag = N_obs_1btag*log(N_hat_1btag)-N_hat_1btag;
  double likelihood_2btag = N_obs_2btag*log(N_hat_2btag)-N_hat_2btag;
  likelihood_0btag = 0.;
  likelihood_1btag = 0.;
  llh = likelihood_0btag+likelihood_1btag+likelihood_2btag;
  f = -llh;
}

void fcnChi2_pTrel(int &npar, double *gin, double &f, double *par, int iflag) {
}

void fcnChi2_pTrel_PE(int &npar, double *gin, double &f, double *par, int iflag) {
}
