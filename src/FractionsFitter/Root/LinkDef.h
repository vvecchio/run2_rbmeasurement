//#include "FractionsFitter/Looper.h"
#include "FractionsFitter/FractionsFitter.h"
//#include "FractionsFitter/Channel.h"

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#endif

#ifdef __CINT__
//#pragma link C++ class Looper+;
#pragma link C++ class Fitter+;
//#pragma link C++ class Channel+;
#endif
