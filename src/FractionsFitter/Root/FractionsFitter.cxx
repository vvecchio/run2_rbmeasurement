#include "FractionsFitter/FractionsFitter.h"
#include "FractionsFitter/FractionsFitter.h"
#include <TMath.h>
#include <TGraph.h>

using namespace std;
double  startValue; 
int     doChi2Fit;
int     numberOfPE;
int     firstBinToFit = 1;
int     lastBinToFit = 100;
float *mc_fb_notop, *mc_fq_notop, *mc_f_top, *mc_f_notop, *data_jetpt;
float *mc_fb_notop_Err, *mc_fq_notop_Err, *mc_f_top_Err,*mc_f_notop_Err, *data_jetpt_Err;
void fcnLLH_Fb_notop(int &npar, double *gin, double &f, double *par, int iflag);
void fcnLLH_Fb_notop_twopar(int &npar, double *gin, double &f, double *par, int iflag);
void fcnLLH_Fb_notop_PE(int &npar, double *gin, double &f, double *par, int iflag);
void fcnChi2_pTrel(int &npar, double *gin, double &f, double *par, int iflag);
void fcnChi2_pTrel_PE(int &npar, double *gin, double &f, double *par, int iflag);
int iter = 10000;

FractionsFitter::FractionsFitter() {}
FractionsFitter::~FractionsFitter() {}

void FractionsFitter::fitToData(TH1F *dataHist, TH1F *fb_notop_Hist, TH1F *fq_notop_Hist, TH1F *f_top_Hist, int doChi2Fit, int forceFraction, float startValue, bool doThreepar, int first_bin, int last_bin) {
  
  fb_notop_Fitted = (TH1F*) fb_notop_Hist->Clone();
  fq_notop_Fitted = (TH1F*) fq_notop_Hist->Clone();
  f_notop_Fitted  = (TH1F*) fb_notop_Hist->Clone();
  f_top_Fitted 	  = (TH1F*) f_top_Hist->Clone();
  data  	  = (TH1F*) dataHist->Clone();

  setupArrays(doThreepar);
  
  if(doThreepar) {
  fillArray(dataHist, fb_notop_Hist, fq_notop_Hist, f_top_Hist, doThreepar);
  } else {
  fillArray(dataHist, f_notop_Fitted, fq_notop_Hist, f_top_Hist, doThreepar);
  }
 
  cerr<<"Sono nel fit"<<endl;
  cerr<<"f_notop_Fitted	"<<f_notop_Fitted->Integral()<<"	f_top_Fitted	"<<f_top_Fitted->Integral()<<"	data	"<<data->Integral()<<endl;

  TMinuit *myMinuit;

  firstBinToFit = first_bin;
  lastBinToFit = last_bin;
  doChi2Fit = 0;
  startValue = 0.001;

  // Three parameter Minuit
  myMinuit = new TMinuit(1); //arg is Int_t maxpar
  myMinuit->SetPrintLevel(1); //= -1 quiet (also suppress all warnings) = 0 normal = 1 verbose
  if(doThreepar){
  myMinuit->SetFCN(fcnLLH_Fb_notop);
  } else {
  myMinuit->SetFCN(fcnLLH_Fb_notop_twopar);
  }
  myMinuit->SetErrorDef(1); //SetErrorDef (Double_t up) To get the n-sigma contour the error def parameter "up" has to set to n^2. 

  int ierflag = 0;
  float startStepSize = 0.0001;
  float minValue = 0.0001;
  float maxValue = 100;
   
  if(doThreepar){
  myMinuit->mnparm(0, "Fb_notop",  0.01, startStepSize, minValue, maxValue, ierflag);
  myMinuit->mnparm(1, "Fq_notop",  0.01, startStepSize, minValue, maxValue, ierflag);
  myMinuit->mnparm(2, "F_top", 0.01, startStepSize, minValue, maxValue, ierflag);
  } else {
  myMinuit->mnparm(0, "F_notop",  0.01, startStepSize, minValue, maxValue, ierflag);
  //myMinuit->mnparm(1, "F_top",  0.01, startStepSize, minValue, maxValue, ierflag);
  }
  myMinuit->Migrad(); 
  myMinuit->mnmnos();
  myMinuit->SetMaxIterations(iter);
 
  double Fb_notop_fit, Fb_notop_fit_error;
  double Fq_notop_fit, Fq_notop_fit_error;
  double F_top_fit, F_top_fit_error;
  double F_notop_fit, F_notop_fit_error;
  if(doThreepar){
  myMinuit->GetParameter(0, Fb_notop_fit, Fb_notop_fit_error);
  myMinuit->GetParameter(1, Fq_notop_fit, Fq_notop_fit_error);
  myMinuit->GetParameter(2, F_top_fit, F_top_fit_error);
  } else {
  myMinuit->GetParameter(0, F_notop_fit, F_notop_fit_error);
  //myMinuit->GetParameter(1, F_top_fit, F_top_fit_error);
  F_top_fit = 1-F_notop_fit;
  F_top_fit_error = 0.;
  }

  if(doThreepar){
  fb_notop_Fitted->Scale(Fb_notop_fit*dataHist->Integral()/fb_notop_Fitted->Integral());
  fq_notop_Fitted->Scale(Fq_notop_fit*dataHist->Integral()/fq_notop_Fitted->Integral());
  f_top_Fitted->Scale(F_top_fit*dataHist->Integral()/f_top_Fitted->Integral());
  } else {
  f_notop_Fitted->Scale(F_notop_fit*dataHist->Integral()/f_notop_Fitted->Integral());
  f_top_Fitted->Scale(F_top_fit*dataHist->Integral()/f_top_Fitted->Integral());
  cerr<<"f_notop_Fitted	"<<f_notop_Fitted->Integral()<<endl;
  cerr<<"f_top_Fitted		"<<f_top_Fitted->Integral()<<endl;
  cerr<<"dataHist		"<<dataHist->Integral()<<endl;
  }
 

  if(doThreepar){
  fillFitParameterHists(dataHist, fb_notop_Fitted, fq_notop_Fitted, f_top_Fitted, Fb_notop_fit, Fb_notop_fit_error, Fq_notop_fit, Fq_notop_fit_error, F_top_fit, F_top_fit_error);
  } else {
  fillFitParameterHists_twopar(dataHist, f_notop_Fitted, f_top_Fitted, F_notop_fit, F_notop_fit_error,  F_top_fit, F_top_fit_error);
  }
  
  if (numberOfPE) {
    cout<<"Start pseudo experiments:"<<endl;
    //runPEs(numberOfPE, doChi2Fit, forceFraction, fitLCRatio);   
  } 

}
 
void FractionsFitter::setOutput(TString output) {
    Tools tool;
    TString ffile = "fitterOutput/"+output;
    cout << "\n -> Output file is =" << ffile << endl;
    RootFile = new TFile(ffile, "RECREATE", "FractionsFitter results file");
    RootFile->cd();
}

void FractionsFitter::setupArrays(bool doThreepar) {
  if(doThreepar){
  mc_fb_notop = (float*) calloc(lastBinToFit, sizeof(float));
  mc_fq_notop = (float*) calloc(lastBinToFit, sizeof(float)); 
  mc_f_top = (float*) calloc(lastBinToFit, sizeof(float));
  data_jetpt = (float*) calloc(lastBinToFit, sizeof(float));
  mc_fb_notop_Err = (float*) calloc(lastBinToFit, sizeof(float));
  mc_fq_notop_Err = (float*) calloc(lastBinToFit, sizeof(float)); 
  mc_f_top_Err = (float*) calloc(lastBinToFit, sizeof(float));
  data_jetpt_Err = (float*) calloc(lastBinToFit, sizeof(float));
  } else {
  mc_f_notop = (float*) calloc(lastBinToFit, sizeof(float));
  mc_f_top = (float*) calloc(lastBinToFit, sizeof(float));
  data_jetpt = (float*) calloc(lastBinToFit, sizeof(float));
  mc_f_notop_Err = (float*) calloc(lastBinToFit, sizeof(float));
  mc_f_top_Err = (float*) calloc(lastBinToFit, sizeof(float));
  data_jetpt_Err = (float*) calloc(lastBinToFit, sizeof(float));
  }
}

void FractionsFitter::setupParameters(int inputNumberOfPE, int inputFirstBinToFit, int inputLastBinToFit) {
}
 
void FractionsFitter::fillArray(TH1F *dataHist, TH1F *fb_notop_Hist, TH1F *fq_notop_Hist, TH1F *f_top_Hist, bool doThreepar) {
  if(doThreepar){
  for (int i = firstBinToFit; i <= lastBinToFit; i++) {
    mc_fb_notop[i - 1]   = fb_notop_Hist->GetBinContent(i);     
    mc_fq_notop[i - 1]   = fq_notop_Hist->GetBinContent(i);
    mc_f_top[i - 1] = f_top_Hist->GetBinContent(i);
    data_jetpt[i - 1]  = dataHist->GetBinContent(i);     
    mc_fb_notop_Err[i - 1]   = fb_notop_Hist->GetBinError(i);     
    mc_fq_notop_Err[i - 1]   = fq_notop_Hist->GetBinError(i);
    mc_f_top_Err[i - 1] = f_top_Hist->GetBinError(i);
    data_jetpt_Err[i - 1]  = dataHist->GetBinError(i);     
  }
  }else{
  TH1F* f_notop_Hist = fb_notop_Hist;
  for (int i = firstBinToFit; i <= lastBinToFit; i++) {
    mc_f_notop[i - 1] = f_notop_Hist->GetBinContent(i);
    mc_f_top[i - 1] = f_top_Hist->GetBinContent(i);
    data_jetpt[i - 1]  = dataHist->GetBinContent(i);     
    mc_f_notop_Err[i - 1] = f_notop_Hist->GetBinError(i);
    mc_f_top_Err[i - 1] = f_top_Hist->GetBinError(i);
    data_jetpt_Err[i - 1]  = dataHist->GetBinError(i);     
  }
  }
} 

void FractionsFitter::runPEs(int numberOfPEs, int doChi2Fit, int forceFraction, int fitLCRatio) {
}

void FractionsFitter::fillFitParameterHists_twopar(TH1F *dataHist, TH1F *fb_notop_Hist, TH1F *f_top_Hist, double b_frac, double b_frac_err, double l_frac, double l_frac_err) {
  TH1F *FittedParameters = new TH1F("fitted_parameters", "fitted_parameters", 8, 0, 8);
  TH1F *FittedHist = (TH1F*) fb_notop_Hist->Clone();
  FittedHist->SetName("Leading_jet_pt_fit");
  FittedHist->Add(f_top_Hist);
  FittedParameters->GetXaxis()->SetBinLabel(1, "f_notop"); 
  FittedParameters->SetBinContent(1, b_frac); 
  FittedParameters->GetXaxis()->SetBinLabel(2, "f_notop_error"); 
  FittedParameters->SetBinContent(2, b_frac_err); 
  FittedParameters->GetXaxis()->SetBinLabel(3, "f_top"); 
  FittedParameters->SetBinContent(3, l_frac); 
  FittedParameters->GetXaxis()->SetBinLabel(4, "f_top_error"); 
  FittedParameters->SetBinContent(4, l_frac_err); 
  FittedParameters->GetXaxis()->SetBinLabel(5, "chi2"); 
  FittedParameters->GetXaxis()->SetBinLabel(6, "nDOF"); 
  FittedParameters->GetXaxis()->SetBinLabel(7, "chi2/nDOF"); 
  FittedParameters->GetXaxis()->SetBinLabel(8, "illh");
  //Calculate chi2
  double chi2 = 0;
  for (int i = firstBinToFit; i <= lastBinToFit; i++) {
    double chi2Tmp = pow((dataHist->GetBinContent(i) - fb_notop_Hist->GetBinContent(i) - f_top_Hist->GetBinContent(i)),2);        
    chi2Tmp = chi2Tmp/(fb_notop_Hist->GetBinContent(i) + f_top_Hist->GetBinContent(i));
    chi2 += chi2Tmp;
  }
  FittedParameters->SetBinContent(5, chi2); 
  FittedParameters->SetBinContent(6, lastBinToFit - firstBinToFit + 1); 
  FittedParameters->SetBinContent(7, chi2/(lastBinToFit - firstBinToFit + 1)); 
  cout<<"Chi2/nDOF is: "<<chi2/(lastBinToFit - firstBinToFit + 1)<<endl;

  //Calculate llh
  double llh = 0;
  for (int i = firstBinToFit; i <= lastBinToFit; i++) {
    double dataPoint = dataHist->GetBinContent(i);
    double mcPoint = fb_notop_Hist->GetBinContent(i) + f_top_Hist->GetBinContent(i);
    llh += dataPoint * log(mcPoint) - TMath::LnGamma(dataPoint + 1) - mcPoint;
  }
  FittedParameters->SetBinContent(8, llh); 
} 

void FractionsFitter::fillFitParameterHists(TH1F *dataHist, TH1F *fb_notop_Hist, TH1F *fq_notop_Hist, TH1F *f_top_Hist, double b_frac, double b_frac_err, double c_frac, double c_frac_err, double l_frac, double l_frac_err) {
  TH1F *FittedParameters = new TH1F("fitted_parameters", "fitted_parameters", 8, 0, 8);
  TH1F *FittedHist = (TH1F*) fb_notop_Hist->Clone();
  FittedHist->SetName("Leading_jet_pt_fit");
  FittedHist->Add(fq_notop_Hist);
  FittedHist->Add(f_top_Hist);
  FittedParameters->GetXaxis()->SetBinLabel(1, "fb_notop"); 
  FittedParameters->SetBinContent(1, b_frac); 
  FittedParameters->GetXaxis()->SetBinLabel(2, "fb_notop_error"); 
  FittedParameters->SetBinContent(2, b_frac_err); 
  FittedParameters->GetXaxis()->SetBinLabel(3, "fq_notop"); 
  FittedParameters->SetBinContent(3, c_frac); 
  FittedParameters->GetXaxis()->SetBinLabel(4, "fq_notop_error"); 
  FittedParameters->SetBinContent(4, c_frac_err); 
  FittedParameters->GetXaxis()->SetBinLabel(5, "f_top"); 
  FittedParameters->SetBinContent(5, l_frac); 
  FittedParameters->GetXaxis()->SetBinLabel(6, "f_top_error"); 
  FittedParameters->SetBinContent(6, l_frac_err); 
  FittedParameters->GetXaxis()->SetBinLabel(7, "chi2"); 
  FittedParameters->GetXaxis()->SetBinLabel(8, "nDOF"); 
  FittedParameters->GetXaxis()->SetBinLabel(9, "chi2/nDOF"); 
  FittedParameters->GetXaxis()->SetBinLabel(10, "illh");
  //Calculate chi2
  double chi2 = 0;
  for (int i = firstBinToFit; i <= lastBinToFit; i++) {
    double chi2Tmp = pow((dataHist->GetBinContent(i) - fb_notop_Hist->GetBinContent(i) - fq_notop_Hist->GetBinContent(i) - f_top_Hist->GetBinContent(i)),2);        
    chi2Tmp = chi2Tmp/(fb_notop_Hist->GetBinContent(i) + fq_notop_Hist->GetBinContent(i) + f_top_Hist->GetBinContent(i));
    chi2 += chi2Tmp;
  }
  FittedParameters->SetBinContent(7, chi2); 
  FittedParameters->SetBinContent(8, lastBinToFit - firstBinToFit + 1); 
  FittedParameters->SetBinContent(9, chi2/(lastBinToFit - firstBinToFit + 1)); 
  cout<<"Chi2/nDOF is: "<<chi2/(lastBinToFit - firstBinToFit + 1)<<endl;

  //Calculate llh
  double llh = 0;
  for (int i = firstBinToFit; i <= lastBinToFit; i++) {
    double dataPoint = dataHist->GetBinContent(i);
    double mcPoint = fb_notop_Hist->GetBinContent(i) + fq_notop_Hist->GetBinContent(i) + f_top_Hist->GetBinContent(i);
    llh += dataPoint * log(mcPoint) - TMath::LnGamma(dataPoint + 1) - mcPoint;
  }
  FittedParameters->SetBinContent(10, llh); 
} 

void FractionsFitter::finalize() {
  RootFile->Write();
  RootFile->Close();
}

void fcnLLH_Fb_notop(int &npar, double *gin, double &f, double *par, int iflag) {
  double llh = 0;
  for (int i = firstBinToFit; i <= lastBinToFit; i++) {
    double mc_pTrel;
    //mc_pTrel = par[0] * mc_fb_notop[i - 1] + par[1] * mc_fq_notop[i - 1] + (1 - par[0] - par[1])  * mc_f_top[i - 1];
    mc_pTrel = par[0] * mc_fb_notop[i - 1] + par[1] * mc_fq_notop[i - 1] + par[2]  * mc_f_top[i - 1];
    if (mc_pTrel < 0.0001) mc_pTrel = 0.0001;
      llh += data_jetpt[i - 1] * log(mc_pTrel) - mc_pTrel;
      //Log likelihood from ROOT documentation
      //llh += data_jetpt[i - 1] * log(mc_pTrel) - TMath::LnGamma(data_jetpt[i - 1] + 1) - mc_pTrel;
  }
  f = -llh;
}
void fcnLLH_Fb_notop_twopar(int &npar, double *gin, double &f, double *par, int iflag) {
  double llh = 0;
  for (int i = firstBinToFit; i <= lastBinToFit; i++) {
    double mc_pTrel;
    //mc_pTrel = par[0] * mc_f_notop[i - 1] + par[1] * mc_f_top[i - 1];
    mc_pTrel = par[0] * mc_f_notop[i - 1] + (1-par[0])* mc_f_top[i - 1];
    if (mc_pTrel < 0.0001) mc_pTrel = 0.0001;
      llh += data_jetpt[i - 1] * log(mc_pTrel) - mc_pTrel;
      //Log likelihood from ROOT documentation
      //llh += data_jetpt[i - 1] * log(mc_pTrel) - TMath::LnGamma(data_jetpt[i - 1] + 1) - mc_pTrel;
  }
  f = -llh;
}

void fcnLLH_Fb_notop_PE(int &npar, double *gin, double &f, double *par, int iflag) {
}

void fcnChi2_pTrel(int &npar, double *gin, double &f, double *par, int iflag) {
}

void fcnChi2_pTrel_PE(int &npar, double *gin, double &f, double *par, int iflag) {
}
