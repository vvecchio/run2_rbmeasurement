#ifndef _FractionsFitter_H_
#define _FractionsFitter_H_

#include <iostream>
#include "TMinuit.h"
#include "TRandom3.h"
#include "TFile.h"
#include "TH1.h"
#include "TString.h"
#include "math.h"


using namespace std;
// Header file for the classes stored in the TTree if any.
class FractionsFitter {
    public:
      FractionsFitter();
      TRandom3 *randGen;
      TFile *RootFile;
      void fitToData(TH1F *dataHist, TH1F *fb_notop_Hist, TH1F *fq_notop_Hist, TH1F *f_top_Hist, int doChi2Fit, int forceFraction, float startValue, bool doThreepar, int first_bin, int last_bin);
      //void fitToData(TH1F *dataBTagHist, TH1F *dataNjetsHist, double alpha_value[3], double epsilon_b_value, double epsilon_q_value);
      TH1F *data;
      TH1F *fb_notop_Fitted;
      TH1F *fq_notop_Fitted;
      TH1F *f_top_Fitted;
      TH1F *f_notop_Fitted;
      //TH1F *bFracFitted;
      //TH1F *cFracFitted;
      //TH1F *lFracFitted;       
      TH1F *FittedParameters;       
      TH1F *FittedHist;
      void setOutput(TString output);
      void setupArrays(bool doThreePar); 
      void runPEs(int numberOfPEs, int doChi2Fit, int forceFraction, int fitLCRatio);
      void setupParameters(int inputNumberOfPE, int inputFirstBinToFit, int inputLastBinToFit);
      void fillArray(TH1F *dataHist, TH1F *bHist, TH1F *cHist, TH1F *lHist, bool doThreePar);  
      //void fillFitParameterHists(TH1F *bTagHisto, TH1F* njetsHisto, double alpha_0, double alpha_1, double alpha_2, double epsilon_b, double epsilon_q, double Rb_fit, double Rb_fit_err);
      void fillFitParameterHists_twopar(TH1F *dataHist, TH1F *fb_notop_Hist, TH1F *f_top_Hist, double b_frac, double b_frac_err, double l_frac, double l_frac_err);
      void fillFitParameterHists(TH1F *dataHist, TH1F *fb_notop_Hist, TH1F *fq_notop_Hist, TH1F *f_top_Hist, double b_frac, double b_frac_err, double c_frac, double c_frac_err, double l_frac, double l_frac_err);
      void finalize();
      ~FractionsFitter();
};

class Tools {
  public:
    vector<string> getStringSegments(string input, char delim);
    Tools(){};
    virtual ~Tools(){};
};
#endif
