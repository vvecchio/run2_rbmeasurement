#include "FractionsFitter/FractionsFitter.h"
#include<iostream>
#include<fstream>
using namespace std;

int main(int argc, char **argv)
{
  if(argc < 2) {
    printf("More than two arguments are needed, aborting!\n");
    exit(0);
  }
 
  char * inputConfig = argv[1];
  string inputConfigName = string(inputConfig);
  cout<<"Input config file: "<<inputConfigName<<endl; 

  char * tree = argv[2];
  TString treename = string(tree);
  cerr<<"Fitting on treename: "<<treename<<endl;
  
  ifstream inputConfigFile;
  inputConfigFile.open(inputConfigName.c_str(), ios::in);

  /////////////////////////////////////////////////////////
  //  Get values and info from the external config file  //
  /////////////////////////////////////////////////////////
  string message;
  TString dataFileName;
  TString mcFileName;
  int doChi2Fit;
  int forceFraction;
  double startValue;
  int numberOfPE;
  int firstBinToFit;
  int lastBinToFit;
  string dataHistPath; 
  string fbnotopHistPath; 
  string fqnotopHistPath; 
  string ftopHistPath; 
  TString outputFileName; 
  bool doThreepar;
 
  inputConfigFile >> message >> dataFileName;
  inputConfigFile >> message >> mcFileName;
  inputConfigFile >> message >> dataHistPath;
  inputConfigFile >> message >> fbnotopHistPath;
  inputConfigFile >> message >> fqnotopHistPath;
  inputConfigFile >> message >> ftopHistPath;
  inputConfigFile >> message >> outputFileName;
  inputConfigFile >> message >> doChi2Fit;
  inputConfigFile >> message >> forceFraction;
  inputConfigFile >> message >> startValue;
  inputConfigFile >> message >> numberOfPE;
  inputConfigFile >> message >> firstBinToFit;
  inputConfigFile >> message >> lastBinToFit;
  inputConfigFile >> message >> doThreepar;

  TFile dataInputFile("/storage/local/home/atlas/vvecchio/RbMeaurement/RunLooper/"+dataFileName);
  TFile mcInputFile("/storage/local/home/atlas/vvecchio/RbMeaurement/RunLooper/"+dataFileName);
  //TFile mcInputFile("/storage/local/home/atlas/vvecchio/RbMeaurement/RunLooper/");
  //cerr<<"dataInputFile	"<<dataInputFile.GetName()<<endl;
 
  TH1F* dataHist       = (TH1F*)dataInputFile.Get(dataHistPath.c_str())->Clone();
  TH1F* fbnotopHist    = (TH1F*)mcInputFile.Get(fbnotopHistPath.c_str())->Clone();
  TH1F* fqnotopHist    = (TH1F*)mcInputFile.Get(fqnotopHistPath.c_str())->Clone();
  TH1F* ftopHist       = (TH1F*)mcInputFile.Get(ftopHistPath.c_str())->Clone();

  if(doThreepar){
  fbnotopHist->Scale(dataHist->Integral()/fbnotopHist->Integral()); 
  fqnotopHist->Scale(dataHist->Integral()/fqnotopHist->Integral()); 
  ftopHist->Scale(dataHist->Integral()/ftopHist->Integral()); 
  } else {
  //fbnotopHist->Add(fqnotopHist);
  fbnotopHist->Scale(dataHist->Integral()/fbnotopHist->Integral());
  ftopHist->Scale(dataHist->Integral()/ftopHist->Integral());
  cerr<<"fbnotopHist	"<<fbnotopHist->Integral()<<"	ftopHist	"<<ftopHist->Integral()<<"	dataHist	"<<dataHist->Integral()<<endl;
  }
  
  FractionsFitter fitter;
  fitter.setupParameters(numberOfPE, firstBinToFit, lastBinToFit);
  fitter.setOutput(outputFileName+"_"+treename+".root");
  fitter.fitToData(dataHist, fbnotopHist, fqnotopHist, ftopHist, doChi2Fit, forceFraction, startValue, doThreepar, firstBinToFit, lastBinToFit);
  fitter.finalize(); 
  
  return 0;
}
