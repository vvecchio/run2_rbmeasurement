//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Jan 19 19:39:08 2016 by ROOT version 6.02/12
// from TTree nominal/tree
// found on file: 410009.root
//////////////////////////////////////////////////////////

#ifndef Looper_h
#define Looper_h

#include <TROOT.h>
#include <TChain.h>
#include <TTreeIndex.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TH3F.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <TLorentzVector.h>
using std::cout;
using std::endl;
using std::string;
using std::stringstream;
using std::vector;

class Looper {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   vector<float>   *mc_generator_weights;
   Float_t         weight_mc;
   Float_t         weight_pileup;
   Float_t         weight_leptonSF;
   Float_t         weight_globalLeptonTriggerSF;
   Float_t         weight_oldTriggerSF;
   Float_t         weight_bTagSF_MV2c10_77;
   Float_t         weight_jvt;
   Float_t         weight_pileup_UP;
   Float_t         weight_pileup_DOWN;
   Float_t         weight_leptonSF_EL_SF_Trigger_UP;
   Float_t         weight_leptonSF_EL_SF_Trigger_DOWN;
   Float_t         weight_leptonSF_EL_SF_Reco_UP;
   Float_t         weight_leptonSF_EL_SF_Reco_DOWN;
   Float_t         weight_leptonSF_EL_SF_ID_UP;
   Float_t         weight_leptonSF_EL_SF_ID_DOWN;
   Float_t         weight_leptonSF_EL_SF_Isol_UP;
   Float_t         weight_leptonSF_EL_SF_Isol_DOWN;
   Float_t         weight_leptonSF_MU_SF_Trigger_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_Trigger_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_Trigger_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_Trigger_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN;
   Float_t         weight_leptonSF_MU_SF_Isol_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_Isol_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_Isol_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_Isol_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_TTVA_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_TTVA_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_TTVA_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_TTVA_SYST_DOWN;
   Float_t         weight_globalLeptonTriggerSF_EL_Trigger_UP;
   Float_t         weight_globalLeptonTriggerSF_EL_Trigger_DOWN;
   Float_t         weight_globalLeptonTriggerSF_MU_Trigger_STAT_UP;
   Float_t         weight_globalLeptonTriggerSF_MU_Trigger_STAT_DOWN;
   Float_t         weight_globalLeptonTriggerSF_MU_Trigger_SYST_UP;
   Float_t         weight_globalLeptonTriggerSF_MU_Trigger_SYST_DOWN;
   Float_t         weight_oldTriggerSF_EL_Trigger_UP;
   Float_t         weight_oldTriggerSF_EL_Trigger_DOWN;
   Float_t         weight_oldTriggerSF_MU_Trigger_STAT_UP;
   Float_t         weight_oldTriggerSF_MU_Trigger_STAT_DOWN;
   Float_t         weight_oldTriggerSF_MU_Trigger_SYST_UP;
   Float_t         weight_oldTriggerSF_MU_Trigger_SYST_DOWN;
   Float_t         weight_indiv_SF_EL_Reco;
   Float_t         weight_indiv_SF_EL_Reco_UP;
   Float_t         weight_indiv_SF_EL_Reco_DOWN;
   Float_t         weight_indiv_SF_EL_ID;
   Float_t         weight_indiv_SF_EL_ID_UP;
   Float_t         weight_indiv_SF_EL_ID_DOWN;
   Float_t         weight_indiv_SF_EL_Isol;
   Float_t         weight_indiv_SF_EL_Isol_UP;
   Float_t         weight_indiv_SF_EL_Isol_DOWN;
   Float_t         weight_indiv_SF_EL_ChargeID;
   Float_t         weight_indiv_SF_EL_ChargeID_UP;
   Float_t         weight_indiv_SF_EL_ChargeID_DOWN;
   Float_t         weight_indiv_SF_EL_ChargeMisID;
   Float_t         weight_indiv_SF_EL_ChargeMisID_STAT_UP;
   Float_t         weight_indiv_SF_EL_ChargeMisID_STAT_DOWN;
   Float_t         weight_indiv_SF_EL_ChargeMisID_SYST_UP;
   Float_t         weight_indiv_SF_EL_ChargeMisID_SYST_DOWN;
   Float_t         weight_indiv_SF_MU_ID;
   Float_t         weight_indiv_SF_MU_ID_STAT_UP;
   Float_t         weight_indiv_SF_MU_ID_STAT_DOWN;
   Float_t         weight_indiv_SF_MU_ID_SYST_UP;
   Float_t         weight_indiv_SF_MU_ID_SYST_DOWN;
   Float_t         weight_indiv_SF_MU_ID_STAT_LOWPT_UP;
   Float_t         weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN;
   Float_t         weight_indiv_SF_MU_ID_SYST_LOWPT_UP;
   Float_t         weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN;
   Float_t         weight_indiv_SF_MU_Isol;
   Float_t         weight_indiv_SF_MU_Isol_STAT_UP;
   Float_t         weight_indiv_SF_MU_Isol_STAT_DOWN;
   Float_t         weight_indiv_SF_MU_Isol_SYST_UP;
   Float_t         weight_indiv_SF_MU_Isol_SYST_DOWN;
   Float_t         weight_indiv_SF_MU_TTVA;
   Float_t         weight_indiv_SF_MU_TTVA_STAT_UP;
   Float_t         weight_indiv_SF_MU_TTVA_STAT_DOWN;
   Float_t         weight_indiv_SF_MU_TTVA_SYST_UP;
   Float_t         weight_indiv_SF_MU_TTVA_SYST_DOWN;
   Float_t         weight_jvt_UP;
   Float_t         weight_jvt_DOWN;
   vector<float>   *weight_bTagSF_MV2c10_77_eigenvars_B_up;
   vector<float>   *weight_bTagSF_MV2c10_77_eigenvars_C_up;
   vector<float>   *weight_bTagSF_MV2c10_77_eigenvars_Light_up;
   vector<float>   *weight_bTagSF_MV2c10_77_eigenvars_B_down;
   vector<float>   *weight_bTagSF_MV2c10_77_eigenvars_C_down;
   vector<float>   *weight_bTagSF_MV2c10_77_eigenvars_Light_down;
   Float_t         weight_bTagSF_MV2c10_77_extrapolation_up;
   Float_t         weight_bTagSF_MV2c10_77_extrapolation_down;
   Float_t         weight_bTagSF_MV2c10_77_extrapolation_from_charm_up;
   Float_t         weight_bTagSF_MV2c10_77_extrapolation_from_charm_down;
   vector<int>     *weight_poisson;
   ULong64_t       eventNumber;
   UInt_t          runNumber;
   UInt_t          randomRunNumber;
   UInt_t          mcChannelNumber;
   Float_t         mu;
   UInt_t          backgroundFlags;
   UInt_t          hasBadMuon;
   vector<float>   *el_pt;
   vector<float>   *el_eta;
   vector<float>   *el_cl_eta;
   vector<float>   *el_phi;
   vector<float>   *el_e;
   vector<float>   *el_charge;
   vector<float>   *el_topoetcone20;
   vector<float>   *el_ptvarcone20;
   vector<char>    *el_CF;
   vector<float>   *el_d0sig;
   vector<float>   *el_delta_z0_sintheta;
   vector<int>     *el_true_type;
   vector<int>     *el_true_origin;
   vector<int>     *el_true_firstEgMotherTruthType;
   vector<int>     *el_true_firstEgMotherTruthOrigin;
   vector<int>     *el_true_firstEgMotherPdgId;
   vector<char>    *el_true_isPrompt;
   vector<char>    *el_true_isChargeFl;
   vector<float>   *mu_pt;
   vector<float>   *mu_eta;
   vector<float>   *mu_phi;
   vector<float>   *mu_e;
   vector<float>   *mu_charge;
   vector<float>   *mu_topoetcone20;
   vector<float>   *mu_ptvarcone30;
   vector<float>   *mu_d0sig;
   vector<float>   *mu_delta_z0_sintheta;
   vector<int>     *mu_true_type;
   vector<int>     *mu_true_origin;
   vector<char>    *mu_true_isPrompt;
   vector<float>   *jet_pt;
   vector<float>   *jet_eta;
   vector<float>   *jet_phi;
   vector<float>   *jet_e;
   vector<float>   *jet_mv2c00;
   vector<float>   *jet_mv2c10;
   vector<float>   *jet_mv2c20;
   vector<float>   *jet_ip3dsv1;
   vector<float>   *jet_jvt;
   vector<char>    *jet_passfjvt;
   vector<int>     *jet_truthflav;
   vector<int>     *jet_truthPartonLabel;
   vector<char>    *jet_isTrueHS;
   vector<int>     *jet_truthflavExtended;
   vector<char>    *jet_isbtagged_MV2c10_77;
   vector<float>   *jet_MV2r;
   vector<float>   *jet_MV2rmu;
   vector<float>   *jet_DL1;
   vector<float>   *jet_DL1r;
   vector<float>   *jet_DL1rmu;
   vector<float>   *jet_MV2cl100;
   vector<float>   *jet_MV2c100;
   vector<float>   *jet_DL1_pu;
   vector<float>   *jet_DL1_pc;
   vector<float>   *jet_DL1_pb;
   vector<float>   *jet_DL1r_pu;
   vector<float>   *jet_DL1r_pc;
   vector<float>   *jet_DL1r_pb;
   vector<float>   *jet_DL1rmu_pu;
   vector<float>   *jet_DL1rmu_pc;
   vector<float>   *jet_DL1rmu_pb;
   Float_t         met_met;
   Float_t         met_phi;
   Int_t           emu_2015;
   Int_t           emu_2016;
   Int_t           emu_2017;
   Int_t           emu_2018;
   Int_t           emu_particle;
   Int_t           ee_2015;
   Int_t           ee_2016;
   Int_t           ee_particle;
   Int_t           mumu_2015;
   Int_t           mumu_2016;
   Int_t           mumu_particle;
   vector<int>     *muon_truth_pdgid;
   vector<int>     *el_truth_pdgid;

   // List of branches
   TBranch        *b_mc_generator_weights;   //!
   TBranch        *b_weight_mc;   //!
   TBranch        *b_weight_pileup;   //!
   TBranch        *b_weight_leptonSF;   //!
   TBranch        *b_weight_globalLeptonTriggerSF;   //!
   TBranch        *b_weight_oldTriggerSF;   //!
   TBranch        *b_weight_bTagSF_MV2c10_77;   //!
   TBranch        *b_weight_jvt;   //!
   TBranch        *b_weight_pileup_UP;   //!
   TBranch        *b_weight_pileup_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Trigger_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Trigger_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Reco_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Reco_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_ID_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_ID_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Isol_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Isol_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_SYST_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_SYST_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_SYST_DOWN;   //!
   TBranch        *b_weight_globalLeptonTriggerSF_EL_Trigger_UP;   //!
   TBranch        *b_weight_globalLeptonTriggerSF_EL_Trigger_DOWN;   //!
   TBranch        *b_weight_globalLeptonTriggerSF_MU_Trigger_STAT_UP;   //!
   TBranch        *b_weight_globalLeptonTriggerSF_MU_Trigger_STAT_DOWN;   //!
   TBranch        *b_weight_globalLeptonTriggerSF_MU_Trigger_SYST_UP;   //!
   TBranch        *b_weight_globalLeptonTriggerSF_MU_Trigger_SYST_DOWN;   //!
   TBranch        *b_weight_oldTriggerSF_EL_Trigger_UP;   //!
   TBranch        *b_weight_oldTriggerSF_EL_Trigger_DOWN;   //!
   TBranch        *b_weight_oldTriggerSF_MU_Trigger_STAT_UP;   //!
   TBranch        *b_weight_oldTriggerSF_MU_Trigger_STAT_DOWN;   //!
   TBranch        *b_weight_oldTriggerSF_MU_Trigger_SYST_UP;   //!
   TBranch        *b_weight_oldTriggerSF_MU_Trigger_SYST_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_Reco;   //!
   TBranch        *b_weight_indiv_SF_EL_Reco_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_Reco_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_ID;   //!
   TBranch        *b_weight_indiv_SF_EL_ID_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_ID_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_Isol;   //!
   TBranch        *b_weight_indiv_SF_EL_Isol_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_Isol_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeID;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeID_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeID_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeMisID;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeMisID_STAT_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeMisID_STAT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeMisID_SYST_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeMisID_SYST_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_ID;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_STAT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_STAT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_SYST_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_SYST_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_STAT_LOWPT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_SYST_LOWPT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_Isol;   //!
   TBranch        *b_weight_indiv_SF_MU_Isol_STAT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_Isol_STAT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_Isol_SYST_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_Isol_SYST_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_TTVA;   //!
   TBranch        *b_weight_indiv_SF_MU_TTVA_STAT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_TTVA_STAT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_TTVA_SYST_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_TTVA_SYST_DOWN;   //!
   TBranch        *b_weight_jvt_UP;   //!
   TBranch        *b_weight_jvt_DOWN;   //!
   TBranch        *b_weight_bTagSF_MV2c10_77_eigenvars_B_up;   //!
   TBranch        *b_weight_bTagSF_MV2c10_77_eigenvars_C_up;   //!
   TBranch        *b_weight_bTagSF_MV2c10_77_eigenvars_Light_up;   //!
   TBranch        *b_weight_bTagSF_MV2c10_77_eigenvars_B_down;   //!
   TBranch        *b_weight_bTagSF_MV2c10_77_eigenvars_C_down;   //!
   TBranch        *b_weight_bTagSF_MV2c10_77_eigenvars_Light_down;   //!
   TBranch        *b_weight_bTagSF_MV2c10_77_extrapolation_up;   //!
   TBranch        *b_weight_bTagSF_MV2c10_77_extrapolation_down;   //!
   TBranch        *b_weight_bTagSF_MV2c10_77_extrapolation_from_charm_up;   //!
   TBranch        *b_weight_bTagSF_MV2c10_77_extrapolation_from_charm_down;   //!
   TBranch        *b_weight_poisson;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_randomRunNumber;   //!
   TBranch        *b_mcChannelNumber;   //!
   TBranch        *b_mu;   //!
   TBranch        *b_backgroundFlags;   //!
   TBranch        *b_hasBadMuon;   //!
   TBranch        *b_el_pt;   //!
   TBranch        *b_el_eta;   //!
   TBranch        *b_el_cl_eta;   //!
   TBranch        *b_el_phi;   //!
   TBranch        *b_el_e;   //!
   TBranch        *b_el_charge;   //!
   TBranch        *b_el_topoetcone20;   //!
   TBranch        *b_el_ptvarcone20;   //!
   TBranch        *b_el_CF;   //!
   TBranch        *b_el_d0sig;   //!
   TBranch        *b_el_delta_z0_sintheta;   //!
   TBranch        *b_el_true_type;   //!
   TBranch        *b_el_true_origin;   //!
   TBranch        *b_el_true_firstEgMotherTruthType;   //!
   TBranch        *b_el_true_firstEgMotherTruthOrigin;   //!
   TBranch        *b_el_true_firstEgMotherPdgId;   //!
   TBranch        *b_el_true_isPrompt;   //!
   TBranch        *b_el_true_isChargeFl;   //!
   TBranch        *b_mu_pt;   //!
   TBranch        *b_mu_eta;   //!
   TBranch        *b_mu_phi;   //!
   TBranch        *b_mu_e;   //!
   TBranch        *b_mu_charge;   //!
   TBranch        *b_mu_topoetcone20;   //!
   TBranch        *b_mu_ptvarcone30;   //!
   TBranch        *b_mu_d0sig;   //!
   TBranch        *b_mu_delta_z0_sintheta;   //!
   TBranch        *b_mu_true_type;   //!
   TBranch        *b_mu_true_origin;   //!
   TBranch        *b_mu_true_isPrompt;   //!
   TBranch        *b_jet_pt;   //!
   TBranch        *b_jet_eta;   //!
   TBranch        *b_jet_phi;   //!
   TBranch        *b_jet_e;   //!
   TBranch        *b_jet_mv2c00;   //!
   TBranch        *b_jet_mv2c10;   //!
   TBranch        *b_jet_mv2c20;   //!
   TBranch        *b_jet_ip3dsv1;   //!
   TBranch        *b_jet_jvt;   //!
   TBranch        *b_jet_passfjvt;   //!
   TBranch        *b_jet_truthflav;   //!
   TBranch        *b_jet_truthPartonLabel;   //!
   TBranch        *b_jet_isTrueHS;   //!
   TBranch        *b_jet_truthflavExtended;   //!
   TBranch        *b_jet_isbtagged_MV2c10_77;   //!
   TBranch        *b_jet_MV2r;   //!
   TBranch        *b_jet_MV2rmu;   //!
   TBranch        *b_jet_DL1;   //!
   TBranch        *b_jet_DL1r;   //!
   TBranch        *b_jet_DL1rmu;   //!
   TBranch        *b_jet_MV2cl100;   //!
   TBranch        *b_jet_MV2c100;   //!
   TBranch        *b_jet_DL1_pu;   //!
   TBranch        *b_jet_DL1_pc;   //!
   TBranch        *b_jet_DL1_pb;   //!
   TBranch        *b_jet_DL1r_pu;   //!
   TBranch        *b_jet_DL1r_pc;   //!
   TBranch        *b_jet_DL1r_pb;   //!
   TBranch        *b_jet_DL1rmu_pu;   //!
   TBranch        *b_jet_DL1rmu_pc;   //!
   TBranch        *b_jet_DL1rmu_pb;   //!
   TBranch        *b_met_met;   //!
   TBranch        *b_met_phi;   //!
   TBranch        *b_emu_2015;   //!
   TBranch        *b_emu_2016;   //!
   TBranch        *b_emu_2017;   //!
   TBranch        *b_emu_2018;   //!
   TBranch        *b_emu_particle;   //!
   TBranch        *b_ee_2015;   //!
   TBranch        *b_ee_2016;   //!
   TBranch        *b_ee_particle;   //!
   TBranch        *b_mumu_2015;   //!
   TBranch        *b_mumu_2016;   //!
   TBranch        *b_mumu_particle;   //!
   TBranch        *b_muon_truth_pdgid;   //!
   TBranch        *b_el_truth_pdgid;   //!
   //Members of this class
   int m_sampleType;
   int m_index_name;
   TString m_TreeName;
   TString m_inputFileListName;
   int m_debugLevel;
   int current_dsid;
   int m_isMC16a, m_isMC16d, m_isMC16e;

   Looper(const int& sample, int index_name, TString inputFileListName, TString TreeName, int isMC16a, int isMC16d, int isMC16e);
   virtual ~Looper();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop(TString region, TString OS_or_SS);
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
   void		    CustomAddChain( TChain *chain, TString name_list_dsid );
   void		    CustomAddChain_Truth( TChain *chain, TString name_list_dsid );
   
   //my methods                                                                                   
   virtual void     SaveMyHistos(vector<TH1F*> vect_1d, vector<TH2F*> vect_2d, vector<TH3F*> vector_3d, int dsid, int indexname, TString treenamesyst, int isMC16a, int isMC16d, int isMC16e, TString region, TString OS_or_SS);
   virtual void     histos_sumw2(vector<TH1F*> vect_1d, vector<TH2F*> vect_2d, vector<TH3F*> vector_3d);
   virtual void     histos_clear(vector<TH1F*> vect_1d, vector<TH2F*> vect_2d, vector<TH3F*> vector_3d);
   TLorentzVector   RecoJet_TL(UInt_t RecoJet_idx);
   TLorentzVector   RecoEle_TL(UInt_t RecoEle_idx);
   TLorentzVector   RecoMu_TL(UInt_t RecoMu_idx);
   TLorentzVector   Rotated_Lep(TLorentzVector lep);
   bool             Pass_Goodemu();
   vector<int>      Pass_GoodJets();
   vector<int>      Pass_GoodEle();
   vector<int>      Pass_GoodMu();
   Double_t         xsecWeight(Int_t dsid, int isMC16a, int isMC16d, int isMC16e, TString treename);
   TTree*	    SetTruthTree(int sampleType);  
   TLorentzVector   Return_PreviousEventJet(vector<int> GoodJets);
   void             DeltaRMatch_GoodJet(TLorentzVector obj_to_match, int *num_of_match, double *deltaR, UInt_t *goodjet_idx);
   double	    ReturnBJets_ScaleFactor(double jet_pt,  bool isNominal, bool isUp, bool isDown);
   double	    ReturnBJets_EffMC(double jet_pt);
   double	    ReturnCJets_ScaleFactor(double jet_pt,  bool isNominal, bool isUp, bool isDown);
   double	    ReturnCJets_EffMC(double jet_pt);
   double	    ReturnLJets_ScaleFactor(double jet_pt,  bool isNominal, bool isUp, bool isDown);
   double	    ReturnLJets_EffMC(double jet_pt);
   double 	    ReturnFTAG_ScaleFactor(TString TREENAME_for_FTAG);
   double 	    Retrieve_ttbar_nominal_sampleNEvt(bool isMC16a, bool isMC16d, bool isMC16e, TString treename);
   
};

#endif

#ifdef Looper_cxx
Looper::Looper(const int& sample, int index_name, TString inputFileListName, TString TreeName, int isMC16a, int isMC16d, int isMC16e) : fChain(0) 
{

  m_sampleType = sample;
  m_index_name = index_name;
  m_TreeName = TreeName;
  m_inputFileListName = inputFileListName;
  m_isMC16a = isMC16a;
  m_isMC16d = isMC16d;
  m_isMC16e = isMC16e;

  if(TreeName=="weight_FTAG_B_UP") TreeName="nominal";
  if(TreeName=="weight_FTAG_B_DOWN") TreeName="nominal";
  if(TreeName=="weight_FTAG_C_UP") TreeName="nominal";
  if(TreeName=="weight_FTAG_C_DOWN") TreeName="nominal";
  if(TreeName=="weight_FTAG_L_UP") TreeName="nominal";
  if(TreeName=="weight_FTAG_L_DOWN") TreeName="nominal";

  if(TreeName=="weight_pileup_UP") TreeName="nominal";
  if(TreeName=="weight_pileup_DOWN") TreeName="nominal";
  if(TreeName=="weight_leptonSF_EL_SF_Trigger_UP") TreeName="nominal";
  if(TreeName=="weight_leptonSF_EL_SF_Trigger_DOWN") TreeName="nominal";
  if(TreeName=="weight_leptonSF_EL_SF_Reco_UP") TreeName="nominal";
  if(TreeName=="weight_leptonSF_EL_SF_Reco_DOWN") TreeName="nominal";
  if(TreeName=="weight_leptonSF_EL_SF_ID_UP") TreeName="nominal";
  if(TreeName=="weight_leptonSF_EL_SF_ID_DOWN") TreeName="nominal";
  if(TreeName=="weight_leptonSF_EL_SF_Isol_UP") TreeName="nominal";
  if(TreeName=="weight_leptonSF_EL_SF_Isol_DOWN") TreeName="nominal";
  if(TreeName=="weight_leptonSF_MU_SF_Trigger_STAT_UP") TreeName="nominal";
  if(TreeName=="weight_leptonSF_MU_SF_Trigger_STAT_DOWN") TreeName="nominal";
  if(TreeName=="weight_leptonSF_MU_SF_Trigger_SYST_UP") TreeName="nominal";
  if(TreeName=="weight_leptonSF_MU_SF_Trigger_SYST_DOWN") TreeName="nominal";
  if(TreeName=="weight_leptonSF_MU_SF_ID_STAT_UP") TreeName="nominal";
  if(TreeName=="weight_leptonSF_MU_SF_ID_STAT_DOWN") TreeName="nominal";
  if(TreeName=="weight_leptonSF_MU_SF_ID_SYST_UP") TreeName="nominal";
  if(TreeName=="weight_leptonSF_MU_SF_ID_SYST_DOWN") TreeName="nominal";
  if(TreeName=="weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP") TreeName="nominal";
  if(TreeName=="weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN") TreeName="nominal";
  if(TreeName=="weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP") TreeName="nominal";
  if(TreeName=="weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN") TreeName="nominal";
  if(TreeName=="weight_leptonSF_MU_SF_Isol_STAT_UP") TreeName="nominal";
  if(TreeName=="weight_leptonSF_MU_SF_Isol_STAT_DOWN") TreeName="nominal";
  if(TreeName=="weight_leptonSF_MU_SF_Isol_SYST_UP") TreeName="nominal";
  if(TreeName=="weight_leptonSF_MU_SF_Isol_SYST_DOWN") TreeName="nominal";
  if(TreeName=="weight_leptonSF_MU_SF_TTVA_STAT_UP") TreeName="nominal";
  if(TreeName=="weight_leptonSF_MU_SF_TTVA_STAT_DOWN") TreeName="nominal";
  if(TreeName=="weight_leptonSF_MU_SF_TTVA_SYST_UP") TreeName="nominal";
  if(TreeName=="weight_leptonSF_MU_SF_TTVA_SYST_DOWN") TreeName="nominal";
  if(TreeName=="weight_globalLeptonTriggerSF_EL_Trigger_UP") TreeName="nominal";
  if(TreeName=="weight_globalLeptonTriggerSF_EL_Trigger_DOWN") TreeName="nominal";
  if(TreeName=="weight_globalLeptonTriggerSF_MU_Trigger_STAT_UP") TreeName="nominal";
  if(TreeName=="weight_globalLeptonTriggerSF_MU_Trigger_STAT_DOWN") TreeName="nominal";
  if(TreeName=="weight_globalLeptonTriggerSF_MU_Trigger_SYST_UP") TreeName="nominal";
  if(TreeName=="weight_globalLeptonTriggerSF_MU_Trigger_SYST_DOWN") TreeName="nominal";
  if(TreeName=="weight_indiv_SF_EL_ChargeID_UP") TreeName="nominal";
  if(TreeName=="weight_indiv_SF_EL_ChargeID_DOWN") TreeName="nominal";
  if(TreeName=="weight_indiv_SF_EL_ChargeMisID_STAT_UP") TreeName="nominal";
  if(TreeName=="weight_indiv_SF_EL_ChargeMisID_STAT_DOWN") TreeName="nominal";
  if(TreeName=="weight_indiv_SF_EL_ChargeMisID_SYST_UP") TreeName="nominal";
  if(TreeName=="weight_indiv_SF_EL_ChargeMisID_SYST_DOWN") TreeName="nominal";
  if(TreeName=="weight_jvt_UP") TreeName="nominal";
  if(TreeName=="weight_jvt_DOWN") TreeName="nominal";
  if(TreeName=="PDF_90900") TreeName="nominal";
  if(TreeName=="PDF_90901") TreeName="nominal";
  if(TreeName=="PDF_90902") TreeName="nominal";
  if(TreeName=="PDF_90903") TreeName="nominal";
  if(TreeName=="PDF_90904") TreeName="nominal";
  if(TreeName=="PDF_90905") TreeName="nominal";
  if(TreeName=="PDF_90906") TreeName="nominal";
  if(TreeName=="PDF_90907") TreeName="nominal";
  if(TreeName=="PDF_90908") TreeName="nominal";
  if(TreeName=="PDF_90909") TreeName="nominal";
  if(TreeName=="PDF_90910") TreeName="nominal";
  if(TreeName=="PDF_90911") TreeName="nominal";
  if(TreeName=="PDF_90912") TreeName="nominal";
  if(TreeName=="PDF_90913") TreeName="nominal";
  if(TreeName=="PDF_90914") TreeName="nominal";
  if(TreeName=="PDF_90915") TreeName="nominal";
  if(TreeName=="PDF_90916") TreeName="nominal";
  if(TreeName=="PDF_90917") TreeName="nominal";
  if(TreeName=="PDF_90918") TreeName="nominal";
  if(TreeName=="PDF_90919") TreeName="nominal";
  if(TreeName=="PDF_90920") TreeName="nominal";
  if(TreeName=="PDF_90921") TreeName="nominal";
  if(TreeName=="PDF_90922") TreeName="nominal";
  if(TreeName=="PDF_90923") TreeName="nominal";
  if(TreeName=="PDF_90924") TreeName="nominal";
  if(TreeName=="PDF_90925") TreeName="nominal";
  if(TreeName=="PDF_90926") TreeName="nominal";
  if(TreeName=="PDF_90927") TreeName="nominal";
  if(TreeName=="PDF_90928") TreeName="nominal";
  if(TreeName=="PDF_90929") TreeName="nominal";
  if(TreeName=="PDF_90930") TreeName="nominal";
  if(TreeName=="PDF_260001") TreeName="nominal";
  if(TreeName=="PDF_260002") TreeName="nominal";
  if(TreeName=="PDF_260003") TreeName="nominal";
  if(TreeName=="PDF_260004") TreeName="nominal";
  if(TreeName=="PDF_260005") TreeName="nominal";
  if(TreeName=="PDF_260006") TreeName="nominal";
  if(TreeName=="PDF_260007") TreeName="nominal";
  if(TreeName=="PDF_260008") TreeName="nominal";
  if(TreeName=="PDF_260009") TreeName="nominal";
  if(TreeName=="PDF_260010") TreeName="nominal";
  if(TreeName=="PDF_260011") TreeName="nominal";
  if(TreeName=="PDF_260012") TreeName="nominal";
  if(TreeName=="PDF_260013") TreeName="nominal";
  if(TreeName=="PDF_260014") TreeName="nominal";
  if(TreeName=="PDF_260015") TreeName="nominal";
  if(TreeName=="PDF_260016") TreeName="nominal";
  if(TreeName=="PDF_260017") TreeName="nominal";
  if(TreeName=="PDF_260018") TreeName="nominal";
  if(TreeName=="PDF_260019") TreeName="nominal";
  if(TreeName=="PDF_260020") TreeName="nominal";
  if(TreeName=="PDF_260021") TreeName="nominal";
  if(TreeName=="PDF_260022") TreeName="nominal";
  if(TreeName=="PDF_260023") TreeName="nominal";
  if(TreeName=="PDF_260024") TreeName="nominal";
  if(TreeName=="PDF_260025") TreeName="nominal";
  if(TreeName=="PDF_260026") TreeName="nominal";
  if(TreeName=="PDF_260027") TreeName="nominal";
  if(TreeName=="PDF_260028") TreeName="nominal";
  if(TreeName=="PDF_260029") TreeName="nominal";
  if(TreeName=="PDF_260030") TreeName="nominal";
  if(TreeName=="PDF_260031") TreeName="nominal";
  if(TreeName=="PDF_260032") TreeName="nominal";
  if(TreeName=="PDF_260033") TreeName="nominal";
  if(TreeName=="PDF_260034") TreeName="nominal";
  if(TreeName=="PDF_260035") TreeName="nominal";
  if(TreeName=="PDF_260036") TreeName="nominal";
  if(TreeName=="PDF_260037") TreeName="nominal";
  if(TreeName=="PDF_260038") TreeName="nominal";
  if(TreeName=="PDF_260039") TreeName="nominal";
  if(TreeName=="PDF_260040") TreeName="nominal";
  if(TreeName=="PDF_260041") TreeName="nominal";
  if(TreeName=="PDF_260042") TreeName="nominal";
  if(TreeName=="PDF_260043") TreeName="nominal";
  if(TreeName=="PDF_260044") TreeName="nominal";
  if(TreeName=="PDF_260045") TreeName="nominal";
  if(TreeName=="PDF_260046") TreeName="nominal";
  if(TreeName=="PDF_260047") TreeName="nominal";
  if(TreeName=="PDF_260048") TreeName="nominal";
  if(TreeName=="PDF_260049") TreeName="nominal";
  if(TreeName=="PDF_260050") TreeName="nominal";
  if(TreeName=="PDF_260051") TreeName="nominal";
  if(TreeName=="PDF_260052") TreeName="nominal";
  if(TreeName=="PDF_260053") TreeName="nominal";
  if(TreeName=="PDF_260054") TreeName="nominal";
  if(TreeName=="PDF_260055") TreeName="nominal";
  if(TreeName=="PDF_260056") TreeName="nominal";
  if(TreeName=="PDF_260057") TreeName="nominal";
  if(TreeName=="PDF_260058") TreeName="nominal";
  if(TreeName=="PDF_260059") TreeName="nominal";
  if(TreeName=="PDF_260060") TreeName="nominal";
  if(TreeName=="PDF_260061") TreeName="nominal";
  if(TreeName=="PDF_260062") TreeName="nominal";
  if(TreeName=="PDF_260063") TreeName="nominal";
  if(TreeName=="PDF_260064") TreeName="nominal";
  if(TreeName=="PDF_260065") TreeName="nominal";
  if(TreeName=="PDF_260066") TreeName="nominal";
  if(TreeName=="PDF_260067") TreeName="nominal";
  if(TreeName=="PDF_260068") TreeName="nominal";
  if(TreeName=="PDF_260069") TreeName="nominal";
  if(TreeName=="PDF_260070") TreeName="nominal";
  if(TreeName=="PDF_260071") TreeName="nominal";
  if(TreeName=="PDF_260072") TreeName="nominal";
  if(TreeName=="PDF_260073") TreeName="nominal";
  if(TreeName=="PDF_260074") TreeName="nominal";
  if(TreeName=="PDF_260075") TreeName="nominal";
  if(TreeName=="PDF_260076") TreeName="nominal";
  if(TreeName=="PDF_260077") TreeName="nominal";
  if(TreeName=="PDF_260078") TreeName="nominal";
  if(TreeName=="PDF_260079") TreeName="nominal";
  if(TreeName=="PDF_260080") TreeName="nominal";
  if(TreeName=="PDF_260081") TreeName="nominal";
  if(TreeName=="PDF_260082") TreeName="nominal";
  if(TreeName=="PDF_260083") TreeName="nominal";
  if(TreeName=="PDF_260084") TreeName="nominal";
  if(TreeName=="PDF_260085") TreeName="nominal";
  if(TreeName=="PDF_260086") TreeName="nominal";
  if(TreeName=="PDF_260087") TreeName="nominal";
  if(TreeName=="PDF_260088") TreeName="nominal";
  if(TreeName=="PDF_260089") TreeName="nominal";
  if(TreeName=="PDF_260090") TreeName="nominal";
  if(TreeName=="PDF_260091") TreeName="nominal";
  if(TreeName=="PDF_260092") TreeName="nominal";
  if(TreeName=="PDF_260093") TreeName="nominal";
  if(TreeName=="PDF_260094") TreeName="nominal";
  if(TreeName=="PDF_260095") TreeName="nominal";
  if(TreeName=="PDF_260096") TreeName="nominal";
  if(TreeName=="PDF_260097") TreeName="nominal";
  if(TreeName=="PDF_260098") TreeName="nominal";
  if(TreeName=="PDF_260099") TreeName="nominal";
  if(TreeName=="PDF_260100") TreeName="nominal";

  TTree* tree = 0;
  TChain * chain = new TChain(TreeName,"");
  if(TreeName=="RadHi" || TreeName=="RadLo" ||
     TreeName=="FSRHi" || TreeName=="FSRLo") {
  chain->Add(inputFileListName+"/nominal");
  } else {
  chain->Add(inputFileListName+"/"+TreeName);
  }
  tree = chain;
  //questa era una prova per vedere se gira sui syst trees, ma niente
  //  string filename = (string)inputFileListName;
  //  TFile *f = TFile::Open(filename.c_str());
  //  f->GetObject(TreeName,tree);
  Init(tree);
}

Looper::~Looper()
{
   std::cerr<<"fChain	"<<endl;
   std::cerr<<"before if (!fChain) return;"<<endl;
   if (!fChain) return;
   std::cerr<<"before delete fChain->GetCurrentFile();"<<endl;
   delete fChain->GetCurrentFile();
   std::cerr<<"after delete fChain->GetCurrentFile();"<<endl;
}

Int_t Looper::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t Looper::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void Looper::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   mc_generator_weights = 0;
   weight_bTagSF_MV2c10_77_eigenvars_B_up = 0;
   weight_bTagSF_MV2c10_77_eigenvars_C_up = 0;
   weight_bTagSF_MV2c10_77_eigenvars_Light_up = 0;
   weight_bTagSF_MV2c10_77_eigenvars_B_down = 0;
   weight_bTagSF_MV2c10_77_eigenvars_C_down = 0;
   weight_bTagSF_MV2c10_77_eigenvars_Light_down = 0;
   weight_poisson = 0;
   el_pt = 0;
   el_eta = 0;
   el_cl_eta = 0;
   el_phi = 0;
   el_e = 0;
   el_charge = 0;
   el_topoetcone20 = 0;
   el_ptvarcone20 = 0;
   el_CF = 0;
   el_d0sig = 0;
   el_delta_z0_sintheta = 0;
   el_true_type = 0;
   el_true_origin = 0;
   el_true_firstEgMotherTruthType = 0;
   el_true_firstEgMotherTruthOrigin = 0;
   el_true_firstEgMotherPdgId = 0;
   el_true_isPrompt = 0;
   el_true_isChargeFl = 0;
   mu_pt = 0;
   mu_eta = 0;
   mu_phi = 0;
   mu_e = 0;
   mu_charge = 0;
   mu_topoetcone20 = 0;
   mu_ptvarcone30 = 0;
   mu_d0sig = 0;
   mu_delta_z0_sintheta = 0;
   mu_true_type = 0;
   mu_true_origin = 0;
   mu_true_isPrompt = 0;
   jet_pt = 0;
   jet_eta = 0;
   jet_phi = 0;
   jet_e = 0;
   jet_mv2c00 = 0;
   jet_mv2c10 = 0;
   jet_mv2c20 = 0;
   jet_ip3dsv1 = 0;
   jet_jvt = 0;
   jet_passfjvt = 0;
   jet_truthflav = 0;
   jet_truthPartonLabel = 0;
   jet_isTrueHS = 0;
   jet_truthflavExtended = 0;
   jet_isbtagged_MV2c10_77 = 0;
   jet_MV2r = 0;
   jet_MV2rmu = 0;
   jet_DL1 = 0;
   jet_DL1r = 0;
   jet_DL1rmu = 0;
   jet_MV2cl100 = 0;
   jet_MV2c100 = 0;
   jet_DL1_pu = 0;
   jet_DL1_pc = 0;
   jet_DL1_pb = 0;
   jet_DL1r_pu = 0;
   jet_DL1r_pc = 0;
   jet_DL1r_pb = 0;
   jet_DL1rmu_pu = 0;
   jet_DL1rmu_pc = 0;
   jet_DL1rmu_pb = 0;
   muon_truth_pdgid = 0;
   el_truth_pdgid = 0;

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("mc_generator_weights", &mc_generator_weights, &b_mc_generator_weights);
   fChain->SetBranchAddress("weight_mc", &weight_mc, &b_weight_mc);
   fChain->SetBranchAddress("weight_pileup", &weight_pileup, &b_weight_pileup);
   fChain->SetBranchAddress("weight_leptonSF", &weight_leptonSF, &b_weight_leptonSF);
   fChain->SetBranchAddress("weight_globalLeptonTriggerSF", &weight_globalLeptonTriggerSF, &b_weight_globalLeptonTriggerSF);
   fChain->SetBranchAddress("weight_oldTriggerSF", &weight_oldTriggerSF, &b_weight_oldTriggerSF);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_77", &weight_bTagSF_MV2c10_77, &b_weight_bTagSF_MV2c10_77);
   fChain->SetBranchAddress("weight_jvt", &weight_jvt, &b_weight_jvt);
   fChain->SetBranchAddress("weight_pileup_UP", &weight_pileup_UP, &b_weight_pileup_UP);
   fChain->SetBranchAddress("weight_pileup_DOWN", &weight_pileup_DOWN, &b_weight_pileup_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Trigger_UP", &weight_leptonSF_EL_SF_Trigger_UP, &b_weight_leptonSF_EL_SF_Trigger_UP);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Trigger_DOWN", &weight_leptonSF_EL_SF_Trigger_DOWN, &b_weight_leptonSF_EL_SF_Trigger_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Reco_UP", &weight_leptonSF_EL_SF_Reco_UP, &b_weight_leptonSF_EL_SF_Reco_UP);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Reco_DOWN", &weight_leptonSF_EL_SF_Reco_DOWN, &b_weight_leptonSF_EL_SF_Reco_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_ID_UP", &weight_leptonSF_EL_SF_ID_UP, &b_weight_leptonSF_EL_SF_ID_UP);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_ID_DOWN", &weight_leptonSF_EL_SF_ID_DOWN, &b_weight_leptonSF_EL_SF_ID_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Isol_UP", &weight_leptonSF_EL_SF_Isol_UP, &b_weight_leptonSF_EL_SF_Isol_UP);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Isol_DOWN", &weight_leptonSF_EL_SF_Isol_DOWN, &b_weight_leptonSF_EL_SF_Isol_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_STAT_UP", &weight_leptonSF_MU_SF_Trigger_STAT_UP, &b_weight_leptonSF_MU_SF_Trigger_STAT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_STAT_DOWN", &weight_leptonSF_MU_SF_Trigger_STAT_DOWN, &b_weight_leptonSF_MU_SF_Trigger_STAT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_SYST_UP", &weight_leptonSF_MU_SF_Trigger_SYST_UP, &b_weight_leptonSF_MU_SF_Trigger_SYST_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_SYST_DOWN", &weight_leptonSF_MU_SF_Trigger_SYST_DOWN, &b_weight_leptonSF_MU_SF_Trigger_SYST_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_UP", &weight_leptonSF_MU_SF_ID_STAT_UP, &b_weight_leptonSF_MU_SF_ID_STAT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_DOWN", &weight_leptonSF_MU_SF_ID_STAT_DOWN, &b_weight_leptonSF_MU_SF_ID_STAT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_UP", &weight_leptonSF_MU_SF_ID_SYST_UP, &b_weight_leptonSF_MU_SF_ID_SYST_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_DOWN", &weight_leptonSF_MU_SF_ID_SYST_DOWN, &b_weight_leptonSF_MU_SF_ID_SYST_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP", &weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP, &b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN", &weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN, &b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP", &weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP, &b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN", &weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN, &b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_STAT_UP", &weight_leptonSF_MU_SF_Isol_STAT_UP, &b_weight_leptonSF_MU_SF_Isol_STAT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_STAT_DOWN", &weight_leptonSF_MU_SF_Isol_STAT_DOWN, &b_weight_leptonSF_MU_SF_Isol_STAT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_SYST_UP", &weight_leptonSF_MU_SF_Isol_SYST_UP, &b_weight_leptonSF_MU_SF_Isol_SYST_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_SYST_DOWN", &weight_leptonSF_MU_SF_Isol_SYST_DOWN, &b_weight_leptonSF_MU_SF_Isol_SYST_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_STAT_UP", &weight_leptonSF_MU_SF_TTVA_STAT_UP, &b_weight_leptonSF_MU_SF_TTVA_STAT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_STAT_DOWN", &weight_leptonSF_MU_SF_TTVA_STAT_DOWN, &b_weight_leptonSF_MU_SF_TTVA_STAT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_SYST_UP", &weight_leptonSF_MU_SF_TTVA_SYST_UP, &b_weight_leptonSF_MU_SF_TTVA_SYST_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_SYST_DOWN", &weight_leptonSF_MU_SF_TTVA_SYST_DOWN, &b_weight_leptonSF_MU_SF_TTVA_SYST_DOWN);
   fChain->SetBranchAddress("weight_globalLeptonTriggerSF_EL_Trigger_UP", &weight_globalLeptonTriggerSF_EL_Trigger_UP, &b_weight_globalLeptonTriggerSF_EL_Trigger_UP);
   fChain->SetBranchAddress("weight_globalLeptonTriggerSF_EL_Trigger_DOWN", &weight_globalLeptonTriggerSF_EL_Trigger_DOWN, &b_weight_globalLeptonTriggerSF_EL_Trigger_DOWN);
   fChain->SetBranchAddress("weight_globalLeptonTriggerSF_MU_Trigger_STAT_UP", &weight_globalLeptonTriggerSF_MU_Trigger_STAT_UP, &b_weight_globalLeptonTriggerSF_MU_Trigger_STAT_UP);
   fChain->SetBranchAddress("weight_globalLeptonTriggerSF_MU_Trigger_STAT_DOWN", &weight_globalLeptonTriggerSF_MU_Trigger_STAT_DOWN, &b_weight_globalLeptonTriggerSF_MU_Trigger_STAT_DOWN);
   fChain->SetBranchAddress("weight_globalLeptonTriggerSF_MU_Trigger_SYST_UP", &weight_globalLeptonTriggerSF_MU_Trigger_SYST_UP, &b_weight_globalLeptonTriggerSF_MU_Trigger_SYST_UP);
   fChain->SetBranchAddress("weight_globalLeptonTriggerSF_MU_Trigger_SYST_DOWN", &weight_globalLeptonTriggerSF_MU_Trigger_SYST_DOWN, &b_weight_globalLeptonTriggerSF_MU_Trigger_SYST_DOWN);
   fChain->SetBranchAddress("weight_oldTriggerSF_EL_Trigger_UP", &weight_oldTriggerSF_EL_Trigger_UP, &b_weight_oldTriggerSF_EL_Trigger_UP);
   fChain->SetBranchAddress("weight_oldTriggerSF_EL_Trigger_DOWN", &weight_oldTriggerSF_EL_Trigger_DOWN, &b_weight_oldTriggerSF_EL_Trigger_DOWN);
   fChain->SetBranchAddress("weight_oldTriggerSF_MU_Trigger_STAT_UP", &weight_oldTriggerSF_MU_Trigger_STAT_UP, &b_weight_oldTriggerSF_MU_Trigger_STAT_UP);
   fChain->SetBranchAddress("weight_oldTriggerSF_MU_Trigger_STAT_DOWN", &weight_oldTriggerSF_MU_Trigger_STAT_DOWN, &b_weight_oldTriggerSF_MU_Trigger_STAT_DOWN);
   fChain->SetBranchAddress("weight_oldTriggerSF_MU_Trigger_SYST_UP", &weight_oldTriggerSF_MU_Trigger_SYST_UP, &b_weight_oldTriggerSF_MU_Trigger_SYST_UP);
   fChain->SetBranchAddress("weight_oldTriggerSF_MU_Trigger_SYST_DOWN", &weight_oldTriggerSF_MU_Trigger_SYST_DOWN, &b_weight_oldTriggerSF_MU_Trigger_SYST_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_EL_Reco", &weight_indiv_SF_EL_Reco, &b_weight_indiv_SF_EL_Reco);
   fChain->SetBranchAddress("weight_indiv_SF_EL_Reco_UP", &weight_indiv_SF_EL_Reco_UP, &b_weight_indiv_SF_EL_Reco_UP);
   fChain->SetBranchAddress("weight_indiv_SF_EL_Reco_DOWN", &weight_indiv_SF_EL_Reco_DOWN, &b_weight_indiv_SF_EL_Reco_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ID", &weight_indiv_SF_EL_ID, &b_weight_indiv_SF_EL_ID);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ID_UP", &weight_indiv_SF_EL_ID_UP, &b_weight_indiv_SF_EL_ID_UP);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ID_DOWN", &weight_indiv_SF_EL_ID_DOWN, &b_weight_indiv_SF_EL_ID_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_EL_Isol", &weight_indiv_SF_EL_Isol, &b_weight_indiv_SF_EL_Isol);
   fChain->SetBranchAddress("weight_indiv_SF_EL_Isol_UP", &weight_indiv_SF_EL_Isol_UP, &b_weight_indiv_SF_EL_Isol_UP);
   fChain->SetBranchAddress("weight_indiv_SF_EL_Isol_DOWN", &weight_indiv_SF_EL_Isol_DOWN, &b_weight_indiv_SF_EL_Isol_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeID", &weight_indiv_SF_EL_ChargeID, &b_weight_indiv_SF_EL_ChargeID);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeID_UP", &weight_indiv_SF_EL_ChargeID_UP, &b_weight_indiv_SF_EL_ChargeID_UP);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeID_DOWN", &weight_indiv_SF_EL_ChargeID_DOWN, &b_weight_indiv_SF_EL_ChargeID_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeMisID", &weight_indiv_SF_EL_ChargeMisID, &b_weight_indiv_SF_EL_ChargeMisID);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeMisID_STAT_UP", &weight_indiv_SF_EL_ChargeMisID_STAT_UP, &b_weight_indiv_SF_EL_ChargeMisID_STAT_UP);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeMisID_STAT_DOWN", &weight_indiv_SF_EL_ChargeMisID_STAT_DOWN, &b_weight_indiv_SF_EL_ChargeMisID_STAT_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeMisID_SYST_UP", &weight_indiv_SF_EL_ChargeMisID_SYST_UP, &b_weight_indiv_SF_EL_ChargeMisID_SYST_UP);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeMisID_SYST_DOWN", &weight_indiv_SF_EL_ChargeMisID_SYST_DOWN, &b_weight_indiv_SF_EL_ChargeMisID_SYST_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_MU_ID", &weight_indiv_SF_MU_ID, &b_weight_indiv_SF_MU_ID);
   fChain->SetBranchAddress("weight_indiv_SF_MU_ID_STAT_UP", &weight_indiv_SF_MU_ID_STAT_UP, &b_weight_indiv_SF_MU_ID_STAT_UP);
   fChain->SetBranchAddress("weight_indiv_SF_MU_ID_STAT_DOWN", &weight_indiv_SF_MU_ID_STAT_DOWN, &b_weight_indiv_SF_MU_ID_STAT_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_MU_ID_SYST_UP", &weight_indiv_SF_MU_ID_SYST_UP, &b_weight_indiv_SF_MU_ID_SYST_UP);
   fChain->SetBranchAddress("weight_indiv_SF_MU_ID_SYST_DOWN", &weight_indiv_SF_MU_ID_SYST_DOWN, &b_weight_indiv_SF_MU_ID_SYST_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_MU_ID_STAT_LOWPT_UP", &weight_indiv_SF_MU_ID_STAT_LOWPT_UP, &b_weight_indiv_SF_MU_ID_STAT_LOWPT_UP);
   fChain->SetBranchAddress("weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN", &weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN, &b_weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_MU_ID_SYST_LOWPT_UP", &weight_indiv_SF_MU_ID_SYST_LOWPT_UP, &b_weight_indiv_SF_MU_ID_SYST_LOWPT_UP);
   fChain->SetBranchAddress("weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN", &weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN, &b_weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_MU_Isol", &weight_indiv_SF_MU_Isol, &b_weight_indiv_SF_MU_Isol);
   fChain->SetBranchAddress("weight_indiv_SF_MU_Isol_STAT_UP", &weight_indiv_SF_MU_Isol_STAT_UP, &b_weight_indiv_SF_MU_Isol_STAT_UP);
   fChain->SetBranchAddress("weight_indiv_SF_MU_Isol_STAT_DOWN", &weight_indiv_SF_MU_Isol_STAT_DOWN, &b_weight_indiv_SF_MU_Isol_STAT_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_MU_Isol_SYST_UP", &weight_indiv_SF_MU_Isol_SYST_UP, &b_weight_indiv_SF_MU_Isol_SYST_UP);
   fChain->SetBranchAddress("weight_indiv_SF_MU_Isol_SYST_DOWN", &weight_indiv_SF_MU_Isol_SYST_DOWN, &b_weight_indiv_SF_MU_Isol_SYST_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_MU_TTVA", &weight_indiv_SF_MU_TTVA, &b_weight_indiv_SF_MU_TTVA);
   fChain->SetBranchAddress("weight_indiv_SF_MU_TTVA_STAT_UP", &weight_indiv_SF_MU_TTVA_STAT_UP, &b_weight_indiv_SF_MU_TTVA_STAT_UP);
   fChain->SetBranchAddress("weight_indiv_SF_MU_TTVA_STAT_DOWN", &weight_indiv_SF_MU_TTVA_STAT_DOWN, &b_weight_indiv_SF_MU_TTVA_STAT_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_MU_TTVA_SYST_UP", &weight_indiv_SF_MU_TTVA_SYST_UP, &b_weight_indiv_SF_MU_TTVA_SYST_UP);
   fChain->SetBranchAddress("weight_indiv_SF_MU_TTVA_SYST_DOWN", &weight_indiv_SF_MU_TTVA_SYST_DOWN, &b_weight_indiv_SF_MU_TTVA_SYST_DOWN);
   fChain->SetBranchAddress("weight_jvt_UP", &weight_jvt_UP, &b_weight_jvt_UP);
   fChain->SetBranchAddress("weight_jvt_DOWN", &weight_jvt_DOWN, &b_weight_jvt_DOWN);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_77_eigenvars_B_up", &weight_bTagSF_MV2c10_77_eigenvars_B_up, &b_weight_bTagSF_MV2c10_77_eigenvars_B_up);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_77_eigenvars_C_up", &weight_bTagSF_MV2c10_77_eigenvars_C_up, &b_weight_bTagSF_MV2c10_77_eigenvars_C_up);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_77_eigenvars_Light_up", &weight_bTagSF_MV2c10_77_eigenvars_Light_up, &b_weight_bTagSF_MV2c10_77_eigenvars_Light_up);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_77_eigenvars_B_down", &weight_bTagSF_MV2c10_77_eigenvars_B_down, &b_weight_bTagSF_MV2c10_77_eigenvars_B_down);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_77_eigenvars_C_down", &weight_bTagSF_MV2c10_77_eigenvars_C_down, &b_weight_bTagSF_MV2c10_77_eigenvars_C_down);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_77_eigenvars_Light_down", &weight_bTagSF_MV2c10_77_eigenvars_Light_down, &b_weight_bTagSF_MV2c10_77_eigenvars_Light_down);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_77_extrapolation_up", &weight_bTagSF_MV2c10_77_extrapolation_up, &b_weight_bTagSF_MV2c10_77_extrapolation_up);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_77_extrapolation_down", &weight_bTagSF_MV2c10_77_extrapolation_down, &b_weight_bTagSF_MV2c10_77_extrapolation_down);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_77_extrapolation_from_charm_up", &weight_bTagSF_MV2c10_77_extrapolation_from_charm_up, &b_weight_bTagSF_MV2c10_77_extrapolation_from_charm_up);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_77_extrapolation_from_charm_down", &weight_bTagSF_MV2c10_77_extrapolation_from_charm_down, &b_weight_bTagSF_MV2c10_77_extrapolation_from_charm_down);
   fChain->SetBranchAddress("weight_poisson", &weight_poisson, &b_weight_poisson);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("randomRunNumber", &randomRunNumber, &b_randomRunNumber);
   fChain->SetBranchAddress("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
   fChain->SetBranchAddress("mu", &mu, &b_mu);
   fChain->SetBranchAddress("backgroundFlags", &backgroundFlags, &b_backgroundFlags);
   fChain->SetBranchAddress("hasBadMuon", &hasBadMuon, &b_hasBadMuon);
   fChain->SetBranchAddress("el_pt", &el_pt, &b_el_pt);
   fChain->SetBranchAddress("el_eta", &el_eta, &b_el_eta);
   fChain->SetBranchAddress("el_cl_eta", &el_cl_eta, &b_el_cl_eta);
   fChain->SetBranchAddress("el_phi", &el_phi, &b_el_phi);
   fChain->SetBranchAddress("el_e", &el_e, &b_el_e);
   fChain->SetBranchAddress("el_charge", &el_charge, &b_el_charge);
   fChain->SetBranchAddress("el_topoetcone20", &el_topoetcone20, &b_el_topoetcone20);
   fChain->SetBranchAddress("el_ptvarcone20", &el_ptvarcone20, &b_el_ptvarcone20);
   fChain->SetBranchAddress("el_CF", &el_CF, &b_el_CF);
   fChain->SetBranchAddress("el_d0sig", &el_d0sig, &b_el_d0sig);
   fChain->SetBranchAddress("el_delta_z0_sintheta", &el_delta_z0_sintheta, &b_el_delta_z0_sintheta);
   fChain->SetBranchAddress("el_true_type", &el_true_type, &b_el_true_type);
   fChain->SetBranchAddress("el_true_origin", &el_true_origin, &b_el_true_origin);
   fChain->SetBranchAddress("el_true_firstEgMotherTruthType", &el_true_firstEgMotherTruthType, &b_el_true_firstEgMotherTruthType);
   fChain->SetBranchAddress("el_true_firstEgMotherTruthOrigin", &el_true_firstEgMotherTruthOrigin, &b_el_true_firstEgMotherTruthOrigin);
   fChain->SetBranchAddress("el_true_firstEgMotherPdgId", &el_true_firstEgMotherPdgId, &b_el_true_firstEgMotherPdgId);
   fChain->SetBranchAddress("el_true_isPrompt", &el_true_isPrompt, &b_el_true_isPrompt);
   fChain->SetBranchAddress("el_true_isChargeFl", &el_true_isChargeFl, &b_el_true_isChargeFl);
   fChain->SetBranchAddress("mu_pt", &mu_pt, &b_mu_pt);
   fChain->SetBranchAddress("mu_eta", &mu_eta, &b_mu_eta);
   fChain->SetBranchAddress("mu_phi", &mu_phi, &b_mu_phi);
   fChain->SetBranchAddress("mu_e", &mu_e, &b_mu_e);
   fChain->SetBranchAddress("mu_charge", &mu_charge, &b_mu_charge);
   fChain->SetBranchAddress("mu_topoetcone20", &mu_topoetcone20, &b_mu_topoetcone20);
   fChain->SetBranchAddress("mu_ptvarcone30", &mu_ptvarcone30, &b_mu_ptvarcone30);
   fChain->SetBranchAddress("mu_d0sig", &mu_d0sig, &b_mu_d0sig);
   fChain->SetBranchAddress("mu_delta_z0_sintheta", &mu_delta_z0_sintheta, &b_mu_delta_z0_sintheta);
   fChain->SetBranchAddress("mu_true_type", &mu_true_type, &b_mu_true_type);
   fChain->SetBranchAddress("mu_true_origin", &mu_true_origin, &b_mu_true_origin);
   fChain->SetBranchAddress("mu_true_isPrompt", &mu_true_isPrompt, &b_mu_true_isPrompt);
   fChain->SetBranchAddress("jet_pt", &jet_pt, &b_jet_pt);
   fChain->SetBranchAddress("jet_eta", &jet_eta, &b_jet_eta);
   fChain->SetBranchAddress("jet_phi", &jet_phi, &b_jet_phi);
   fChain->SetBranchAddress("jet_e", &jet_e, &b_jet_e);
   fChain->SetBranchAddress("jet_mv2c00", &jet_mv2c00, &b_jet_mv2c00);
   fChain->SetBranchAddress("jet_mv2c10", &jet_mv2c10, &b_jet_mv2c10);
   fChain->SetBranchAddress("jet_mv2c20", &jet_mv2c20, &b_jet_mv2c20);
   fChain->SetBranchAddress("jet_ip3dsv1", &jet_ip3dsv1, &b_jet_ip3dsv1);
   fChain->SetBranchAddress("jet_jvt", &jet_jvt, &b_jet_jvt);
   fChain->SetBranchAddress("jet_passfjvt", &jet_passfjvt, &b_jet_passfjvt);
   fChain->SetBranchAddress("jet_truthflav", &jet_truthflav, &b_jet_truthflav);
   fChain->SetBranchAddress("jet_truthPartonLabel", &jet_truthPartonLabel, &b_jet_truthPartonLabel);
   fChain->SetBranchAddress("jet_isTrueHS", &jet_isTrueHS, &b_jet_isTrueHS);
   fChain->SetBranchAddress("jet_truthflavExtended", &jet_truthflavExtended, &b_jet_truthflavExtended);
   fChain->SetBranchAddress("jet_isbtagged_MV2c10_77", &jet_isbtagged_MV2c10_77, &b_jet_isbtagged_MV2c10_77);
   fChain->SetBranchAddress("jet_MV2r", &jet_MV2r, &b_jet_MV2r);
   fChain->SetBranchAddress("jet_MV2rmu", &jet_MV2rmu, &b_jet_MV2rmu);
   fChain->SetBranchAddress("jet_DL1", &jet_DL1, &b_jet_DL1);
   fChain->SetBranchAddress("jet_DL1r", &jet_DL1r, &b_jet_DL1r);
   fChain->SetBranchAddress("jet_DL1rmu", &jet_DL1rmu, &b_jet_DL1rmu);
   fChain->SetBranchAddress("jet_MV2cl100", &jet_MV2cl100, &b_jet_MV2cl100);
   fChain->SetBranchAddress("jet_MV2c100", &jet_MV2c100, &b_jet_MV2c100);
   fChain->SetBranchAddress("jet_DL1_pu", &jet_DL1_pu, &b_jet_DL1_pu);
   fChain->SetBranchAddress("jet_DL1_pc", &jet_DL1_pc, &b_jet_DL1_pc);
   fChain->SetBranchAddress("jet_DL1_pb", &jet_DL1_pb, &b_jet_DL1_pb);
   fChain->SetBranchAddress("jet_DL1r_pu", &jet_DL1r_pu, &b_jet_DL1r_pu);
   fChain->SetBranchAddress("jet_DL1r_pc", &jet_DL1r_pc, &b_jet_DL1r_pc);
   fChain->SetBranchAddress("jet_DL1r_pb", &jet_DL1r_pb, &b_jet_DL1r_pb);
   fChain->SetBranchAddress("jet_DL1rmu_pu", &jet_DL1rmu_pu, &b_jet_DL1rmu_pu);
   fChain->SetBranchAddress("jet_DL1rmu_pc", &jet_DL1rmu_pc, &b_jet_DL1rmu_pc);
   fChain->SetBranchAddress("jet_DL1rmu_pb", &jet_DL1rmu_pb, &b_jet_DL1rmu_pb);
   fChain->SetBranchAddress("met_met", &met_met, &b_met_met);
   fChain->SetBranchAddress("met_phi", &met_phi, &b_met_phi);
   fChain->SetBranchAddress("emu_2015", &emu_2015, &b_emu_2015);
   fChain->SetBranchAddress("emu_2016", &emu_2016, &b_emu_2016);
   fChain->SetBranchAddress("emu_2017", &emu_2017, &b_emu_2017);
   fChain->SetBranchAddress("emu_2018", &emu_2018, &b_emu_2018);
   fChain->SetBranchAddress("emu_particle", &emu_particle, &b_emu_particle);
   fChain->SetBranchAddress("ee_2015", &ee_2015, &b_ee_2015);
   fChain->SetBranchAddress("ee_2016", &ee_2016, &b_ee_2016);
   fChain->SetBranchAddress("ee_particle", &ee_particle, &b_ee_particle);
   fChain->SetBranchAddress("mumu_2015", &mumu_2015, &b_mumu_2015);
   fChain->SetBranchAddress("mumu_2016", &mumu_2016, &b_mumu_2016);
   fChain->SetBranchAddress("mumu_particle", &mumu_particle, &b_mumu_particle);
   fChain->SetBranchAddress("muon_truth_pdgid", &muon_truth_pdgid, &b_muon_truth_pdgid);
   fChain->SetBranchAddress("el_truth_pdgid", &el_truth_pdgid, &b_el_truth_pdgid);
   Notify();
}

Bool_t Looper::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void Looper::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t Looper::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}

void Looper::CustomAddChain( TChain *chain, TString name_list_dsid ){

   //std::ifstream filein(name_list_dsid);
   //if(!filein) { cout << "Error opening input file "<<name_list_dsid<<"	in Looper::CustomAddChain" <<endl;
   //} else {
   //  while(filein.good()){
   //  TString name_root;
   //  filein>>name_root;
   //  if(name_root=="") break;
   //  chain->Add(name_root+"/nominal");	      
   //  }
   //}
   //filein.close();
}

void Looper::CustomAddChain_Truth( TChain *chain, TString name_list_dsid ){

   //std::ifstream filein(name_list_dsid);
   //if(!filein) { cout << "Error opening input file "<<name_list_dsid<<"	in nominalLooper::CustomAddChain" <<endl;
   //} else {
   //  while(filein.good()){
   //  TString name_root;
   //  filein>>name_root;
   //  if(name_root=="") break;
   //  chain->Add(name_root+"/truth");	      
   //  }
   //}
   //filein.close();
}
#endif
