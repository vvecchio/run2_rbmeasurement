#define Looper_cxx
#include "Looper/Looper.h"
#include <TH3.h>
#include <TH2.h>
#include <TH1F.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TAxis.h>
#include <TVector3.h>
#include <TLorentzVector.h>
#include <TMath.h>
#include <TRandom.h>
#include <TGraphErrors.h>
#include "iostream"
#include "sstream"
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include "math.h"
#include <TRandom3.h>
#include "Looper/SampleXsection.h"

using namespace std;
#define PI 3.14159265
#define DELTA_R_JET_QUARK 0.15

TH1F *h_CutFlowUnw = new TH1F("h_CutFlowUnw","h_CutFlowUnw",25,0,25);
TH1F *h_CutFlowWei = new TH1F("h_CutFlowWei","h_CutFlowWei",25,0,25);
//control plots
TH1F* h_nele = new TH1F("h_nele","h_nele",10,0,10);
TH1F* h_nmuo = new TH1F("h_nmuo","h_nmuo",10,0,10);
TH1F* h_nlep = new TH1F("h_nlep","h_nlep",10,0,10);
TH1F* h_njet = new TH1F("h_njet","h_njet",15,0,15);
TH1F* h_mu = new TH1F("h_mu","h_mu",100,0,100);
TH1F* h_nbjet_77 = new TH1F("h_nbjet_77","h_nbjet_77",10,0,10);
TH1F* h_njet_bin   = new TH1F("h_njet_bin","h_njet_bin",1,0,1);
TH1F* h_0bjet_77   = new TH1F("h_0bjet_77","h_0bjet_77",1,0,1);
TH1F* h_1bjet_77   = new TH1F("h_1bjet_77","h_1bjet_77",1,0,1);
TH1F* h_2bjet_77   = new TH1F("h_2bjet_77","h_2bjet_77",1,0,1);
TH1F* h_Mll   = new TH1F("h_Mll","h_Mll",100,0,1000);
TH1F* h_HT    = new TH1F("h_HT","h_HT",100,0,1200);
TH1F* h_met   = new TH1F("h_met","h_met",100,0,1200);
TH1F* h_el_true_origin = new TH1F("h_el_true_origin","h_el_true_origin",60,0,60);
TH1F* h_mu_true_origin = new TH1F("h_mu_true_origin","h_mu_true_origin",60,0,60);
TH1F* h_Eta_ele = new TH1F("h_Eta_ele","h_Eta_ele",100,-4,4);
TH1F* h_Eta_muon = new TH1F("h_Eta_muon","h_Eta_muon",100,-4,4);
TH1F* h_Pt_ele = new TH1F("h_Pt_ele","h_Pt_ele",100,0,500);
TH1F* h_Pt_muon = new TH1F("h_Pt_muon","h_Pt_muon",100,0,500);
TH1F* h_DeltaR_emu = new TH1F("h_DeltaR_emu","h_DeltaR_emu",100,0,10);
TH1F* h_DeltaR_lep_jet = new TH1F("h_DeltaR_lep_jet","h_DeltaR_lep_jet",1000,0,10);
TH1F* h_DeltaR_el_jet = new TH1F("h_DeltaR_el_jet","h_DeltaR_el_jet",1000,0,10);
TH1F* h_DeltaR_mu_jet = new TH1F("h_DeltaR_mu_jet","h_DeltaR_mu_jet",1000,0,10);
TH1F* h_Eta_jet1 = new TH1F("h_Eta_jet1","h_Eta_jet1",100,-4,4);
TH1F* h_Eta_jet2 = new TH1F("h_Eta_jet2","h_Eta_jet2",100,-4,4);
TH1F* h_Pt_jet1 = new TH1F("h_Pt_jet1","h_Pt_jet1",100,0,500);
TH1F* h_Pt_jet2 = new TH1F("h_Pt_jet2","h_Pt_jet2",100,0,500);
TH1F* h_DeltaR_j1j2 = new TH1F("h_DeltaR_j1j2","h_DeltaR_j1j2",100,0,10);
TH1F* h_taggedEta_jet1 = new TH1F("h_taggedEta_jet1","h_taggedEta_jet1",100,-4,4);
TH1F* h_taggedEta_jet2 = new TH1F("h_taggedEta_jet2","h_taggedEta_jet2",100,-4,4);
TH1F* h_taggedPt_jet1 = new TH1F("h_taggedPt_jet1","h_taggedPt_jet1",100,0,500);
TH1F* h_taggedPt_jet2 = new TH1F("h_taggedPt_jet2","h_taggedPt_jet2",100,0,500);
TH1F* h_taggedDeltaR_j1j2 = new TH1F("h_taggedDeltaR_j1j2","h_taggedDeltaR_j1j2",100,0,10);
//top jets
TH1F* h_Pt_jet1_top = new TH1F("h_Pt_jet1_top","h_Pt_jet1_top",100,0,500);
TH1F* h_Pt_jet2_top = new TH1F("h_Pt_jet2_top","h_Pt_jet2_top",100,0,500);
TH1F* h_Eta_jet1_top = new TH1F("h_Eta_jet1_top","h_Eta_jet1_top",100,-4,4);
TH1F* h_Eta_jet2_top = new TH1F("h_Eta_jet2_top","h_Eta_jet2_top",100,-4,4);
TH1F* h_DeltaR_j1j2_top = new TH1F("h_DeltaR_j1j2_top","h_DeltaR_j1j2_top",100,0,10);
//notop jets
TH1F* h_Eta_jet1_bnotop = new TH1F("h_Eta_jet1_bnotop","h_Eta_jet1_bnotop",100,-4,4);
TH1F* h_Eta_jet2_bnotop = new TH1F("h_Eta_jet2_bnotop","h_Eta_jet2_bnotop",100,-4,4);
TH1F* h_DeltaR_j1j2_bnotop = new TH1F("h_DeltaR_j1j2_bnotop","h_DeltaR_j1j2_bnotop",100,0,10);
TH1F* h_Eta_jet1_qnotop = new TH1F("h_Eta_jet1_qnotop","h_Eta_jet1_qnotop",100,-4,4);
TH1F* h_Eta_jet2_qnotop = new TH1F("h_Eta_jet2_qnotop","h_Eta_jet2_qnotop",100,-4,4);
TH1F* h_DeltaR_j1j2_qnotop = new TH1F("h_DeltaR_j1j2_qnotop","h_DeltaR_j1j2_qnotop",100,0,10);

TH1F* h_truth_partonfromtop_pdgId = new TH1F("h_truth_partonfromtop_pdgId","h_truth_partonfromtop_pdgId",10,0,10);
TH1F* h_truth_partonfromtopbar_pdgId = new TH1F("h_truth_partonfromtopbar_pdgId","h_truth_partonfromtopbar_pdgId",10,0,10);

//kinematical variables of leptons 
TH1F* h_btagj_0_leppluspt = new TH1F("h_btagj_0_leppluspt","h_btagj_0_leppluspt",100,0,500);
TH1F* h_btagj_1_leppluspt = new TH1F("h_btagj_1_leppluspt","h_btagj_1_leppluspt",100,0,500);
TH1F* h_btagj_2_leppluspt = new TH1F("h_btagj_2_leppluspt","h_btagj_2_leppluspt",100,0,500);
TH1F* h_2jets_leppluspt   = new TH1F("h_2jets_leppluspt","h_2jets_leppluspt",100,0,500);
TH1F* h_btagj_0_leppluseta = new TH1F("h_btagj_0_leppluseta","h_btagj_0_leppluseta",100,-4,4);
TH1F* h_btagj_1_leppluseta = new TH1F("h_btagj_1_leppluseta","h_btagj_1_leppluseta",100,-4,4);
TH1F* h_btagj_2_leppluseta = new TH1F("h_btagj_2_leppluseta","h_btagj_2_leppluseta",100,-4,4);
TH1F* h_2jets_leppluseta   = new TH1F("h_2jets_leppluseta","h_2jets_leppluseta",100,-4,4);

//efficiency pt jets
Float_t bins[] = {0, 30, 40, 50, 70, 90, 110, 140, 170, 200, 500, 1000};
//Float_t bins[] = {0, 30, 90, 1000};
//Float_t bins[] = { 0, 1000};
Int_t  binnum = sizeof(bins)/sizeof(Float_t) - 1; 

//inclusive jet pt spectrum
TH1F* h_Pt_jet_btop   = new TH1F("h_Pt_jet_btop","h_Pt_jet_btop",binnum, bins);
TH1F* h_Pt_jet_qtop   = new TH1F("h_Pt_jet_qtop","h_Pt_jet_qtop",binnum, bins);
TH1F* h_Pt_jet_qnotop = new TH1F("h_Pt_jet_qnotop","h_Pt_jet_qnotop",binnum, bins);
TH1F* h_Pt_jet_bnotop = new TH1F("h_Pt_jet_bnotop","h_Pt_jet_bnotop",binnum, bins);

//tagged jet pt spectrum
TH1F* h_tagged_Pt_jet_btop   = new TH1F("h_tagged_Pt_jet_btop","h_tagged_Pt_jet_btop",binnum, bins);
TH1F* h_tagged_Pt_jet_qtop   = new TH1F("h_tagged_Pt_jet_qtop","h_tagged_Pt_jet_qtop",binnum, bins);
TH1F* h_tagged_Pt_jet_qnotop = new TH1F("h_tagged_Pt_jet_qnotop","h_tagged_Pt_jet_qnotop",binnum, bins);
TH1F* h_tagged_Pt_jet_bnotop = new TH1F("h_tagged_Pt_jet_bnotop","h_tagged_Pt_jet_bnotop",binnum, bins);

Float_t bins_charm[] = {0, 25, 40, 60, 140, 250};
Int_t  binnum_charm = sizeof(bins_charm)/sizeof(Float_t) - 1; 
TH1F* h_tagged_Pt_jet_charm   = new TH1F("h_tagged_Pt_jet_charm","h_tagged_Pt_jet_charm",binnum_charm, bins_charm);
TH1F* h_Pt_jet_charm   	      = new TH1F("h_Pt_jet_charm","h_Pt_jet_charm",binnum_charm, bins_charm);

Float_t bins_light[] = {0, 20, 50, 100, 150, 300};
Int_t  binnum_light = sizeof(bins_light)/sizeof(Float_t) - 1; 
TH1F* h_tagged_Pt_jet_light   = new TH1F("h_tagged_Pt_jet_light","h_tagged_Pt_jet_light",binnum_light, bins_light);
TH1F* h_Pt_jet_light   	      = new TH1F("h_Pt_jet_light","h_Pt_jet_light",binnum_light, bins_light);

//inclusive jet pt spectrum
TH1F* h_P_jet_btop   = new TH1F("h_P_jet_btop","h_P_jet_btop",binnum, bins);
TH1F* h_P_jet_qtop   = new TH1F("h_P_jet_qtop","h_P_jet_qtop",binnum, bins);
TH1F* h_P_jet_qnotop = new TH1F("h_P_jet_qnotop","h_P_jet_qnotop",binnum, bins);
TH1F* h_P_jet_bnotop = new TH1F("h_P_jet_bnotop","h_P_jet_bnotop",binnum, bins);

//tagged jet pt spectrum
TH1F* h_tagged_P_jet_btop   = new TH1F("h_tagged_P_jet_btop","h_tagged_P_jet_btop",binnum, bins);
TH1F* h_tagged_P_jet_qtop   = new TH1F("h_tagged_P_jet_qtop","h_tagged_P_jet_qtop",binnum, bins);
TH1F* h_tagged_P_jet_qnotop = new TH1F("h_tagged_P_jet_qnotop","h_tagged_P_jet_qnotop",binnum, bins);
TH1F* h_tagged_P_jet_bnotop = new TH1F("h_tagged_P_jet_bnotop","h_tagged_P_jet_bnotop",binnum, bins);

//jet1jet2 jet pt spectrum
TH1F* h_Pt_jet1_btop   = new TH1F("h_Pt_jet1_btop","h_Pt_jet1_btop",binnum, bins);
TH1F* h_Pt_jet1_qtop   = new TH1F("h_Pt_jet1_qtop","h_Pt_jet1_qtop",binnum, bins);
TH1F* h_Pt_jet1_bnotop = new TH1F("h_Pt_jet1_bnotop","h_Pt_jet1_bnotop",binnum, bins);
TH1F* h_Pt_jet1_qnotop = new TH1F("h_Pt_jet1_qnotop","h_Pt_jet1_qnotop",binnum, bins);
TH1F* h_Pt_jet2_btop   = new TH1F("h_Pt_jet2_btop","h_Pt_jet2_btop",binnum, bins);
TH1F* h_Pt_jet2_qtop   = new TH1F("h_Pt_jet2_qtop","h_Pt_jet2_qtop",binnum, bins);
TH1F* h_Pt_jet2_bnotop = new TH1F("h_Pt_jet2_bnotop","h_Pt_jet2_bnotop",binnum, bins);
TH1F* h_Pt_jet2_qnotop = new TH1F("h_Pt_jet2_qnotop","h_Pt_jet2_qnotop",binnum, bins);

//tagged jet1jet2 jet pt spectrum
TH1F* h_tagged_Pt_jet1_btop   = new TH1F("h_tagged_Pt_jet1_btop","h_tagged_Pt_jet1_btop",binnum, bins);
TH1F* h_tagged_Pt_jet1_qtop   = new TH1F("h_tagged_Pt_jet1_qtop","h_tagged_Pt_jet1_qtop",binnum, bins);
TH1F* h_tagged_Pt_jet1_bnotop = new TH1F("h_tagged_Pt_jet1_bnotop","h_tagged_Pt_jet1_bnotop",binnum, bins);
TH1F* h_tagged_Pt_jet1_qnotop = new TH1F("h_tagged_Pt_jet1_qnotop","h_tagged_Pt_jet1_qnotop",binnum, bins);
TH1F* h_tagged_Pt_jet2_btop   = new TH1F("h_tagged_Pt_jet2_btop","h_tagged_Pt_jet2_btop",binnum, bins);
TH1F* h_tagged_Pt_jet2_qtop   = new TH1F("h_tagged_Pt_jet2_qtop","h_tagged_Pt_jet2_qtop",binnum, bins);
TH1F* h_tagged_Pt_jet2_bnotop = new TH1F("h_tagged_Pt_jet2_bnotop","h_tagged_Pt_jet2_bnotop",binnum, bins);
TH1F* h_tagged_Pt_jet2_qnotop = new TH1F("h_tagged_Pt_jet2_qnotop","h_tagged_Pt_jet2_qnotop",binnum, bins);

//jet1jet2 maps 
TH2F* h2_Pt_jet_btop   = new TH2F("h2_Pt_jet_btop","h2_Pt_jet_btop",binnum, bins, binnum, bins);
TH2F* h2_Pt_jet_qtop   = new TH2F("h2_Pt_jet_qtop","h2_Pt_jet_qtop",binnum, bins, binnum, bins);
TH2F* h2_Pt_jet_bnotop = new TH2F("h2_Pt_jet_bnotop","h2_Pt_jet_bnotop",binnum, bins, binnum, bins);
TH2F* h2_Pt_jet_qnotop = new TH2F("h2_Pt_jet_qnotop","h2_Pt_jet_qnotop",binnum, bins, binnum, bins);
TH2F* h2_tagged_Pt_jet_btop   = new TH2F("h2_tagged_Pt_jet_btop","h2_tagged_Pt_jet_btop",binnum, bins, binnum, bins);
TH2F* h2_tagged_Pt_jet_qtop   = new TH2F("h2_tagged_Pt_jet_qtop","h2_tagged_Pt_jet_qtop",binnum, bins, binnum, bins);
TH2F* h2_tagged_Pt_jet_bnotop = new TH2F("h2_tagged_Pt_jet_bnotop","h2_tagged_Pt_jet_bnotop",binnum, bins, binnum, bins);
TH2F* h2_tagged_Pt_jet_qnotop = new TH2F("h2_tagged_Pt_jet_qnotop","h2_tagged_Pt_jet_qnotop",binnum, bins, binnum, bins);

//alphai jet pt map
TH2F* h2_alpha_0_jetpt = new TH2F("h2_alpha_0_jetpt","h2_alpha_0_jetpt",binnum, bins, binnum, bins);
TH2F* h2_alpha_1_jetpt = new TH2F("h2_alpha_1_jetpt","h2_alpha_1_jetpt",binnum, bins, binnum, bins);
TH2F* h2_alpha_2_jetpt = new TH2F("h2_alpha_2_jetpt","h2_alpha_2_jetpt",binnum, bins, binnum, bins);
//these two sum up to h2_alpha_1_jetpt
TH2F* h2_alpha_1_leadjetpt    = new TH2F("h2_alpha_1_leadjetpt","h2_alpha_1_leadjetpt",binnum, bins, binnum, bins);
TH2F* h2_alpha_1_subleadjetpt = new TH2F("h2_alpha_1_subleadjetpt","h2_alpha_1_subleadjetpt",binnum, bins, binnum, bins);

TH2F* h2_btagj_0_jetpt = new TH2F("h2_btagj_0_jetpt","h2_btagj_0_jetpt",binnum, bins, binnum, bins);
TH2F* h2_btagj_1_jetpt = new TH2F("h2_btagj_1_jetpt","h2_btagj_1_jetpt",binnum, bins, binnum, bins);
TH2F* h2_btagj_2_jetpt = new TH2F("h2_btagj_2_jetpt","h2_btagj_2_jetpt",binnum, bins, binnum, bins);
TH2F* h2_2jets_jetpt   = new TH2F("h2_2jets_jetpt","h2_2jets_jetpt",binnum, bins, binnum, bins);
TH2F* h2_btagj_1_leadjetpt = new TH2F("h2_btagj_1_leadjetpt","h2_btagj_1_leadjetpt",binnum, bins, binnum, bins);
TH2F* h2_btagj_1_subleadjetpt = new TH2F("h2_btagj_1_subleadjetpt","h2_btagj_1_subleadjetpt",binnum, bins, binnum, bins);

TH2F* h2_tagged_jet_eta_jet_pt = new TH2F("h2_tagged_jet_eta_jet_pt","h2_tagged_jet_eta_jet_pt",50,-2.5,2.5,80,0,400000);
TH2F* h2_jet_eta_jet_pt = new TH2F("h2_jet_eta_jet_pt","h2_jet_eta_jet_pt",50,-2.5,2.5,80,0,400000);
//fakes
TH1F* h_Fakes 	    = new TH1F("h_Fakes","h_Fakes",10,0,10);

//efficiency pt jets
Float_t bins_mlj[] = { 0, 20, 40, 60, 80, 100, 120, 140, 160, 180, 200, 220, 240, 260, 280, 300, 320, 340, 360, 380, 400, 420, 440, 460, 480, 500, 530, 560, 650, 750, 1000};
Int_t  binnum_mlj = sizeof(bins_mlj)/sizeof(Float_t) - 1; 
//reco level Mlj distributions
TH1F* h_Mej   = new TH1F("h_Mej","h_Mej",binnum_mlj, bins_mlj);
TH1F* h_Mmj   = new TH1F("h_Mmj","h_Mmj",binnum_mlj, bins_mlj);
TH1F* h_Mlplusj   = new TH1F("h_Mlplusj","h_Mlplusj",binnum_mlj, bins_mlj);
TH1F* h_Mlminj    = new TH1F("h_Mlminj","h_Mlminj",binnum_mlj, bins_mlj);
TH2F* h2_Mlplusj  = new TH2F("h2_Mlplusj","h2_Mlplusj", binnum_mlj, bins_mlj,  binnum_mlj, bins_mlj);
TH3F* h3_Mlplusj  = new TH3F("h3_Mlplusj","h3_Mlplusj", binnum, bins, binnum, bins, binnum_mlj, bins_mlj);
TH3F* h3_Mlplusj_0  = new TH3F("h3_Mlplusj_0","h3_Mlplusj_0", binnum, bins, binnum, bins, binnum_mlj, bins_mlj);
TH3F* h3_Mlplusj_1  = new TH3F("h3_Mlplusj_1","h3_Mlplusj_1", binnum, bins, binnum, bins, binnum_mlj, bins_mlj);
TH3F* h3_Mlplusj_2  = new TH3F("h3_Mlplusj_2","h3_Mlplusj_2", binnum, bins, binnum, bins, binnum_mlj, bins_mlj);
TH1F* h_Meqjet   = new TH1F("h_Meqjet","h_Meqjet",binnum_mlj, bins_mlj);
TH1F* h_Mmqjet   = new TH1F("h_Mmqjet","h_Mmqjet",binnum_mlj, bins_mlj);
TH1F* h_Mebjet   = new TH1F("h_Mebjet","h_Mebjet",binnum_mlj, bins_mlj);
TH1F* h_Mmbjet   = new TH1F("h_Mmbjet","h_Mmbjet",binnum_mlj, bins_mlj);
TH1F* h_Ml1j1   = new TH1F("h_Ml1j1","h_Ml1j1",binnum_mlj, bins_mlj);
TH1F* h_Ml2j2   = new TH1F("h_Ml2j2","h_Ml2j2",binnum_mlj, bins_mlj);
TH1F* h_Ml1qjet   = new TH1F("h_Ml1qjet","h_Ml1qjet",binnum_mlj, bins_mlj);
TH1F* h_Ml2qjet   = new TH1F("h_Ml2qjet","h_Ml2qjet",binnum_mlj, bins_mlj);
TH1F* h_Ml1bjet   = new TH1F("h_Ml1bjet","h_Ml1bjet",binnum_mlj, bins_mlj);
TH1F* h_Ml2bjet   = new TH1F("h_Ml2bjet","h_Ml2bjet",binnum_mlj, bins_mlj);
TH1F* h_Ml1j1_bb   = new TH1F("h_Ml1j1_bb","h_Ml1j1_bb",binnum_mlj, bins_mlj);
TH1F* h_Ml2j2_bb   = new TH1F("h_Ml2j2_bb","h_Ml2j2_bb",binnum_mlj, bins_mlj);
TH1F* h_Ml1j1_bl   = new TH1F("h_Ml1j1_bl","h_Ml1j1_bl",binnum_mlj, bins_mlj);
TH1F* h_Ml2j2_bl   = new TH1F("h_Ml2j2_bl","h_Ml2j2_bl",binnum_mlj, bins_mlj);
TH1F* h_Ml1j1_lb   = new TH1F("h_Ml1j1_lb","h_Ml1j1_lb",binnum_mlj, bins_mlj);
TH1F* h_Ml2j2_lb   = new TH1F("h_Ml2j2_lb","h_Ml2j2_lb",binnum_mlj, bins_mlj);
TH1F* h_Ml1j1_ll   = new TH1F("h_Ml1j1_ll","h_Ml1j1_ll",binnum_mlj, bins_mlj);
TH1F* h_Ml2j2_ll   = new TH1F("h_Ml2j2_ll","h_Ml2j2_ll",binnum_mlj, bins_mlj);
TH2F* h2_Ml1j1_Ml2j2   = new TH2F("h2_Ml1j1_Ml2j2","h2_Ml1j1_Ml2j2",binnum_mlj, bins_mlj, binnum_mlj, bins_mlj);

//histo modelli
TH1F* h_Mlplusj_model   = new TH1F("h_Mlplusj_model","h_Mlplusj_model",binnum_mlj, bins_mlj);
TH1F* h_Mlminj_model   = new TH1F("h_Mlminj_model","h_Mlminj_model",binnum_mlj, bins_mlj);
TH1F* h_Mlj_model2   = new TH1F("h_Mlj_model2","h_Mlj_model2",binnum_mlj, bins_mlj);
TH1F* h_DeltaR_lep_jet_model = new TH1F("h_DeltaR_lep_jet_model","h_DeltaR_lep_jet_model",1000,0,10);
TH1F* h_DeltaR_lep_jet_model2 = new TH1F("h_DeltaR_lep_jet_model2","h_DeltaR_lep_jet_model2",1000,0,10);
TH1F* h_Pt_lplus   = new TH1F("h_Pt_lplus","h_Pt_lplus",100,0,1000);
TH1F* h_Pt_lplus_rotated   = new TH1F("h_Pt_lplus_rotated","h_Pt_lplus_rotated",100,0,1000);
TH1F* h_P_lplus   = new TH1F("h_P_lplus","h_P_lplus",100,0,1000);
TH1F* h_P_lplus_rotated   = new TH1F("h_P_lplus_rotated","h_P_lplus_rotated",100,0,1000);
TH1F* h_lep_phi = new TH1F("h_lep_phi","h_lep_phi",100,-4,4);
TH1F* h_lep_eta = new TH1F("h_lep_eta","h_lep_eta",100,-4,4);
TH1F* h_lep_theta = new TH1F("h_lep_theta","h_lep_eta",100,-4,4);
TH1F* h_rot_phi = new TH1F("h_rot_phi","h_rot_phi",100,-4,4);
TH1F* h_rot_eta = new TH1F("h_rot_eta","h_rot_eta",100,-4,4);
TH3F* h3_Mlplusj_model  = new TH3F("h3_Mlplusj_model","h3_Mlplusj_model", binnum, bins, binnum, bins, binnum_mlj, bins_mlj);

TH1F* h_Mtop_bottom   = new TH1F("h_Mtop_bottom","h_Mtop_bottom",200,0,500);
TH1F* h_Ml_jet_match_bottom   = new TH1F("h_Ml_jet_match_bottom","h_Ml_jet_match_bottom",binnum_mlj, bins_mlj);
TH1F* h_Mtop_jet_match_bottom   = new TH1F("h_Mtop_jet_match_bottom","h_Mtop_jet_match_bottom",200,0,500);
TH1F* h_Ml_jet_notmatch_bottom   = new TH1F("h_Ml_jet_notmatch_bottom","h_Ml_jet_notmatch_bottom",binnum_mlj, bins_mlj);
TH1F* h_Mtop_jet_notmatch_bottom   = new TH1F("h_Mtop_jet_notmatch_bottom","h_Mtop_jet_notmatch_bottom",200,0,500);
TH1F* h_Mtop_bjet_notmatch_bottom   = new TH1F("h_Mtop_bjet_notmatch_bottom","h_Mtop_bjet_notmatch_bottom",200,0,500);

//truth level Mlj distributions
TH1F* h_Meb_corr   = new TH1F("h_Meb_corr","h_Meb_corr",binnum_mlj, bins_mlj);
TH1F* h_Mmb_corr   = new TH1F("h_Mmb_corr","h_Mmb_corr",binnum_mlj, bins_mlj);
TH1F* h_Mlplusb_corr   = new TH1F("h_Mlplusb_corr","h_Mlplusb_corr",binnum_mlj, bins_mlj);
TH1F* h_Mlminb_corr   = new TH1F("h_Mlminb_corr","h_Mlminb_corr",binnum_mlj, bins_mlj);
TH1F* h_Mlplusb_uncorr   = new TH1F("h_Mlplusb_uncorr","h_Mlplusb_uncorr",binnum_mlj, bins_mlj);
TH1F* h_Mlminb_uncorr   = new TH1F("h_Mlminb_uncorr","h_Mlminb_uncorr",binnum_mlj, bins_mlj);

//truth-reco matched correct assigment
TH1F* h_DeltaR_b_jet  = new TH1F("h_DeltaR_b_jet","h_DeltaR_b_jet",100,0,10);
TH1F* h_DeltaPt_b_jet  = new TH1F("h_DeltaPt_b_jet","h_DeltaPt_b_jet",100,0,200);
TH1F* h_DeltaR_bbar_jet  = new TH1F("h_DeltaR_bbar_jet","h_DeltaR_bbar_jet",100,0,10);
TH1F* h_DeltaPt_bbar_jet  = new TH1F("h_DeltaPt_bbar_jet","h_DeltaPt_bbar_jet",100,0,200);
TH1F* h_nbmatch	       = new TH1F("h_nbmatch","h_nbmatch",10,0,10);
TH1F* h_nbbarmatch     = new TH1F("h_nbbarmatch","h_nbbarmatch",10,0,10);
TH1F* h_alphatruth     = new TH1F("h_alphatruth","h_alphatruth",10,0,10);
TH1F* h_mistagrates    = new TH1F("h_mistagrates","h_mistagrates",10,0,10);
TH1F* h_btageff        = new TH1F("h_btageff","h_btageff",10,0,10);
TH1F* h_mistagrates_top    = new TH1F("h_mistagrates_top","h_mistagrates_top",10,0,10);
TH1F* h_btageff_top        = new TH1F("h_btageff_top","h_btageff_top",10,0,10);
TH1F* h_mistagrates_notop    = new TH1F("h_mistagrates_notop","h_mistagrates_notop",10,0,10);
TH1F* h_btageff_notop        = new TH1F("h_btageff_notop","h_btageff_notop",10,0,10);

TH1F* h_Mlplusjmatch_corr   = new TH1F("h_Mlplusjmatch_corr","h_Mlplusjmatch_corr",binnum_mlj, bins_mlj);
TH1F* h_Mlminjmatch_corr    = new TH1F("h_Mlminjmatch_corr","h_Mlminjmatch_corr",binnum_mlj, bins_mlj);
TH1F* h_Mlminjmatch_model   = new TH1F("h_Mlminjmatch_model","h_Mlminjmatch_model",binnum_mlj, bins_mlj);
TH1F* h_Mlplusjmatch_swap   = new TH1F("h_Mlplusjmatch_swap","h_Mlplusjmatch_swap",binnum_mlj, bins_mlj);
TH1F* h_DeltaR_ttbar        = new TH1F("h_DeltaR_ttbar","h_DeltaR_ttbar",100,0,10);
TH1F* h_DeltaR_lplusb        = new TH1F("h_DeltaR_lplusb","h_DeltaR_lplusb",100,0,10);
TH1F* h_DeltaR_lplusbbar        = new TH1F("h_DeltaR_lplusbbar","h_DeltaR_lplusbbar",100,0,10);
TH1F* h_DeltaR_lep_jet_corr = new TH1F("h_DeltaR_lep_jet_corr","h_DeltaR_lep_jet_corr",1000,0,10);
TH3F* h3_Mlplusjmatch_corr  = new TH3F("h3_Mlplusjmatch_corr","h3_Mlplusjmatch_corr", binnum, bins, binnum, bins, binnum_mlj, bins_mlj);

//weight plots
TH1F* h_xsec_norm 		= new TH1F("h_xsec_norm", "h_xsec_norm", 1000,-100,100);
TH1F* h_weight_mc 		= new TH1F("h_weight_mc", "h_weight_mc", 10000,-1000,1000);
TH1F* h_weight_pileup 		= new TH1F("h_weight_pileup", "h_weight_pileup", 100,-10,10);
TH1F* h_weight_jvt 		= new TH1F("h_weight_jvt", "h_weight_jvt", 100,-10,10);
TH1F* h_weight_leptonSF 	= new TH1F("h_weight_leptonSF", "h_weight_leptonSF", 100,-10,10);
TH1F* h_weight_bTagSF_MV2c10_77 = new TH1F("h_weight_bTagSF_MV2c10_77", "h_weight_bTagSF_MV2c10_77", 100,-10,10);


//truth variables
Float_t t_b_pt;
Float_t t_b_eta;
Float_t t_b_phi;
Float_t t_b_m;
Int_t t_b_pdgId;
Float_t t_bbar_pt;
Float_t t_bbar_eta;
Float_t t_bbar_phi;
Float_t t_bbar_m;
Int_t t_bbar_pdgId;
Float_t t_top_pt;
Float_t t_top_eta;
Float_t t_top_phi;
Float_t t_top_m;
Float_t t_topbar_pt;
Float_t t_topbar_eta;
Float_t t_topbar_phi;
Float_t t_topbar_m;

Float_t t_lep_top_pt;
Float_t t_lep_top_eta;
Float_t t_lep_top_phi;
Float_t t_lep_top_m;
Float_t t_lep_topbar_pt;
Float_t t_lep_topbar_eta;
Float_t t_lep_topbar_phi;
Float_t t_lep_topbar_m;
Float_t t_nu_top_pt;
Float_t t_nu_top_eta;
Float_t t_nu_top_phi;
Float_t t_nu_top_m;
Float_t t_nu_topbar_pt;
Float_t t_nu_topbar_eta;
Float_t t_nu_topbar_phi;
Float_t t_nu_topbar_m;


TLorentzVector Previous_Jet;

void Looper::Loop(TString region, TString OS_or_SS){

TH1::SetDefaultSumw2(kTRUE);
TH2::SetDefaultSumw2(kTRUE);
TH3::SetDefaultSumw2(kTRUE);

Double_t Nmodel = 0;
Double_t Nmodel_153 = 0;
Double_t Nmis = 0;
Double_t Nmodel_2 = 0;
Double_t Nmodel_2_153 = 0;
Double_t Nmis_2 = 0;
Double_t Nmis_153 = 0;
Double_t Nevents = 0;
Double_t Nmatch_bj = 0;
Int_t    Nmatch_samejet = 0;
Double_t Nmatch_charmj = 0;
Double_t Nmatch_topj = 0;
Double_t Nevents_alpha = 0;
Double_t Nmatch_alpha0 = 0;
Double_t Nmatch_alpha1 = 0;
Double_t Nmatch_alpha2 = 0;
Double_t N85 = 0;
Double_t N77 = 0;
Double_t N70 = 0;
Double_t N60 = 0;
Double_t N85_nonb = 0;
Double_t N77_nonb = 0;
Double_t N70_nonb = 0;
Double_t N60_nonb = 0;
Double_t NTot_b	  = 0;
Double_t N85_b 	  = 0;
Double_t N77_b    = 0;
Double_t N70_b    = 0;
Double_t N60_b    = 0;
Double_t NTot_nob  = 0;
Double_t N85_notop = 0;
Double_t N77_notop = 0;
Double_t N70_notop = 0;
Double_t N60_notop = 0;
Double_t N85_nonb_notop = 0;
Double_t N77_nonb_notop = 0;
Double_t N70_nonb_notop = 0;
Double_t N60_nonb_notop = 0;
Double_t NTot_b_notop	  = 0;
Double_t N85_b_notop 	  = 0;
Double_t N77_b_notop    = 0;
Double_t N70_b_notop    = 0;
Double_t N60_b_notop    = 0;
Double_t NTot_nob_notop	  = 0;
Double_t N85_top = 0;
Double_t N77_top = 0;
Double_t N70_top = 0;
Double_t N60_top = 0;
Double_t N85_nonb_top = 0;
Double_t N77_nonb_top = 0;
Double_t N70_nonb_top = 0;
Double_t N60_nonb_top = 0;
Double_t N85_b_top 	  = 0;
Double_t N77_b_top    = 0;
Double_t N70_b_top    = 0;
Double_t N60_b_top    = 0;
Double_t NTot_b_top	  = 0;
Double_t NTot_nob_top	  = 0;
Double_t NTot_b_t	  = 0;
Double_t NTot_b_tbar	  = 0;
Double_t NTot_nob_t	  = 0;
Double_t NTot_nob_tbar	  = 0;
Double_t Nevents_fakes = 0.;
Double_t Nevents_fakeisele = 0.;
Double_t Nevents_fakeismuon = 0.;

  if (fChain == 0) return;
  Long64_t nentries = 0;
  Long64_t nbytes = 0;
  Long64_t nb = 0;
  nentries = fChain->GetEntries();
  
  //cross section
  Int_t previous_dsid = -1;
  Float_t previous_sampleWeight = 0;

  //setParticle Level Tree to do matching;
  TTree* tPL = 0;
  if(m_sampleType==1 || m_sampleType==14) tPL = SetTruthTree(m_sampleType);

  //Loop over events
  cout<<"=== Entering in Loop over "<<nentries<<" entries"<<endl;
  for(Long64_t jentry=0; jentry<nentries; jentry++) 
    { 
      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      nb = fChain->GetEntry(jentry); 
      nbytes += nb;
      double xsec_norm = 1.0;
      double scaling_factor = 1.0;

      cout << "\r" <<(int)( (double)jentry/(double)nentries*100 )<< "\% processed" << flush;
      
      bool isDATA = false;
      if(mcChannelNumber==0) isDATA = true;

      if(isDATA){
      xsec_norm = 1.0;
      }else{
          //a sort of primitive caching
         if((Int_t)mcChannelNumber==previous_dsid && jentry>0){
	 xsec_norm = previous_sampleWeight;
         }else{
	 xsec_norm = xsecWeight(mcChannelNumber, m_isMC16a, m_isMC16d, m_isMC16e, m_TreeName);
	 }
      }

      previous_dsid = mcChannelNumber;
      previous_sampleWeight = xsec_norm;

      scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF * weight_pileup * weight_jvt * weight_leptonSF;//*weight_bTagSF_MV2c10_77
      //ttbar modeling variations
      if(mcChannelNumber == 410482 && m_TreeName == "RadHi") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(5) * mc_generator_weights->at(193) * weight_pileup * weight_jvt * weight_leptonSF/mc_generator_weights->at(0);
      if(mcChannelNumber == 410472 && m_TreeName == "RadLo") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(6) * mc_generator_weights->at(194) * weight_pileup * weight_jvt * weight_leptonSF/mc_generator_weights->at(0);
      if(mcChannelNumber == 410472 && m_TreeName == "FSRHi") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(198) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "FSRLo") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(199) * weight_pileup * weight_jvt * weight_leptonSF;
      //singletop modeling variations
      if((mcChannelNumber == 410648 || mcChannelNumber == 410649) && m_TreeName == "RadHi") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(5) * mc_generator_weights->at(142) * weight_pileup * weight_jvt * weight_leptonSF/mc_generator_weights->at(0);
      if((mcChannelNumber == 410648 || mcChannelNumber == 410649) && m_TreeName == "RadLo") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(6) * mc_generator_weights->at(143) * weight_pileup * weight_jvt * weight_leptonSF/mc_generator_weights->at(0);
      if((mcChannelNumber == 410658 || mcChannelNumber == 410659) && m_TreeName == "RadHi") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(5) * mc_generator_weights->at(166) * weight_pileup * weight_jvt * weight_leptonSF/mc_generator_weights->at(0);
      if((mcChannelNumber == 410658 || mcChannelNumber == 410659) && m_TreeName == "RadLo") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(6) * mc_generator_weights->at(167) * weight_pileup * weight_jvt * weight_leptonSF/mc_generator_weights->at(0);
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_90900") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(11) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_90901") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(115) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_90902") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(116) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_90903") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(117) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_90904") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(118) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_90905") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(119) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_90906") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(120) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_90907") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(121) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_90908") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(122) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_90909") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(123) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_90910") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(124) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_90911") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(125) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_90912") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(126) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_90913") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(127) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_90914") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(128) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_90915") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(129) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_90916") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(130) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_90917") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(131) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_90918") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(132) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_90919") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(133) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_90920") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(134) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_90921") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(135) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_90922") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(136) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_90923") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(137) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_90924") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(138) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_90925") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(139) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_90926") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(140) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_90927") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(141) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_90928") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(142) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_90929") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(143) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_90930") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(144) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260001") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(15) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260002") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(16) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260003") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(17) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260004") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(18) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260005") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(19) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260006") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(20) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260007") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(21) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260008") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(22) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260009") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(23) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260010") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(24) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260011") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(25) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260012") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(26) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260013") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(27) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260014") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(28) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260015") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(29) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260016") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(30) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260017") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(31) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260018") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(32) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260019") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(33) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260020") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(34) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260021") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(35) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260022") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(36) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260023") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(37) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260024") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(38) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260025") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(39) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260026") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(40) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260027") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(41) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260028") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(42) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260029") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(43) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260030") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(44) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260031") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(45) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260032") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(46) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260033") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(47) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260034") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(48) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260035") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(49) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260036") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(50) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260037") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(51) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260038") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(52) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260039") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(53) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260040") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(54) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260041") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(55) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260042") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(56) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260043") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(57) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260044") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(58) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260045") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(59) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260046") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(60) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260047") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(61) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260048") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(62) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260049") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(63) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260050") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(64) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260051") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(65) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260052") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(66) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260053") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(67) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260054") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(68) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260055") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(69) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260056") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(70) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260057") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(71) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260058") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(72) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260059") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(73) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260060") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(74) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260061") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(75) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260062") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(76) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260063") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(77) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260064") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(78) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260065") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(79) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260066") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(80) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260067") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(81) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260068") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(82) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260069") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(83) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260070") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(84) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260071") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(85) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260072") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(86) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260073") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(87) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260074") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(88) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260075") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(89) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260076") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(90) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260077") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(91) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260078") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(92) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260079") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(93) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260080") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(94) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260081") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(95) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260082") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(96) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260083") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(97) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260084") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(98) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260085") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(99) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260086") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(100) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260087") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(101) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260088") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(102) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260089") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(103) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260090") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(104) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260091") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(105) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260092") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(106) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260093") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(107) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260094") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(108) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260095") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(109) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260096") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(110) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260097") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(111) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260098") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(112) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260099") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(113) * weight_pileup * weight_jvt * weight_leptonSF;
      if(mcChannelNumber == 410472 && m_TreeName == "PDF_260100") scaling_factor = xsec_norm * weight_globalLeptonTriggerSF * mc_generator_weights->at(114) * weight_pileup * weight_jvt * weight_leptonSF;

      //detector variations
      if(m_TreeName=="weight_pileup_UP") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF * weight_pileup_UP * weight_jvt * weight_leptonSF;
      if(m_TreeName=="weight_pileup_DOWN") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF * weight_pileup_DOWN * weight_jvt * weight_leptonSF;
      if(m_TreeName=="weight_leptonSF_EL_SF_Trigger_UP") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF * weight_pileup * weight_jvt * weight_leptonSF_EL_SF_Trigger_UP;
      if(m_TreeName=="weight_leptonSF_EL_SF_Trigger_DOWN") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF * weight_pileup * weight_jvt * weight_leptonSF_EL_SF_Trigger_DOWN;
      if(m_TreeName=="weight_leptonSF_EL_SF_Reco_UP") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF * weight_pileup * weight_jvt * weight_leptonSF_EL_SF_Reco_UP;
      if(m_TreeName=="weight_leptonSF_EL_SF_Reco_DOWN") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF * weight_pileup * weight_jvt * weight_leptonSF_EL_SF_Reco_DOWN;
      if(m_TreeName=="weight_leptonSF_EL_SF_ID_UP") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF * weight_pileup * weight_jvt * weight_leptonSF_EL_SF_ID_UP;
      if(m_TreeName=="weight_leptonSF_EL_SF_ID_DOWN") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF * weight_pileup * weight_jvt * weight_leptonSF_EL_SF_ID_DOWN;
      if(m_TreeName=="weight_leptonSF_EL_SF_Isol_UP") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF * weight_pileup * weight_jvt * weight_leptonSF_EL_SF_Isol_UP;
      if(m_TreeName=="weight_leptonSF_EL_SF_Isol_DOWN") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF * weight_pileup * weight_jvt * weight_leptonSF_EL_SF_Isol_DOWN;
      if(m_TreeName=="weight_leptonSF_MU_SF_Trigger_STAT_UP") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF * weight_pileup * weight_jvt * weight_leptonSF_MU_SF_Trigger_STAT_UP;
      if(m_TreeName=="weight_leptonSF_MU_SF_Trigger_STAT_DOWN") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF * weight_pileup * weight_jvt * weight_leptonSF_MU_SF_Trigger_STAT_DOWN;
      if(m_TreeName=="weight_leptonSF_MU_SF_Trigger_SYST_UP") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF * weight_pileup * weight_jvt * weight_leptonSF_MU_SF_Trigger_SYST_UP;
      if(m_TreeName=="weight_leptonSF_MU_SF_Trigger_SYST_DOWN") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF * weight_pileup * weight_jvt * weight_leptonSF_MU_SF_Trigger_SYST_DOWN;
      if(m_TreeName=="weight_leptonSF_MU_SF_ID_STAT_UP") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF * weight_pileup * weight_jvt * weight_leptonSF_MU_SF_ID_STAT_UP;
      if(m_TreeName=="weight_leptonSF_MU_SF_ID_STAT_DOWN") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF * weight_pileup * weight_jvt * weight_leptonSF_MU_SF_ID_STAT_DOWN;
      if(m_TreeName=="weight_leptonSF_MU_SF_ID_SYST_UP") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF * weight_pileup * weight_jvt * weight_leptonSF_MU_SF_ID_SYST_UP;
      if(m_TreeName=="weight_leptonSF_MU_SF_ID_SYST_DOWN") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF * weight_pileup * weight_jvt * weight_leptonSF_MU_SF_ID_SYST_DOWN;
      if(m_TreeName=="weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF * weight_pileup * weight_jvt * weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP;
      if(m_TreeName=="weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF * weight_pileup * weight_jvt * weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;
      if(m_TreeName=="weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF * weight_pileup * weight_jvt * weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP;
      if(m_TreeName=="weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF * weight_pileup * weight_jvt * weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN;
      if(m_TreeName=="weight_leptonSF_MU_SF_Isol_STAT_UP") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF * weight_pileup * weight_jvt * weight_leptonSF_MU_SF_Isol_STAT_UP;
      if(m_TreeName=="weight_leptonSF_MU_SF_Isol_STAT_DOWN") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF * weight_pileup * weight_jvt * weight_leptonSF_MU_SF_Isol_STAT_DOWN;
      if(m_TreeName=="weight_leptonSF_MU_SF_Isol_SYST_UP") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF * weight_pileup * weight_jvt * weight_leptonSF_MU_SF_Isol_SYST_UP;
      if(m_TreeName=="weight_leptonSF_MU_SF_Isol_SYST_DOWN") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF * weight_pileup * weight_jvt * weight_leptonSF_MU_SF_Isol_SYST_DOWN;
      if(m_TreeName=="weight_leptonSF_MU_SF_TTVA_STAT_UP") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF * weight_pileup * weight_jvt * weight_leptonSF_MU_SF_TTVA_STAT_UP;
      if(m_TreeName=="weight_leptonSF_MU_SF_TTVA_STAT_DOWN") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF * weight_pileup * weight_jvt * weight_leptonSF_MU_SF_TTVA_STAT_DOWN;
      if(m_TreeName=="weight_leptonSF_MU_SF_TTVA_SYST_UP") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF * weight_pileup * weight_jvt * weight_leptonSF_MU_SF_TTVA_SYST_UP;
      if(m_TreeName=="weight_leptonSF_MU_SF_TTVA_SYST_DOWN") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF * weight_pileup * weight_jvt * weight_leptonSF_MU_SF_TTVA_SYST_DOWN;
      if(m_TreeName=="weight_globalLeptonTriggerSF_EL_Trigger_UP") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF_EL_Trigger_UP * weight_pileup * weight_jvt * weight_leptonSF;
      if(m_TreeName=="weight_globalLeptonTriggerSF_EL_Trigger_DOWN") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF_EL_Trigger_DOWN * weight_pileup * weight_jvt * weight_leptonSF;
      if(m_TreeName=="weight_globalLeptonTriggerSF_MU_Trigger_STAT_UP") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF_MU_Trigger_STAT_UP * weight_pileup * weight_jvt * weight_leptonSF;
      if(m_TreeName=="weight_globalLeptonTriggerSF_MU_Trigger_STAT_DOWN") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF_MU_Trigger_STAT_DOWN * weight_pileup * weight_jvt * weight_leptonSF;
      if(m_TreeName=="weight_globalLeptonTriggerSF_MU_Trigger_SYST_UP") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF_MU_Trigger_SYST_UP * weight_pileup * weight_jvt * weight_leptonSF;
      if(m_TreeName=="weight_globalLeptonTriggerSF_MU_Trigger_SYST_DOWN") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF_MU_Trigger_SYST_DOWN * weight_pileup * weight_jvt * weight_leptonSF;
      if(m_TreeName=="weight_jvt_UP") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF * weight_pileup * weight_jvt_UP * weight_leptonSF;
      if(m_TreeName=="weight_jvt_DOWN") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF * weight_pileup * weight_jvt_DOWN * weight_leptonSF;
      if(m_TreeName=="weight_indiv_SF_EL_ChargeID_UP") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF * weight_pileup * weight_jvt * weight_leptonSF * weight_indiv_SF_EL_ChargeID_UP / weight_indiv_SF_EL_ChargeID;
      if(m_TreeName=="weight_indiv_SF_EL_ChargeID_DOWN") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF * weight_pileup * weight_jvt * weight_leptonSF * weight_indiv_SF_EL_ChargeID_DOWN / weight_indiv_SF_EL_ChargeID;
      if(m_TreeName=="weight_indiv_SF_EL_ChargeMisID_STAT_UP") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF * weight_pileup * weight_jvt * weight_leptonSF * weight_indiv_SF_EL_ChargeMisID_STAT_UP / weight_indiv_SF_EL_ChargeMisID;
      if(m_TreeName=="weight_indiv_SF_EL_ChargeMisID_STAT_DOWN") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF * weight_pileup * weight_jvt * weight_leptonSF * weight_indiv_SF_EL_ChargeMisID_STAT_DOWN / weight_indiv_SF_EL_ChargeMisID;
      if(m_TreeName=="weight_indiv_SF_EL_ChargeMisID_SYST_UP") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF * weight_pileup * weight_jvt * weight_leptonSF * weight_indiv_SF_EL_ChargeMisID_SYST_UP / weight_indiv_SF_EL_ChargeMisID;
      if(m_TreeName=="weight_indiv_SF_EL_ChargeMisID_SYST_DOWN") scaling_factor = xsec_norm * weight_mc * weight_globalLeptonTriggerSF * weight_pileup * weight_jvt * weight_leptonSF * weight_indiv_SF_EL_ChargeMisID_SYST_DOWN / weight_indiv_SF_EL_ChargeMisID;


      if(isDATA) scaling_factor = 1. ;
      else {
        //FTAGGING SCALE FACTOR
        double FTAG_ScaleFactor = 1.;
        FTAG_ScaleFactor = ReturnFTAG_ScaleFactor(m_TreeName);
        scaling_factor = scaling_factor * FTAG_ScaleFactor;
      }
      //scaling_factor =  1.;

        h_xsec_norm ->Fill(xsec_norm);	
        h_weight_mc->Fill(weight_mc);	
        h_weight_pileup->Fill(weight_pileup);	
        h_weight_jvt->Fill(weight_jvt);	
        h_weight_leptonSF->Fill(weight_leptonSF);	
        h_weight_bTagSF_MV2c10_77->Fill(weight_bTagSF_MV2c10_77);

	h_CutFlowUnw->Fill(0.);
	h_CutFlowWei->Fill(0.,scaling_factor);

	//TRG(2015-2016)
	bool passEMU = false;
	if(m_isMC16a && (emu_2015 || emu_2016)) passEMU = true;
	if(m_isMC16d && emu_2017) passEMU = true;
	if(m_isMC16e && emu_2018) passEMU = true;
	if(!passEMU) continue;
	h_CutFlowUnw->Fill(1.);
	h_CutFlowWei->Fill(1.,scaling_factor);
	
	//1ELE+1MU CUTS
	vector<int> GoodEle = Pass_GoodEle();
	Int_t nele  = GoodEle.size();
	vector<int> GoodMu = Pass_GoodMu();
	Int_t nmu  = GoodMu.size();
	if(nele!=1) continue;
	h_CutFlowUnw->Fill(2.);
	h_CutFlowWei->Fill(2.,scaling_factor);

	if(nmu!=1)  continue;
	h_CutFlowUnw->Fill(3.);
	h_CutFlowWei->Fill(3.,scaling_factor);

	//OS CUT
	//vector<int> GoodEle = Pass_GoodEle();
	int goodeleidx = GoodEle.at(0);
	//vector<int> GoodMu = Pass_GoodMu();
	int goodmuidx = GoodMu.at(0);
	if(OS_or_SS == "OS"){
	if(el_charge->at(goodeleidx) == mu_charge->at(goodmuidx)) continue;
	}
	if(OS_or_SS == "SS"){
	if(el_charge->at(goodeleidx) != mu_charge->at(goodmuidx)) continue;
	}
	h_CutFlowUnw->Fill(4.);
	h_CutFlowWei->Fill(4.,scaling_factor);

	UInt_t good_ele_index = GoodEle.at(0);
	UInt_t good_mu_index  = GoodMu.at(0);
	TLorentzVector TL_ele = RecoEle_TL(good_ele_index);
	TLorentzVector TL_muo = RecoMu_TL(good_mu_index);
	
	//NJETS==2
	vector<int> GoodJets = Pass_GoodJets();
    	UInt_t njet  = GoodJets.size();
	if(njet!=2) continue;
	h_CutFlowUnw->Fill(5.);
	h_CutFlowWei->Fill(5.,scaling_factor);
	
	if(!isDATA) h_el_true_origin->Fill(el_true_origin->at(good_ele_index),scaling_factor);
	if(!isDATA) h_mu_true_origin->Fill(mu_true_origin->at(good_mu_index),scaling_factor);

	TLorentzVector TL_lplus, TL_lmin;
	if(OS_or_SS == "OS"){
	  if(el_charge->at(good_ele_index)==1) 		TL_lplus = TL_ele;
	  else if(mu_charge->at(good_mu_index)==1) 	TL_lplus = TL_muo;
	  if(el_charge->at(good_ele_index)==-1) 		TL_lmin = TL_ele;
	  else if(mu_charge->at(good_mu_index)==-1) 	TL_lmin = TL_muo;
	}
	if(OS_or_SS == "SS"){
	  TL_lplus = TL_ele;
	  TL_lmin = TL_muo;
	}

	//Calculating HT
	Float_t HT = TL_lplus.Pt()+TL_lmin.Pt();
	for(UInt_t i=0; i<GoodJets.size(); i++){
	UInt_t k = GoodJets.at(i);
	TLorentzVector temp_jet = RecoJet_TL(k);
	HT += temp_jet.Pt();
        if(!isDATA && jet_mv2c10->at(k)>0.64 && jet_truthflav->at(k) == 4) h_tagged_Pt_jet_charm->Fill(jet_pt->at(k)/1000.,scaling_factor);
        if(!isDATA && jet_mv2c10->at(k)>0.64 && jet_truthflav->at(k) != 4 && jet_truthflav->at(k) != 5) h_tagged_Pt_jet_light->Fill(jet_pt->at(k)/1000.,scaling_factor);
        if(!isDATA && jet_truthflav->at(k) == 4) h_Pt_jet_charm->Fill(jet_pt->at(k)/1000.,scaling_factor);
        if(!isDATA && jet_truthflav->at(k) != 4 && jet_truthflav->at(k) != 5) h_Pt_jet_light->Fill(jet_pt->at(k)/1000.,scaling_factor);
	}

	//HT CUT
	if(HT/1000.<120.) continue;
	h_CutFlowUnw->Fill(6.);
	h_CutFlowWei->Fill(6.,scaling_factor);

	//Mll CUT
	Float_t Mll = (TL_ele+TL_muo).M();
	if(Mll/1000<15.) continue;
	h_CutFlowUnw->Fill(7.);
	h_CutFlowWei->Fill(7.,scaling_factor);


	//Filling Histos
	h_Eta_ele->Fill((el_eta->at(good_ele_index)), scaling_factor);
	h_Eta_muon->Fill((mu_eta->at(good_mu_index)), scaling_factor);
	h_Pt_ele->Fill(el_pt->at(good_ele_index)/1000., scaling_factor);
	h_Pt_muon->Fill(mu_pt->at(good_ele_index)/1000., scaling_factor);
	h_DeltaR_emu->Fill(TL_ele.DeltaR(TL_muo), scaling_factor);
	h_mu->Fill(mu, scaling_factor);
     	h_HT->Fill(HT/1000.,scaling_factor);
	h_met->Fill(met_met/1000.,scaling_factor);
     	h_Mll->Fill(Mll/1000.,scaling_factor);
	
	//Looking for leading and subleading jet
	UInt_t leading_jet_index    = 0;
	UInt_t subleading_jet_index = 1;
	TLorentzVector leading_jet, subleading_jet;
	leading_jet = RecoJet_TL(GoodJets.at(0));
	subleading_jet = RecoJet_TL(GoodJets.at(1));

	if(leading_jet.Pt() < subleading_jet.Pt()){
	leading_jet_index    = 1;
	subleading_jet_index = 0;
	leading_jet = RecoJet_TL(GoodJets.at(1));
	subleading_jet = RecoJet_TL(GoodJets.at(0));
	}

	//if(leading_jet.DeltaR(subleading_jet)<=2.5) continue;

	//Mj1l^2+Mj2l^2 minimization
	Double_t Mj1_ele = (TL_ele+leading_jet).M();
	Double_t Mj2_muo = (TL_muo+subleading_jet).M();
	Double_t Mj1_ele2_Mj2_muo2 = pow(Mj1_ele,2) + pow(Mj2_muo,2);
	Double_t Mj1_muo = (TL_muo+leading_jet).M();
	Double_t Mj2_ele = (TL_ele+subleading_jet).M();
	Double_t Mj1_muo2_Mj2_ele2 = pow(Mj1_muo,2) + pow(Mj2_ele,2);
	Double_t Mj1l1, Mj2l2;

	if(Mj1_ele2_Mj2_muo2<Mj1_muo2_Mj2_ele2){
	  Mj1l1 = Mj1_ele;
          Mj2l2 = Mj2_muo;
	} else {
	  Mj1l1 = Mj1_muo;
	  Mj2l2 = Mj2_ele;
	}

	bool isGoodChannel = false;
	if(region == "SR") isGoodChannel = true;
	if(region == "SR_toptop") if(Mj1l1/1000.< 175 && Mj2l2/1000.< 175) isGoodChannel = true;
	if(region == "CR_notopnotop") if(Mj1l1/1000.> 175 && Mj2l2/1000.> 175) isGoodChannel = true;
	if(region == "CR_topnotop") if(Mj1l1/1000.< 175 && Mj2l2/1000.> 175) isGoodChannel = true;
	if(region == "CR_notoptop") if(Mj1l1/1000.> 175 && Mj2l2/1000.< 175) isGoodChannel = true;

	if(!isDATA){
	  if(fabs(jet_truthflav->at(leading_jet_index))!=5)    h_Ml1qjet->Fill(Mj1l1/1000., scaling_factor);
	  if(fabs(jet_truthflav->at(subleading_jet_index))!=5) h_Ml2qjet->Fill(Mj2l2/1000., scaling_factor);
	  if(fabs(jet_truthflav->at(leading_jet_index))==5)    h_Ml1bjet->Fill(Mj1l1/1000., scaling_factor);
	  if(fabs(jet_truthflav->at(subleading_jet_index))==5) h_Ml2bjet->Fill(Mj2l2/1000., scaling_factor);
	}

	if(!isGoodChannel) continue;
	h_CutFlowUnw->Fill(8.);
	h_CutFlowWei->Fill(8.,scaling_factor);

	h_Ml1j1->Fill(Mj1l1/1000., scaling_factor);
	h_Ml2j2->Fill(Mj2l2/1000., scaling_factor);
	h2_Ml1j1_Ml2j2->Fill(Mj1l1/1000.,Mj2l2/1000., scaling_factor);

	if(!isDATA){
	  if(fabs(jet_truthflav->at(leading_jet_index))==5 && fabs(jet_truthflav->at(subleading_jet_index))==5) {
	    h_Ml1j1_bb->Fill(Mj1l1/1000., scaling_factor);
	    h_Ml2j2_bb->Fill(Mj2l2/1000., scaling_factor);
	  } else if (fabs(jet_truthflav->at(leading_jet_index))==5 && fabs(jet_truthflav->at(subleading_jet_index))!=5) {
	    h_Ml1j1_bl->Fill(Mj1l1/1000., scaling_factor);
	    h_Ml2j2_bl->Fill(Mj2l2/1000., scaling_factor);
	  } else if (fabs(jet_truthflav->at(leading_jet_index))!=5 && fabs(jet_truthflav->at(subleading_jet_index))==5) {
	    h_Ml1j1_lb->Fill(Mj1l1/1000., scaling_factor);
	    h_Ml2j2_lb->Fill(Mj2l2/1000., scaling_factor);
	  } else if (fabs(jet_truthflav->at(leading_jet_index))!=5 && fabs(jet_truthflav->at(subleading_jet_index))!=5) {
	    h_Ml1j1_ll->Fill(Mj1l1/1000., scaling_factor);
	    h_Ml2j2_ll->Fill(Mj2l2/1000., scaling_factor);
	  }
	}

	//Fakes
	if(!isDATA){
	if(!el_true_isPrompt->at(good_ele_index)) Nevents_fakeisele+=scaling_factor;
	if(!mu_true_isPrompt->at(good_mu_index))  Nevents_fakeismuon+=scaling_factor;
	if(!mu_true_isPrompt->at(good_mu_index) || !el_true_isPrompt->at(good_ele_index))  Nevents_fakes+=scaling_factor;
	}

	Nevents += scaling_factor;
	//END RECO-LEVEL CUTFLOW

	//Filling jet histos
	h_Eta_jet1->Fill(leading_jet.Eta(), scaling_factor);
	h_Eta_jet2->Fill(subleading_jet.Eta(), scaling_factor);
	h_Pt_jet1->Fill(leading_jet.Pt()/1000., scaling_factor);
	h_Pt_jet2->Fill(subleading_jet.Pt()/1000., scaling_factor);
	h_DeltaR_j1j2->Fill(leading_jet.DeltaR(subleading_jet), scaling_factor);
	if(jet_mv2c10->at(leading_jet_index)>0.64){
	h_taggedEta_jet1->Fill(leading_jet.Eta(), scaling_factor);
	h_taggedPt_jet1->Fill(leading_jet.Pt()/1000., scaling_factor);
	h_taggedDeltaR_j1j2->Fill(leading_jet.DeltaR(subleading_jet), scaling_factor);
	} else if(jet_mv2c10->at(subleading_jet_index)>0.64){
	h_taggedEta_jet2->Fill(subleading_jet.Eta(), scaling_factor);
	h_taggedPt_jet2->Fill(subleading_jet.Pt()/1000., scaling_factor);
	//h_taggedDeltaR_j1j2->Fill(leading_jet.DeltaR(subleading_jet), scaling_factor);
	}

	//Calculating Mlj histograms
	for(UInt_t i=0; i<GoodJets.size(); i++){

	UInt_t k = GoodJets.at(i);
	TLorentzVector temp_jet = RecoJet_TL(k);

	Double_t Mej = (TL_ele+temp_jet).M();
	Double_t Mmj = (TL_muo+temp_jet).M();
	h_Mej->Fill(Mej/1000.,scaling_factor);
	h_Mmj->Fill(Mmj/1000.,scaling_factor);
	h_DeltaR_el_jet->Fill(TL_ele.DeltaR(temp_jet), scaling_factor);
	h_DeltaR_mu_jet->Fill(TL_muo.DeltaR(temp_jet), scaling_factor);
	if(!isDATA){
	  if(fabs(jet_truthflav->at(k))!=5) h_Meqjet->Fill(Mej/1000., scaling_factor);
	  if(fabs(jet_truthflav->at(k))!=5) h_Mmqjet->Fill(Mmj/1000., scaling_factor);
	  if(fabs(jet_truthflav->at(k))==5) h_Mebjet->Fill(Mej/1000., scaling_factor);
	  if(fabs(jet_truthflav->at(k))==5) h_Mmbjet->Fill(Mmj/1000., scaling_factor);
	}

	Double_t Mlplusj = (TL_lplus+temp_jet).M();
	Double_t Mlminj  = (TL_lmin+temp_jet).M();
	h_Mlplusj->Fill(Mlplusj/1000.,scaling_factor);
	h_Mlminj->Fill(Mlminj/1000.,scaling_factor);
	h_DeltaR_lep_jet->Fill(TL_lplus.DeltaR(temp_jet), scaling_factor);
	h3_Mlplusj->Fill(leading_jet.Pt()/1000., subleading_jet.Pt()/1000.,Mlplusj/1000.,scaling_factor);

	if(Mlplusj/1000.>200.) Nmis_153 += scaling_factor;

	}
	h2_Mlplusj->Fill((TL_lplus+leading_jet).M()/1000., (TL_lplus+subleading_jet).M()/1000., scaling_factor);

	h_lep_eta->Fill(TL_lplus.Eta(),scaling_factor);
	h_lep_theta->Fill(TL_lplus.Theta(),scaling_factor);
	h_lep_phi->Fill(TL_lplus.Phi(),scaling_factor);

	//Rotated lepton model
	TLorentzVector Rot_lplus = Rotated_Lep(TL_lplus);
	TLorentzVector Rot_lmin  = Rotated_Lep(TL_lmin);
	h_rot_phi->Fill(Rot_lplus.Phi(),scaling_factor);
	h_rot_eta->Fill(Rot_lplus.Eta(),scaling_factor);

	h_Pt_lplus->Fill(TL_lplus.Pt()/1000.,scaling_factor);
	h_Pt_lplus_rotated->Fill(Rot_lplus.Pt()/1000.,scaling_factor);
	h_P_lplus->Fill(TL_lplus.P()/1000.,scaling_factor);
	h_P_lplus_rotated->Fill(Rot_lplus.P()/1000.,scaling_factor);

	for(UInt_t i = 0; i<GoodJets.size(); i++){

	UInt_t k = GoodJets.at(i);
	TLorentzVector temp_jet = RecoJet_TL(k);

	double Mlplusj_model = (Rot_lplus+temp_jet).M();
	double Mlminj_model = (Rot_lmin+temp_jet).M();

	h_Mlplusj_model->Fill(Mlplusj_model/1000.,scaling_factor);
	h_Mlminj_model->Fill(Mlminj_model/1000.,scaling_factor);
	h_DeltaR_lep_jet_model->Fill(Rot_lplus.DeltaR(temp_jet), scaling_factor);
	h3_Mlplusj_model->Fill(leading_jet.Pt()/1000., subleading_jet.Pt()/1000., Mlplusj_model/1000.,scaling_factor);

	Nmodel += scaling_factor;
	if(Mlplusj_model/1000.>200.) Nmodel_153 += scaling_factor;

	}

	//Random jet from previous event model
	TLorentzVector Random_Jet = Previous_Jet;
	double Mlplusj_model2 = (TL_lplus+Random_Jet).M();
	double Mlminj_model2  = (TL_lmin+Random_Jet).M();
	h_DeltaR_lep_jet_model2->Fill(TL_lplus.DeltaR(Random_Jet), scaling_factor);
	h_Mlj_model2->Fill(Mlplusj_model2/1000.,scaling_factor);
	h_Mlj_model2->Fill(Mlminj_model2/1000.,scaling_factor);


	//Filling b-jets multiplicity histos
    	Int_t nbjet_77 = 0;
	for(UInt_t i = 0; i<njet; i++){
	  UInt_t k = GoodJets.at(i);
    	  if(jet_mv2c10->at(k)>0.64) nbjet_77++;
          if(!isDATA && jet_mv2c10->at(k)>0.64 && jet_truthflav->at(k)==5) h2_tagged_jet_eta_jet_pt->Fill(jet_eta->at(k),jet_pt->at(k));
          if(!isDATA && jet_truthflav->at(k)==5) h2_jet_eta_jet_pt->Fill(jet_eta->at(k),jet_pt->at(k));
	}

    	h_njet->Fill(njet,scaling_factor);
        h_nele->Fill(nele,scaling_factor);
    	h_nmuo->Fill(nmu,scaling_factor);
    	h_nlep->Fill(nele+nmu,scaling_factor);
    	h_nbjet_77->Fill(nbjet_77,scaling_factor);
	if(njet == 2) 	  h_njet_bin->Fill(0.,scaling_factor);
	if(nbjet_77 == 0) h_0bjet_77->Fill(0., scaling_factor);
	if(nbjet_77 == 1) h_1bjet_77->Fill(0., scaling_factor);
	if(nbjet_77 == 2) h_2bjet_77->Fill(0., scaling_factor);
   	if(njet     == 2) h2_2jets_jetpt->Fill(jet_pt->at(leading_jet_index)/1000., jet_pt->at(subleading_jet_index)/1000., scaling_factor);
   	if(nbjet_77 == 0) h2_btagj_0_jetpt->Fill(jet_pt->at(leading_jet_index)/1000., jet_pt->at(subleading_jet_index)/1000., scaling_factor);
   	if(nbjet_77 == 1) h2_btagj_1_jetpt->Fill(jet_pt->at(leading_jet_index)/1000., jet_pt->at(subleading_jet_index)/1000., scaling_factor);
   	if(nbjet_77 == 2) h2_btagj_2_jetpt->Fill(jet_pt->at(leading_jet_index)/1000., jet_pt->at(subleading_jet_index)/1000., scaling_factor);
	if(njet     == 2) h_2jets_leppluspt->Fill(TL_lplus.Pt()/1000., scaling_factor);
	if(nbjet_77 == 0) h_btagj_0_leppluspt->Fill(TL_lplus.Pt()/1000., scaling_factor);
	if(nbjet_77 == 1) h_btagj_1_leppluspt->Fill(TL_lplus.Pt()/1000., scaling_factor);
	if(nbjet_77 == 2) h_btagj_2_leppluspt->Fill(TL_lplus.Pt()/1000., scaling_factor);
	if(njet     == 2) h_2jets_leppluseta->Fill(TL_lplus.Eta(), scaling_factor);
	if(nbjet_77 == 0) h_btagj_0_leppluseta->Fill(TL_lplus.Eta(), scaling_factor);
	if(nbjet_77 == 1) h_btagj_1_leppluseta->Fill(TL_lplus.Eta(), scaling_factor);
	if(nbjet_77 == 2) h_btagj_2_leppluseta->Fill(TL_lplus.Eta(), scaling_factor);
   	if(!isDATA && nbjet_77 == 1 && jet_mv2c10->at(leading_jet_index)>0.64) h2_btagj_1_leadjetpt->Fill(jet_pt->at(leading_jet_index)/1000., jet_pt->at(subleading_jet_index)/1000., scaling_factor);
   	if(!isDATA && nbjet_77 == 1 && jet_mv2c10->at(subleading_jet_index)>0.64) h2_btagj_1_subleadjetpt->Fill(jet_pt->at(leading_jet_index)/1000., jet_pt->at(subleading_jet_index)/1000., scaling_factor);

	for(UInt_t i = 0; i<njet; i++){
	  if(!isDATA){
	  UInt_t k = GoodJets.at(i);
	  if(jet_mv2c10->at(k)>0.11) N85+=scaling_factor;
	  if(jet_mv2c10->at(k)>0.64) N77+=scaling_factor;
	  if(jet_mv2c10->at(k)>0.83) N70+=scaling_factor;
	  if(jet_mv2c10->at(k)>0.94) N60+=scaling_factor;
	  if(jet_mv2c10->at(k)>0.11 && fabs(jet_truthflav->at(k))!=5) N85_nonb+=scaling_factor;
	  if(jet_mv2c10->at(k)>0.64 && fabs(jet_truthflav->at(k))!=5) N77_nonb+=scaling_factor;
	  if(jet_mv2c10->at(k)>0.83 && fabs(jet_truthflav->at(k))!=5) N70_nonb+=scaling_factor;
	  if(jet_mv2c10->at(k)>0.94 && fabs(jet_truthflav->at(k))!=5) N60_nonb+=scaling_factor;
	  if(jet_mv2c10->at(k)>0.11 && fabs(jet_truthflav->at(k))==5) N85_b+=scaling_factor;
	  if(jet_mv2c10->at(k)>0.64 && fabs(jet_truthflav->at(k))==5) N77_b+=scaling_factor;
	  if(jet_mv2c10->at(k)>0.83 && fabs(jet_truthflav->at(k))==5) N70_b+=scaling_factor;
	  if(jet_mv2c10->at(k)>0.94 && fabs(jet_truthflav->at(k))==5) N60_b+=scaling_factor;
	  if(fabs(jet_truthflav->at(k))==5) NTot_b+=scaling_factor;
	  if(fabs(jet_truthflav->at(k))!=5) NTot_nob+=scaling_factor;
 	  }
	}



	if(jentry==0) Previous_Jet = RecoJet_TL(GoodJets.at(0));
	else	      Previous_Jet = Return_PreviousEventJet(GoodJets);
	
	if(m_sampleType!=1 && m_sampleType!=14) continue;
	//******* MATCHING CON TRUTH-LEVEL *********

	//Reco & TruthLevel
    	Int_t nBytesRead = tPL->GetEntryWithIndex(runNumber, eventNumber);
        if ( nBytesRead < 0 ) continue;
	h_CutFlowUnw->Fill(9.);
	h_CutFlowWei->Fill(9.,scaling_factor);

	//selezione a truth-level
	if(t_b_pt == -1 || t_b_eta == -1000) continue; 
	if(t_bbar_pt == -1 || t_bbar_eta == -1000) continue; 
	h_CutFlowUnw->Fill(10.);
	h_CutFlowWei->Fill(10.,scaling_factor);


	TLorentzVector bbar_part;
	bbar_part.SetPtEtaPhiM(t_bbar_pt, t_bbar_eta, t_bbar_phi, t_bbar_m);
	TLorentzVector b_part;
	b_part.SetPtEtaPhiM(t_b_pt, t_b_eta, t_b_phi, t_b_m);


	if(el_charge->at(good_ele_index)==1) 		h_Meb_corr->Fill((TL_ele+b_part).M()/1000.,scaling_factor);
	else if(mu_charge->at(good_mu_index)==1) 	h_Mmb_corr->Fill((TL_muo+b_part).M()/1000.,scaling_factor);
	if(el_charge->at(good_ele_index)==-1) 		h_Meb_corr->Fill((TL_ele+bbar_part).M()/1000.,scaling_factor);
	else if(mu_charge->at(good_mu_index)==-1) 	h_Mmb_corr->Fill((TL_muo+bbar_part).M()/1000.,scaling_factor);

	//il quark b è assegnato correttamente al leptone
	Double_t Mlplus_b   = (TL_lplus+b_part).M()/1000.;
	Double_t Mlmin_bbar = (TL_lmin+bbar_part).M()/1000.;
	h_Mlplusb_corr->Fill(Mlplus_b,scaling_factor);
	h_Mlminb_corr->Fill(Mlmin_bbar,scaling_factor);
	h_DeltaR_lplusb->Fill(TL_lplus.DeltaR(b_part));
	h_DeltaR_lplusbbar->Fill(TL_lplus.DeltaR(bbar_part));

	//il quark b è assegnato INcorrettamente al leptone
	Double_t Mlplus_bbar   = (TL_lplus+bbar_part).M()/1000.;
	Double_t Mlmin_b       = (TL_lmin+b_part).M()/1000.;
	h_Mlplusb_uncorr->Fill(Mlplus_bbar,scaling_factor);
	h_Mlminb_uncorr->Fill(Mlmin_b,scaling_factor);

	//cerco il jet associato al b-quark
	int nmatch = 0;
	double deltaRmatch = 100.;
	UInt_t index_matched = -99;
        Int_t matched_quark = -99;
 	//Declaring leptons from Ws decay
	TLorentzVector truth_lep_top;
	truth_lep_top.SetPtEtaPhiM(t_lep_top_pt, t_lep_top_eta, t_lep_top_phi, t_lep_top_m);
	TLorentzVector truth_nu_top;
	truth_nu_top.SetPtEtaPhiM(t_nu_top_pt, t_nu_top_eta, t_nu_top_phi, t_nu_top_m);
	TLorentzVector truth_lep_topbar;
	truth_lep_topbar.SetPtEtaPhiM(t_lep_topbar_pt, t_lep_topbar_eta, t_lep_topbar_phi, t_lep_topbar_m);
	TLorentzVector truth_nu_topbar;
	truth_nu_topbar.SetPtEtaPhiM(t_nu_topbar_pt, t_nu_topbar_eta, t_nu_topbar_phi, t_nu_topbar_m);
	h_Mtop_bottom->Fill((truth_lep_top+b_part+truth_nu_top).M()/1000.);
	h_Mtop_bottom->Fill((truth_lep_topbar+bbar_part+truth_nu_topbar).M()/1000.);
        if(m_sampleType==14) h_truth_partonfromtop_pdgId->Fill(fabs(t_b_pdgId));
        if(m_sampleType==14) h_truth_partonfromtopbar_pdgId->Fill(fabs(t_bbar_pdgId));
	
	//deltaR matching between jet and bottom
	for(UInt_t i = 0; i<GoodJets.size(); i++){

	UInt_t k = GoodJets.at(i);
	TLorentzVector temp_jet = RecoJet_TL(k);

	double temp_deltaR = temp_jet.DeltaR(b_part);
	double deltaPt = fabs(temp_jet.Pt()-b_part.Pt());
	h_DeltaR_b_jet->Fill(temp_deltaR,scaling_factor);

		if(temp_deltaR<DELTA_R_JET_QUARK && temp_deltaR<deltaRmatch){// &&  deltaPt/1000.<20.){
		nmatch++;
		deltaRmatch = temp_deltaR;
		index_matched = k;
		h_DeltaPt_b_jet->Fill(deltaPt/1000.,scaling_factor);
		if(m_sampleType==14) matched_quark = fabs(t_b_pdgId);
		else matched_quark = 5;
		}
	}

	//cerco il jet associato al bbar-quark
	double nmatch_bbar = 0;
	double deltaRmatch_bbar = 100.;
	UInt_t index_matched_bbar= -99;
        Int_t matched_quarkbar = -99;

	for(UInt_t i = 0; i<njet; i++){

	UInt_t k = GoodJets.at(i);
	TLorentzVector temp_jet = RecoJet_TL(k);

	double temp_deltaR = temp_jet.DeltaR(bbar_part);
	double deltaPt = fabs(temp_jet.Pt()-bbar_part.Pt());
	h_DeltaR_bbar_jet->Fill(temp_deltaR,scaling_factor);

		if(temp_deltaR<DELTA_R_JET_QUARK && temp_deltaR<deltaRmatch_bbar){// && deltaPt/1000.<20.){
		nmatch_bbar++;
		deltaRmatch_bbar = temp_deltaR;
		index_matched_bbar = k;
		h_DeltaPt_bbar_jet->Fill(deltaPt/1000.,scaling_factor);
		if(m_sampleType==14) matched_quarkbar = fabs(t_bbar_pdgId);
		else matched_quarkbar = 5;
		}
	}

        //if b and bbar associated to the same jet
        //taking the closest one 
        //searching for another jet associated to the remaining parton
	if(index_matched_bbar==index_matched){
	  if(deltaRmatch<deltaRmatch_bbar){
	  deltaRmatch_bbar = 10.;
	  nmatch_bbar = 0;	
	    for(UInt_t i = 0; i<njet; i++){
	      UInt_t k = GoodJets.at(i);
	      if(k == index_matched) continue;
	      TLorentzVector temp_jet = RecoJet_TL(k);
	      double temp_deltaR = temp_jet.DeltaR(bbar_part);
	      double deltaPt = fabs(temp_jet.Pt()-bbar_part.Pt());
	      if(temp_deltaR<DELTA_R_JET_QUARK && temp_deltaR<deltaRmatch_bbar){// && deltaPt/1000.<20.){
	      nmatch_bbar++;
	      deltaRmatch_bbar = temp_deltaR;
	      index_matched_bbar = k;
	      if(m_sampleType==14) matched_quarkbar = fabs(t_bbar_pdgId);
	      else matched_quarkbar = 5;
	      }
	    }
	  
	  }else{
	  deltaRmatch = 10.;
	  nmatch = 0;	
	    for(UInt_t i = 0; i<njet; i++){
	      UInt_t k = GoodJets.at(i);
	      if(k == index_matched_bbar) continue;
	      TLorentzVector temp_jet = RecoJet_TL(k);
	      double temp_deltaR = temp_jet.DeltaR(b_part);
	      double deltaPt = fabs(temp_jet.Pt()-b_part.Pt());
	      if(temp_deltaR<DELTA_R_JET_QUARK && temp_deltaR<deltaRmatch){// && deltaPt/1000.<20.){
	      nmatch++;
	      deltaRmatch = temp_deltaR;
	      index_matched = k;
	      if(m_sampleType==14) matched_quark = fabs(t_b_pdgId);
	      else matched_quark = 5;
	      }
	    }
	  }
	}

	if(nmatch==2 || nmatch_bbar==2) continue;
	h_CutFlowUnw->Fill(11.);
	h_CutFlowWei->Fill(11.,scaling_factor);
	h_nbmatch->Fill(nmatch, scaling_factor);
	h_nbbarmatch->Fill(nmatch_bbar, scaling_factor);

	if(index_matched!=-99){
	    float jet_mv2c10_matched = jet_mv2c10->at(index_matched);
	    float jet_truthflav_matched = fabs(jet_truthflav->at(index_matched));
	    TLorentzVector temp_jet = RecoJet_TL(index_matched);
	    if(fabs(jet_truthflav_matched)==5) h_Pt_jet_btop->Fill(jet_pt->at(index_matched)/1000.,scaling_factor);
	    if(fabs(jet_truthflav_matched)!=5) h_Pt_jet_qtop->Fill(jet_pt->at(index_matched)/1000.,scaling_factor);
	    if(fabs(jet_truthflav_matched)==5) h_P_jet_btop->Fill(temp_jet.P()/1000.,scaling_factor);
	    if(fabs(jet_truthflav_matched)!=5) h_P_jet_qtop->Fill(temp_jet.P()/1000.,scaling_factor);
	    if(jet_mv2c10_matched>0.64 && fabs(jet_truthflav_matched)==5) h_tagged_Pt_jet_btop->Fill(jet_pt->at(index_matched)/1000.,scaling_factor);
	    if(jet_mv2c10_matched>0.64 && fabs(jet_truthflav_matched)!=5) h_tagged_Pt_jet_qtop->Fill(jet_pt->at(index_matched)/1000.,scaling_factor);
	    if(jet_mv2c10_matched>0.64 && fabs(jet_truthflav_matched)==5) h_tagged_P_jet_btop->Fill(temp_jet.P()/1000.,scaling_factor);
	    if(jet_mv2c10_matched>0.64 && fabs(jet_truthflav_matched)!=5) h_tagged_P_jet_qtop->Fill(temp_jet.P()/1000.,scaling_factor);
	    h_Ml_jet_match_bottom->Fill((truth_lep_top+temp_jet).M()/1000., scaling_factor);
            h_Mtop_jet_match_bottom->Fill((truth_lep_top+temp_jet+truth_nu_top).M()/1000., scaling_factor);
	}
	if(index_matched_bbar!=-99){
	    float jet_mv2c10_matched_bbar = jet_mv2c10->at(index_matched_bbar);
	    float jet_truthflav_matched_bbar = fabs(jet_truthflav->at(index_matched_bbar));
	    TLorentzVector temp_jet = RecoJet_TL(index_matched_bbar);
	    if(fabs(jet_truthflav_matched_bbar)==5) h_Pt_jet_btop->Fill(jet_pt->at(index_matched_bbar)/1000.,scaling_factor);
	    if(fabs(jet_truthflav_matched_bbar)!=5) h_Pt_jet_qtop->Fill(jet_pt->at(index_matched_bbar)/1000.,scaling_factor);
	    if(fabs(jet_truthflav_matched_bbar)==5) h_P_jet_btop->Fill(temp_jet.P()/1000.,scaling_factor);
	    if(fabs(jet_truthflav_matched_bbar)!=5) h_P_jet_qtop->Fill(temp_jet.P()/1000.,scaling_factor);
	    if(jet_mv2c10_matched_bbar>0.64 && fabs(jet_truthflav_matched_bbar)==5) h_tagged_Pt_jet_btop->Fill(jet_pt->at(index_matched_bbar)/1000.,scaling_factor);
	    if(jet_mv2c10_matched_bbar>0.64 && fabs(jet_truthflav_matched_bbar)!=5) h_tagged_Pt_jet_qtop->Fill(jet_pt->at(index_matched_bbar)/1000.,scaling_factor);
	    if(jet_mv2c10_matched_bbar>0.64 && fabs(jet_truthflav_matched_bbar)==5) h_tagged_P_jet_btop->Fill(temp_jet.P()/1000.,scaling_factor);
	    if(jet_mv2c10_matched_bbar>0.64 && fabs(jet_truthflav_matched_bbar)!=5) h_tagged_P_jet_qtop->Fill(temp_jet.P()/1000.,scaling_factor);
	    h_Ml_jet_match_bottom->Fill((truth_lep_topbar+temp_jet).M()/1000.,scaling_factor);
	    h_Mtop_jet_match_bottom->Fill((truth_lep_topbar+temp_jet+truth_nu_topbar).M()/1000.,scaling_factor);
	}
	//h_DeltaR_j1j2_top->Fill(leading_jet.DeltaR(subleading_jet),scaling_factor);

	float jet_mv2c10_leading       = jet_mv2c10->at(leading_jet_index);
	float jet_truthflav_leading    = fabs(jet_truthflav->at(leading_jet_index));
	float jet_mv2c10_subleading    = jet_mv2c10->at(subleading_jet_index);
	float jet_truthflav_subleading = fabs(jet_truthflav->at(subleading_jet_index));

	if((leading_jet_index == index_matched) || (leading_jet_index == index_matched_bbar)){
	    //binned jet pt
	    h_Pt_jet1_top->Fill(jet_pt->at(leading_jet_index)/1000.,scaling_factor);
	    h_Eta_jet1_top->Fill(jet_eta->at(leading_jet_index),scaling_factor);
	    if(fabs(jet_truthflav_leading)==5) h_Pt_jet1_btop->Fill(jet_pt->at(leading_jet_index)/1000.,scaling_factor);
	    if(fabs(jet_truthflav_leading)!=5) h_Pt_jet1_qtop->Fill(jet_pt->at(leading_jet_index)/1000.,scaling_factor);
	    if(jet_mv2c10_leading>0.64 && fabs(jet_truthflav_leading)==5) h_tagged_Pt_jet1_btop->Fill(jet_pt->at(leading_jet_index)/1000.,scaling_factor);
	    if(jet_mv2c10_leading>0.64 && fabs(jet_truthflav_leading)!=5) h_tagged_Pt_jet1_qtop->Fill(jet_pt->at(leading_jet_index)/1000.,scaling_factor);
	    //inclusive
	    if(jet_mv2c10_leading>0.11) N85_top+=scaling_factor;
	    if(jet_mv2c10_leading>0.64) N77_top+=scaling_factor;
	    if(jet_mv2c10_leading>0.83) N70_top+=scaling_factor;
	    if(jet_mv2c10_leading>0.94) N60_top+=scaling_factor;
	    if(jet_mv2c10_leading>0.11 && fabs(jet_truthflav_leading)!=5) N85_nonb_top+=scaling_factor;
	    if(jet_mv2c10_leading>0.64 && fabs(jet_truthflav_leading)!=5) N77_nonb_top+=scaling_factor;
	    if(jet_mv2c10_leading>0.83 && fabs(jet_truthflav_leading)!=5) N70_nonb_top+=scaling_factor;
	    if(jet_mv2c10_leading>0.94 && fabs(jet_truthflav_leading)!=5) N60_nonb_top+=scaling_factor;
	    if(jet_mv2c10_leading>0.11 && fabs(jet_truthflav_leading)==5) N85_b_top+=scaling_factor;
	    if(jet_mv2c10_leading>0.64 && fabs(jet_truthflav_leading)==5) N77_b_top+=scaling_factor;
	    if(jet_mv2c10_leading>0.83 && fabs(jet_truthflav_leading)==5) N70_b_top+=scaling_factor;
	    if(jet_mv2c10_leading>0.94 && fabs(jet_truthflav_leading)==5) N60_b_top+=scaling_factor;
	    if(fabs(jet_truthflav_leading)==5) NTot_b_top+=scaling_factor;
	    if(fabs(jet_truthflav_leading)!=5) NTot_nob_top+=scaling_factor;
	    //flavour of matched parton
	    if(leading_jet_index == index_matched){
	      if(m_sampleType == 1) NTot_b_t+=scaling_factor;
	      else if(m_sampleType == 14) {
	        if(fabs(t_b_pdgId)==5) NTot_b_t+=scaling_factor;
		else if(fabs(t_b_pdgId)!=5) NTot_nob_t+=scaling_factor;
	      }
	    } else if(leading_jet_index == index_matched_bbar){
	      if(m_sampleType == 1) NTot_b_tbar+=scaling_factor;
	      else if(m_sampleType == 14) {
	        if(fabs(t_bbar_pdgId)==5) NTot_b_tbar+=scaling_factor;
		else if(fabs(t_bbar_pdgId)!=5) NTot_nob_tbar+=scaling_factor;
	      }
	    }
	}

	if((subleading_jet_index == index_matched) || (subleading_jet_index == index_matched_bbar)){
	    //binned jet pt
	    h_Pt_jet2_top->Fill(jet_pt->at(subleading_jet_index)/1000.,scaling_factor);
	    h_Eta_jet2_top->Fill(jet_eta->at(subleading_jet_index),scaling_factor);
	    if(fabs(jet_truthflav_subleading)==5) h_Pt_jet2_btop->Fill(jet_pt->at(subleading_jet_index)/1000.,scaling_factor);
	    if(fabs(jet_truthflav_subleading)!=5) h_Pt_jet2_qtop->Fill(jet_pt->at(subleading_jet_index)/1000.,scaling_factor);
	    if(jet_mv2c10_subleading>0.64 && fabs(jet_truthflav_subleading)==5) h_tagged_Pt_jet2_btop->Fill(jet_pt->at(subleading_jet_index)/1000.,scaling_factor);
	    if(jet_mv2c10_subleading>0.64 && fabs(jet_truthflav_subleading)!=5) h_tagged_Pt_jet2_qtop->Fill(jet_pt->at(subleading_jet_index)/1000.,scaling_factor);
	    //inclusive
	    if(jet_mv2c10_subleading>0.11) N85_top+=scaling_factor;
	    if(jet_mv2c10_subleading>0.64) N77_top+=scaling_factor;
	    if(jet_mv2c10_subleading>0.83) N70_top+=scaling_factor;
	    if(jet_mv2c10_subleading>0.94) N60_top+=scaling_factor;
	    if(jet_mv2c10_subleading>0.11 && fabs(jet_truthflav_subleading)!=5) N85_nonb_top+=scaling_factor;
	    if(jet_mv2c10_subleading>0.64 && fabs(jet_truthflav_subleading)!=5) N77_nonb_top+=scaling_factor;
	    if(jet_mv2c10_subleading>0.83 && fabs(jet_truthflav_subleading)!=5) N70_nonb_top+=scaling_factor;
	    if(jet_mv2c10_subleading>0.94 && fabs(jet_truthflav_subleading)!=5) N60_nonb_top+=scaling_factor;
	    if(jet_mv2c10_subleading>0.11 && fabs(jet_truthflav_subleading)==5) N85_b_top+=scaling_factor;
	    if(jet_mv2c10_subleading>0.64 && fabs(jet_truthflav_subleading)==5) N77_b_top+=scaling_factor;
	    if(jet_mv2c10_subleading>0.83 && fabs(jet_truthflav_subleading)==5) N70_b_top+=scaling_factor;
	    if(jet_mv2c10_subleading>0.94 && fabs(jet_truthflav_subleading)==5) N60_b_top+=scaling_factor;
	    if(fabs(jet_truthflav_subleading)==5) NTot_b_top+=scaling_factor;
	    if(fabs(jet_truthflav_subleading)!=5) NTot_nob_top+=scaling_factor;
	    //flavour of matched parton
	    if(subleading_jet_index == index_matched){
	      if(m_sampleType == 1) NTot_b_t+=scaling_factor;
	      else if(m_sampleType == 14) {
	        if(fabs(t_b_pdgId)==5) NTot_b_t+=scaling_factor;
		else if(fabs(t_b_pdgId)!=5) NTot_nob_t+=scaling_factor;
	      }
	    } else if(subleading_jet_index == index_matched_bbar){
	      if(m_sampleType == 1) NTot_b_tbar+=scaling_factor;
	      else if(m_sampleType == 14) {
	        if(fabs(t_bbar_pdgId)==5) NTot_b_tbar+=scaling_factor;
		else if(fabs(t_bbar_pdgId)!=5) NTot_nob_tbar+=scaling_factor;
	      }
	    }
	}

	if((leading_jet_index != index_matched) && (leading_jet_index != index_matched_bbar)){
	    //binned jet pt
	    TLorentzVector temp_jet = RecoJet_TL(leading_jet_index);
	    if(fabs(jet_truthflav_leading)==5) h_Pt_jet_bnotop->Fill(jet_pt->at(leading_jet_index)/1000.,scaling_factor);
	    if(fabs(jet_truthflav_leading)!=5) h_Pt_jet_qnotop->Fill(jet_pt->at(leading_jet_index)/1000.,scaling_factor);
	    if(fabs(jet_truthflav_leading)==5) h_P_jet_bnotop->Fill(temp_jet.P()/1000.,scaling_factor);
	    if(fabs(jet_truthflav_leading)!=5) h_P_jet_qnotop->Fill(temp_jet.P()/1000.,scaling_factor);
	    if(fabs(jet_truthflav_leading)==5) h_Pt_jet1_bnotop->Fill(jet_pt->at(leading_jet_index)/1000.,scaling_factor);
	    if(fabs(jet_truthflav_leading)!=5) h_Pt_jet1_qnotop->Fill(jet_pt->at(leading_jet_index)/1000.,scaling_factor);
	    if(jet_mv2c10_leading>0.64 && fabs(jet_truthflav_leading)==5) h_tagged_Pt_jet_bnotop->Fill(jet_pt->at(leading_jet_index)/1000.,scaling_factor);
	    if(jet_mv2c10_leading>0.64 && fabs(jet_truthflav_leading)!=5) h_tagged_Pt_jet_qnotop->Fill(jet_pt->at(leading_jet_index)/1000.,scaling_factor);
	    if(jet_mv2c10_leading>0.64 && fabs(jet_truthflav_leading)==5) h_tagged_P_jet_bnotop->Fill(temp_jet.P()/1000.,scaling_factor);
	    if(jet_mv2c10_leading>0.64 && fabs(jet_truthflav_leading)!=5) h_tagged_P_jet_qnotop->Fill(temp_jet.P()/1000.,scaling_factor);
	    if(jet_mv2c10_leading>0.64 && fabs(jet_truthflav_leading)==5) h_tagged_Pt_jet1_bnotop->Fill(jet_pt->at(leading_jet_index)/1000.,scaling_factor);
	    if(jet_mv2c10_leading>0.64 && fabs(jet_truthflav_leading)!=5) h_tagged_Pt_jet1_qnotop->Fill(jet_pt->at(leading_jet_index)/1000.,scaling_factor);
	    //inclusive
	    if(jet_mv2c10_leading>0.11) N85_notop+=scaling_factor;
	    if(jet_mv2c10_leading>0.64) N77_notop+=scaling_factor;
	    if(jet_mv2c10_leading>0.83) N70_notop+=scaling_factor;
	    if(jet_mv2c10_leading>0.94) N60_notop+=scaling_factor;
	    if(jet_mv2c10_leading>0.11 && fabs(jet_truthflav_leading)!=5) N85_nonb_notop+=scaling_factor;
	    if(jet_mv2c10_leading>0.64 && fabs(jet_truthflav_leading)!=5) N77_nonb_notop+=scaling_factor;
	    if(jet_mv2c10_leading>0.83 && fabs(jet_truthflav_leading)!=5) N70_nonb_notop+=scaling_factor;
	    if(jet_mv2c10_leading>0.94 && fabs(jet_truthflav_leading)!=5) N60_nonb_notop+=scaling_factor;
	    if(jet_mv2c10_leading>0.11 && fabs(jet_truthflav_leading)==5) N85_b_notop+=scaling_factor;
	    if(jet_mv2c10_leading>0.64 && fabs(jet_truthflav_leading)==5) N77_b_notop+=scaling_factor;
	    if(jet_mv2c10_leading>0.83 && fabs(jet_truthflav_leading)==5) N70_b_notop+=scaling_factor;
	    if(jet_mv2c10_leading>0.94 && fabs(jet_truthflav_leading)==5) N60_b_notop+=scaling_factor;
	    if(fabs(jet_truthflav_leading)==5) NTot_b_notop+=scaling_factor;
	    if(fabs(jet_truthflav_leading)!=5) NTot_nob_notop+=scaling_factor;
	    h_Ml_jet_notmatch_bottom->Fill((truth_lep_top+temp_jet).M()/1000., scaling_factor);
            h_Mtop_jet_notmatch_bottom->Fill((truth_lep_top+temp_jet+truth_nu_top).M()/1000., scaling_factor);
            if(fabs(jet_truthflav_leading)==5) h_Mtop_bjet_notmatch_bottom->Fill((truth_lep_top+temp_jet+truth_nu_top).M()/1000., scaling_factor);
	}

	if((subleading_jet_index != index_matched) && (subleading_jet_index != index_matched_bbar)){
	    //binned jet pt
	    TLorentzVector temp_jet = RecoJet_TL(subleading_jet_index);
	    if(fabs(jet_truthflav_subleading)==5) h_Pt_jet_bnotop->Fill(jet_pt->at(subleading_jet_index)/1000.,scaling_factor);
	    if(fabs(jet_truthflav_subleading)!=5) h_Pt_jet_qnotop->Fill(jet_pt->at(subleading_jet_index)/1000.,scaling_factor);
	    if(fabs(jet_truthflav_subleading)==5) h_P_jet_bnotop->Fill(temp_jet.P()/1000.,scaling_factor);
	    if(fabs(jet_truthflav_subleading)!=5) h_P_jet_qnotop->Fill(temp_jet.P()/1000.,scaling_factor);
	    if(fabs(jet_truthflav_subleading)==5) h_Pt_jet2_bnotop->Fill(jet_pt->at(subleading_jet_index)/1000.,scaling_factor);
	    if(fabs(jet_truthflav_subleading)!=5) h_Pt_jet2_qnotop->Fill(jet_pt->at(subleading_jet_index)/1000.,scaling_factor);
	    if(jet_mv2c10_subleading>0.64 && fabs(jet_truthflav_subleading)==5) h_tagged_Pt_jet_bnotop->Fill(jet_pt->at(subleading_jet_index)/1000.,scaling_factor);
	    if(jet_mv2c10_subleading>0.64 && fabs(jet_truthflav_subleading)!=5) h_tagged_Pt_jet_qnotop->Fill(jet_pt->at(subleading_jet_index)/1000.,scaling_factor);
	    if(jet_mv2c10_subleading>0.64 && fabs(jet_truthflav_subleading)==5) h_tagged_P_jet_bnotop->Fill(temp_jet.P()/1000.,scaling_factor);
	    if(jet_mv2c10_subleading>0.64 && fabs(jet_truthflav_subleading)!=5) h_tagged_P_jet_qnotop->Fill(temp_jet.P()/1000.,scaling_factor);
	    if(jet_mv2c10_subleading>0.64 && fabs(jet_truthflav_subleading)==5) h_tagged_Pt_jet2_bnotop->Fill(jet_pt->at(subleading_jet_index)/1000.,scaling_factor);
	    if(jet_mv2c10_subleading>0.64 && fabs(jet_truthflav_subleading)!=5) h_tagged_Pt_jet2_qnotop->Fill(jet_pt->at(subleading_jet_index)/1000.,scaling_factor);
	    //inclusive
	    if(jet_mv2c10_subleading>0.11) N85_notop+=scaling_factor;
	    if(jet_mv2c10_subleading>0.64) N77_notop+=scaling_factor;
	    if(jet_mv2c10_subleading>0.83) N70_notop+=scaling_factor;
	    if(jet_mv2c10_subleading>0.94) N60_notop+=scaling_factor;
	    if(jet_mv2c10_subleading>0.11 && fabs(jet_truthflav_subleading)!=5) N85_nonb_notop+=scaling_factor;
	    if(jet_mv2c10_subleading>0.64 && fabs(jet_truthflav_subleading)!=5) N77_nonb_notop+=scaling_factor;
	    if(jet_mv2c10_subleading>0.83 && fabs(jet_truthflav_subleading)!=5) N70_nonb_notop+=scaling_factor;
	    if(jet_mv2c10_subleading>0.94 && fabs(jet_truthflav_subleading)!=5) N60_nonb_notop+=scaling_factor;
	    if(jet_mv2c10_subleading>0.11 && fabs(jet_truthflav_subleading)==5) N85_b_notop+=scaling_factor;
	    if(jet_mv2c10_subleading>0.64 && fabs(jet_truthflav_subleading)==5) N77_b_notop+=scaling_factor;
	    if(jet_mv2c10_subleading>0.83 && fabs(jet_truthflav_subleading)==5) N70_b_notop+=scaling_factor;
	    if(jet_mv2c10_subleading>0.94 && fabs(jet_truthflav_subleading)==5) N60_b_notop+=scaling_factor;
	    if(fabs(jet_truthflav_subleading)==5) NTot_b_notop+=scaling_factor;
	    if(fabs(jet_truthflav_subleading)!=5) NTot_nob_notop+=scaling_factor;
	    h_Ml_jet_notmatch_bottom->Fill((truth_lep_top+temp_jet).M()/1000., scaling_factor);
            h_Mtop_jet_notmatch_bottom->Fill((truth_lep_top+temp_jet+truth_nu_top).M()/1000., scaling_factor);
            if(fabs(jet_truthflav_subleading)==5) h_Mtop_bjet_notmatch_bottom->Fill((truth_lep_top+temp_jet+truth_nu_top).M()/1000., scaling_factor);
	}

	if((leading_jet_index != index_matched) && (leading_jet_index != index_matched_bbar)){
	  if((subleading_jet_index != index_matched) && (subleading_jet_index != index_matched_bbar)){
	    h2_alpha_0_jetpt->Fill(jet_pt->at(leading_jet_index)/1000., jet_pt->at(subleading_jet_index)/1000., scaling_factor);
	  } else if((subleading_jet_index == index_matched) || (subleading_jet_index == index_matched_bbar)){
	    h2_alpha_1_jetpt->Fill(jet_pt->at(leading_jet_index)/1000., jet_pt->at(subleading_jet_index)/1000., scaling_factor);
	    h2_alpha_1_subleadjetpt->Fill(jet_pt->at(leading_jet_index)/1000., jet_pt->at(subleading_jet_index)/1000., scaling_factor);
	  }
	} else if((leading_jet_index == index_matched) || (leading_jet_index == index_matched_bbar)){
	  if((subleading_jet_index != index_matched) && (subleading_jet_index != index_matched_bbar)){
	    h2_alpha_1_jetpt->Fill(jet_pt->at(leading_jet_index)/1000., jet_pt->at(subleading_jet_index)/1000., scaling_factor);
	    h2_alpha_1_leadjetpt->Fill(jet_pt->at(leading_jet_index)/1000., jet_pt->at(subleading_jet_index)/1000., scaling_factor);
	  } else if((subleading_jet_index == index_matched) || (subleading_jet_index == index_matched_bbar)){
	    h2_alpha_2_jetpt->Fill(jet_pt->at(leading_jet_index)/1000., jet_pt->at(subleading_jet_index)/1000., scaling_factor);
	  }
	}

	if(nmatch==0 && nmatch_bbar==0){
	Nmatch_alpha0 +=scaling_factor;
	Nevents_alpha += scaling_factor;
	}
	if(nmatch==1 && nmatch_bbar==0){
	Nmatch_alpha1 +=scaling_factor;
	Nevents_alpha += scaling_factor;
	}
	if(nmatch==0 && nmatch_bbar==1){
	Nmatch_alpha1 +=scaling_factor;
	Nevents_alpha += scaling_factor;
	}
	if(nmatch==1 && nmatch_bbar==1){
	Nmatch_alpha2 +=scaling_factor;
	Nevents_alpha += scaling_factor;
	}

	for(UInt_t i=0; i<GoodJets.size(); i++){

	UInt_t k = GoodJets.at(i);
	TLorentzVector temp_jet = RecoJet_TL(k);
	Double_t Mlplusj = (TL_lplus+temp_jet).M();
	h3_Mlplusj->Fill(leading_jet.Pt()/1000., subleading_jet.Pt()/1000.,Mlplusj/1000.,scaling_factor);

	  if(nmatch==0 && nmatch_bbar==0){
	  h3_Mlplusj_0->Fill(leading_jet.Pt()/1000., subleading_jet.Pt()/1000.,Mlplusj/1000.,scaling_factor);
	  }
	  if(nmatch==1 && nmatch_bbar==0){
	  h3_Mlplusj_1->Fill(leading_jet.Pt()/1000., subleading_jet.Pt()/1000.,Mlplusj/1000.,scaling_factor);
	  }
	  if(nmatch==0 && nmatch_bbar==1){
	  h3_Mlplusj_1->Fill(leading_jet.Pt()/1000., subleading_jet.Pt()/1000.,Mlplusj/1000.,scaling_factor);
	  }
	  if(nmatch==1 && nmatch_bbar==1){
	  h3_Mlplusj_2->Fill(leading_jet.Pt()/1000., subleading_jet.Pt()/1000.,Mlplusj/1000.,scaling_factor);
	  }

	}

	if(nmatch == 0) continue;
	h_CutFlowUnw->Fill(12.);
	h_CutFlowWei->Fill(12.,scaling_factor);
	Nmatch_bj += scaling_factor;

	if(nmatch_bbar == 0) continue;
	h_CutFlowUnw->Fill(13.);
	h_CutFlowWei->Fill(13.,scaling_factor);

	//if(nmatch_bbar+nmatch!=2) continue;
	h_CutFlowUnw->Fill(14.);
	h_CutFlowWei->Fill(14.,scaling_factor);
	if(index_matched==index_matched_bbar) Nmatch_samejet++;
	//if(index_matched==index_matched_bbar) continue;
	
	TLorentzVector MatchJet_swap = RecoJet_TL(index_matched_bbar);
	Double_t Mlplusjmatch_swap = ((TL_lplus+MatchJet_swap).M());
	h_Mlplusjmatch_swap->Fill(Mlplusjmatch_swap/1000.,scaling_factor);

	TLorentzVector MatchJet_bpart = RecoJet_TL(index_matched);
	Double_t Mlplusjmatch_corr = ((TL_lplus+MatchJet_bpart).M());
	h_DeltaR_lep_jet_corr->Fill(TL_lplus.DeltaR(MatchJet_bpart), scaling_factor);
	h_Mlplusjmatch_corr->Fill(Mlplusjmatch_corr/1000.,scaling_factor);
	h3_Mlplusjmatch_corr->Fill(jet_pt->at(leading_jet_index)/1000., jet_pt->at(subleading_jet_index)/1000., Mlplusjmatch_corr/1000.,scaling_factor);
	

        GoodJets.clear();

      } //chiude il loop sugli eventi


cout<<""<<endl;
cout<<"=== Events with one jet matched to b and bbar	"<<Nmatch_samejet<<endl;
cout<<""<<endl;
cout<<"=== EXIT FROM LOOP		"<<endl;
cout<<"=== Nmodel_153			"<<Nmodel_153<<endl;
cout<<"=== Nmodel			"<<Nmodel<<endl;
cout<<"=== f = Nmodel_153/Nmodel	"<<Nmodel_153/Nmodel<<endl;
Nmis = Nmis_153/(Nmodel_153/Nmodel);
cout<<"=== Nmis				"<<Nmis<<endl;
cout<<"=== Nmis				"<<Nmis<<endl;
cout<<"=== Nmis_153			"<<Nmis_153<<endl;
cout<<"=== f = Nmis_153/Nmis		"<<Nmis_153/Nmis<<endl;
cout<<"=== Nevents			"<<Nevents<<endl;
cout<<"==="<<endl;
cout<<"=== alpha	"<<"(Nmodel - Nmis)/(Nevents)"<<endl;
cout<<"=== alpha	"<<(Nmodel - Nmis)/(Nevents)<<endl;
cout<<"============================"<<endl;
cout<<"SECONDO MODELLO"<<endl;
cout<<"=== Nmodel_2_153			"<<Nmodel_2_153<<endl;
cout<<"=== Nmodel_2			"<<Nmodel_2<<endl;
cout<<"=== f_2 = Nmodel_2_153/Nmodel_2	"<<Nmodel_2_153/Nmodel_2<<endl;
Nmis_2 = Nmis_153/(Nmodel_2_153/Nmodel_2);
cout<<"=== alpha_2	"<<(Nmodel - Nmis_2)/(Nevents)<<endl;


cout<<"**************************"<<endl;
cout<<"=== Rapporto tra i jet matchati ad un quark e il numero di eventi"<<endl;
cout<<"=== alpha TRUTE Nmatch_bj/Nevents	 "<<Nmatch_bj/Nevents<<"	pm	 "<<sqrt(((Nmatch_bj/Nevents)*(1-(Nmatch_bj/Nevents)))/Nevents)<<endl;

cout<<"**************************"<<endl;
cout<<"=== Rapporto Mis tag rate"<<endl;
cout<<"=== Nmatch_charmj/Nmatch_topj	 "<<Nmatch_charmj/Nmatch_topj<<"	pm	 "<<sqrt(((Nmatch_charmj/Nmatch_topj)*(1-(Nmatch_charmj/Nmatch_topj)))/Nmatch_topj)<<endl;


cout<<"**************************"<<endl;
cout<<"=== ALPHA0	"<<Nmatch_alpha0/Nevents<<"	pm	"<<sqrt(((Nmatch_alpha0/Nevents)*(1-(Nmatch_alpha0/Nevents)))/Nevents)<<endl;
cout<<"=== ALPHA1	"<<Nmatch_alpha1/Nevents<<"	pm	"<<sqrt(((Nmatch_alpha1/Nevents)*(1-(Nmatch_alpha1/Nevents)))/Nevents)<<endl;
cout<<"=== ALPHA2	"<<Nmatch_alpha2/Nevents<<"	pm	"<<sqrt(((Nmatch_alpha2/Nevents)*(1-(Nmatch_alpha2/Nevents)))/Nevents)<<endl;
 
 h_Fakes->GetXaxis()->SetBinLabel(1,"Nevents nfakes>1");
 h_Fakes->GetXaxis()->SetBinLabel(2,"Nevents nfakes>1 ele");
 h_Fakes->GetXaxis()->SetBinLabel(3,"Nevents nfakes>1 muon");
 h_Fakes->SetBinContent(1, Nevents_fakes);
 h_Fakes->SetBinContent(2, Nevents_fakeisele);
 h_Fakes->SetBinContent(3, Nevents_fakeismuon);

 h_alphatruth->GetXaxis()->SetBinLabel(1,"Nevents");
 h_alphatruth->GetXaxis()->SetBinLabel(2,"Nmatch_alpha0");
 h_alphatruth->GetXaxis()->SetBinLabel(3,"Nmatch_alpha1");
 h_alphatruth->GetXaxis()->SetBinLabel(4,"Nmatch_alpha2");
 h_alphatruth->SetBinContent(1, Nevents_alpha);
 h_alphatruth->SetBinContent(2, Nmatch_alpha0);
 h_alphatruth->SetBinContent(3, Nmatch_alpha1);
 h_alphatruth->SetBinContent(4, Nmatch_alpha2);

 h_mistagrates->GetXaxis()->SetBinLabel(1,"N85");
 h_mistagrates->GetXaxis()->SetBinLabel(2,"N77");
 h_mistagrates->GetXaxis()->SetBinLabel(3,"N70");
 h_mistagrates->GetXaxis()->SetBinLabel(4,"N60");
 h_mistagrates->GetXaxis()->SetBinLabel(5,"N85_nonb");
 h_mistagrates->GetXaxis()->SetBinLabel(6,"N77_nonb");
 h_mistagrates->GetXaxis()->SetBinLabel(7,"N70_nonb");
 h_mistagrates->GetXaxis()->SetBinLabel(8,"N60_nonb");
 h_mistagrates->SetBinContent(1,N85);
 h_mistagrates->SetBinContent(2,N77);
 h_mistagrates->SetBinContent(3,N70);
 h_mistagrates->SetBinContent(4,N60);
 h_mistagrates->SetBinContent(5,N85_nonb);
 h_mistagrates->SetBinContent(6,N77_nonb);
 h_mistagrates->SetBinContent(7,N70_nonb);
 h_mistagrates->SetBinContent(8,N60_nonb);

 h_btageff->GetXaxis()->SetBinLabel(1,"NTot_b");
 h_btageff->GetXaxis()->SetBinLabel(2,"N85_b");
 h_btageff->GetXaxis()->SetBinLabel(3,"N77_b");
 h_btageff->GetXaxis()->SetBinLabel(4,"N70_b");
 h_btageff->GetXaxis()->SetBinLabel(5,"N60_b");
 h_btageff->GetXaxis()->SetBinLabel(6,"NTot_nob");
 h_btageff->SetBinContent(1,NTot_b);
 h_btageff->SetBinContent(2,N85_b);
 h_btageff->SetBinContent(3,N77_b);
 h_btageff->SetBinContent(4,N70_b);
 h_btageff->SetBinContent(5,N60_b);
 h_btageff->SetBinContent(6,NTot_nob);

 h_mistagrates_notop->GetXaxis()->SetBinLabel(1,"N85_notop");
 h_mistagrates_notop->GetXaxis()->SetBinLabel(2,"N77_notop");
 h_mistagrates_notop->GetXaxis()->SetBinLabel(3,"N70_notop");
 h_mistagrates_notop->GetXaxis()->SetBinLabel(4,"N60_notop");
 h_mistagrates_notop->GetXaxis()->SetBinLabel(5,"N85_nonb_notop");
 h_mistagrates_notop->GetXaxis()->SetBinLabel(6,"N77_nonb_notop");
 h_mistagrates_notop->GetXaxis()->SetBinLabel(7,"N70_nonb_notop");
 h_mistagrates_notop->GetXaxis()->SetBinLabel(8,"N60_nonb_notop");
 h_mistagrates_notop->SetBinContent(1,N85_notop);
 h_mistagrates_notop->SetBinContent(2,N77_notop);
 h_mistagrates_notop->SetBinContent(3,N70_notop);
 h_mistagrates_notop->SetBinContent(4,N60_notop);
 h_mistagrates_notop->SetBinContent(5,N85_nonb_notop);
 h_mistagrates_notop->SetBinContent(6,N77_nonb_notop);
 h_mistagrates_notop->SetBinContent(7,N70_nonb_notop);
 h_mistagrates_notop->SetBinContent(8,N60_nonb_notop);

 h_btageff_notop->GetXaxis()->SetBinLabel(1,"NTot_b_notop");
 h_btageff_notop->GetXaxis()->SetBinLabel(2,"N85_b_notop");
 h_btageff_notop->GetXaxis()->SetBinLabel(3,"N77_b_notop");
 h_btageff_notop->GetXaxis()->SetBinLabel(4,"N70_b_notop");
 h_btageff_notop->GetXaxis()->SetBinLabel(5,"N60_b_notop");
 h_btageff_notop->GetXaxis()->SetBinLabel(6,"NTot_nob_notop");
 h_btageff_notop->SetBinContent(1,NTot_b_notop);
 h_btageff_notop->SetBinContent(2,N85_b_notop);
 h_btageff_notop->SetBinContent(3,N77_b_notop);
 h_btageff_notop->SetBinContent(4,N70_b_notop);
 h_btageff_notop->SetBinContent(5,N60_b_notop);
 h_btageff_notop->SetBinContent(6,NTot_nob_notop);

 h_mistagrates_top->GetXaxis()->SetBinLabel(1,"N85_top");
 h_mistagrates_top->GetXaxis()->SetBinLabel(2,"N77_top");
 h_mistagrates_top->GetXaxis()->SetBinLabel(3,"N70_top");
 h_mistagrates_top->GetXaxis()->SetBinLabel(4,"N60_top");
 h_mistagrates_top->GetXaxis()->SetBinLabel(5,"N85_nonb_top");
 h_mistagrates_top->GetXaxis()->SetBinLabel(6,"N77_nonb_top");
 h_mistagrates_top->GetXaxis()->SetBinLabel(7,"N70_nonb_top");
 h_mistagrates_top->GetXaxis()->SetBinLabel(8,"N60_nonb_top");
 h_mistagrates_top->SetBinContent(1,N85_top);
 h_mistagrates_top->SetBinContent(2,N77_top);
 h_mistagrates_top->SetBinContent(3,N70_top);
 h_mistagrates_top->SetBinContent(4,N60_top);
 h_mistagrates_top->SetBinContent(5,N85_nonb_top);
 h_mistagrates_top->SetBinContent(6,N77_nonb_top);
 h_mistagrates_top->SetBinContent(7,N70_nonb_top);
 h_mistagrates_top->SetBinContent(8,N60_nonb_top);

 h_btageff_top->GetXaxis()->SetBinLabel(1,"NTot_b_top");
 h_btageff_top->GetXaxis()->SetBinLabel(2,"N85_b_top");
 h_btageff_top->GetXaxis()->SetBinLabel(3,"N77_b_top");
 h_btageff_top->GetXaxis()->SetBinLabel(4,"N70_b_top");
 h_btageff_top->GetXaxis()->SetBinLabel(5,"N60_b_top");
 h_btageff_top->GetXaxis()->SetBinLabel(6,"NTot_nob_top");
 h_btageff_top->GetXaxis()->SetBinLabel(7,"NTot_b_t");
 h_btageff_top->GetXaxis()->SetBinLabel(8,"NTot_b_tbar");
 h_btageff_top->GetXaxis()->SetBinLabel(9,"NTot_q_t");
 h_btageff_top->GetXaxis()->SetBinLabel(10,"NTot_q_tbar");
 h_btageff_top->SetBinContent(1,NTot_b_top);
 h_btageff_top->SetBinContent(2,N85_b_top);
 h_btageff_top->SetBinContent(3,N77_b_top);
 h_btageff_top->SetBinContent(4,N70_b_top);
 h_btageff_top->SetBinContent(5,N60_b_top);
 h_btageff_top->SetBinContent(6,NTot_nob_top);
 h_btageff_top->SetBinContent(7,NTot_b_t);
 h_btageff_top->SetBinContent(8,NTot_b_tbar);
 h_btageff_top->SetBinContent(9,NTot_nob_t);
 h_btageff_top->SetBinContent(10,NTot_nob_tbar);

 h_CutFlowUnw->GetXaxis()->SetBinLabel(1,"All");
 h_CutFlowUnw->GetXaxis()->SetBinLabel(2,"TRG+Basic dilep");
 h_CutFlowUnw->GetXaxis()->SetBinLabel(3,"1 ele");  
 h_CutFlowUnw->GetXaxis()->SetBinLabel(4,"1 mu");
 h_CutFlowUnw->GetXaxis()->SetBinLabel(5,"OS");
 h_CutFlowUnw->GetXaxis()->SetBinLabel(6,"2 jets");
 h_CutFlowUnw->GetXaxis()->SetBinLabel(7,"HT>120");
 h_CutFlowUnw->GetXaxis()->SetBinLabel(8,"Mll>15");
 h_CutFlowUnw->GetXaxis()->SetBinLabel(9,"Channel");
 h_CutFlowUnw->GetXaxis()->SetBinLabel(10,"noTruth");
 h_CutFlowUnw->GetXaxis()->SetBinLabel(11,"no good bparton");
 h_CutFlowUnw->GetXaxis()->SetBinLabel(12,"jet-b match");
 h_CutFlowUnw->GetXaxis()->SetBinLabel(13,"jet-bbar match");
 h_CutFlowUnw->GetXaxis()->SetBinLabel(14,"bbar match");


 h_CutFlowWei->GetXaxis()->SetBinLabel(1,"All");
 h_CutFlowWei->GetXaxis()->SetBinLabel(2,"TRG+Basic dilep");
 h_CutFlowWei->GetXaxis()->SetBinLabel(3,"1 ele");  
 h_CutFlowWei->GetXaxis()->SetBinLabel(4,"1 mu");
 h_CutFlowWei->GetXaxis()->SetBinLabel(5,"OS");
 h_CutFlowWei->GetXaxis()->SetBinLabel(6,"2 jets");
 h_CutFlowWei->GetXaxis()->SetBinLabel(7,"HT>120");
 h_CutFlowWei->GetXaxis()->SetBinLabel(8,"Mll>15");
 h_CutFlowWei->GetXaxis()->SetBinLabel(9,"Channel");
 h_CutFlowWei->GetXaxis()->SetBinLabel(10,"noTruth");
 h_CutFlowWei->GetXaxis()->SetBinLabel(11,"no good bparton");
 h_CutFlowWei->GetXaxis()->SetBinLabel(12,"jet-b match");
 h_CutFlowWei->GetXaxis()->SetBinLabel(13,"jet-bbar match");
 h_CutFlowWei->GetXaxis()->SetBinLabel(14,"bbar match");

 vector<TH1F*> histos_1d;
 vector<TH2F*> histos_2d;
 vector<TH3F*> histos_3d;

 histos_1d.push_back(h_CutFlowUnw);
 histos_1d.push_back(h_CutFlowWei);
 histos_1d.push_back(h_nele);
 histos_1d.push_back(h_nmuo);
 histos_1d.push_back(h_nlep);
 histos_1d.push_back(h_njet);
 histos_1d.push_back(h_njet_bin);
 histos_1d.push_back(h_nbjet_77);
 histos_1d.push_back(h_0bjet_77);
 histos_1d.push_back(h_1bjet_77);
 histos_1d.push_back(h_2bjet_77);
 histos_1d.push_back(h_2jets_leppluspt);
 histos_1d.push_back(h_btagj_0_leppluspt);
 histos_1d.push_back(h_btagj_1_leppluspt);
 histos_1d.push_back(h_btagj_2_leppluspt);
 histos_1d.push_back(h_2jets_leppluseta);
 histos_1d.push_back(h_btagj_0_leppluseta);
 histos_1d.push_back(h_btagj_1_leppluseta);
 histos_1d.push_back(h_btagj_2_leppluseta);
 histos_1d.push_back(h_HT);
 histos_1d.push_back(h_Mll);
 histos_1d.push_back(h_met);
 histos_1d.push_back(h_mu);
 histos_1d.push_back(h_el_true_origin);
 histos_1d.push_back(h_mu_true_origin);
 histos_1d.push_back(h_Eta_ele);
 histos_1d.push_back(h_Eta_muon);
 histos_1d.push_back(h_Pt_ele);
 histos_1d.push_back(h_Pt_muon);
 histos_1d.push_back(h_DeltaR_emu);
 histos_1d.push_back(h_DeltaR_lep_jet);
 histos_1d.push_back(h_DeltaR_el_jet);
 histos_1d.push_back(h_DeltaR_mu_jet);
 histos_1d.push_back(h_Eta_jet1);
 histos_1d.push_back(h_Eta_jet2);
 histos_1d.push_back(h_Pt_jet1);
 histos_1d.push_back(h_Pt_jet2);
 histos_1d.push_back(h_DeltaR_j1j2);
 histos_1d.push_back(h_taggedEta_jet1);
 histos_1d.push_back(h_taggedEta_jet2);
 histos_1d.push_back(h_taggedPt_jet1);
 histos_1d.push_back(h_taggedPt_jet2);
 histos_1d.push_back(h_taggedDeltaR_j1j2);
 histos_1d.push_back(h_Mej);
 histos_1d.push_back(h_Mmj);
 histos_1d.push_back(h_Meqjet);
 histos_1d.push_back(h_Mmqjet);
 histos_1d.push_back(h_Mebjet);
 histos_1d.push_back(h_Mmbjet);
 histos_1d.push_back(h_Ml1j1);
 histos_1d.push_back(h_Ml2j2);
 histos_1d.push_back(h_Ml1j1_bb);
 histos_1d.push_back(h_Ml2j2_bb);
 histos_1d.push_back(h_Ml1j1_bl);
 histos_1d.push_back(h_Ml2j2_bl);
 histos_1d.push_back(h_Ml1j1_lb);
 histos_1d.push_back(h_Ml2j2_lb);
 histos_1d.push_back(h_Ml1j1_ll);
 histos_1d.push_back(h_Ml2j2_ll);
 histos_1d.push_back(h_Ml1qjet);
 histos_1d.push_back(h_Ml2qjet);
 histos_1d.push_back(h_Ml1bjet);
 histos_1d.push_back(h_Ml2bjet);
 histos_1d.push_back(h_Mlplusj);
 histos_1d.push_back(h_Mlminj);
 histos_1d.push_back(h_Mlplusj_model);
 histos_1d.push_back(h_DeltaR_lep_jet_model);
 histos_1d.push_back(h_Mlminj_model);
 histos_1d.push_back(h_Mlj_model2);
 histos_1d.push_back(h_DeltaR_lep_jet_model2);
 histos_1d.push_back(h_Pt_lplus);
 histos_1d.push_back(h_Pt_lplus_rotated);
 histos_1d.push_back(h_P_lplus);
 histos_1d.push_back(h_P_lplus_rotated);
 histos_1d.push_back(h_rot_phi);
 histos_1d.push_back(h_rot_eta);
 histos_1d.push_back(h_lep_phi);
 histos_1d.push_back(h_lep_eta);
 histos_1d.push_back(h_lep_theta);
 histos_1d.push_back(h_DeltaR_b_jet);
 histos_1d.push_back(h_DeltaPt_b_jet);
 histos_1d.push_back(h_DeltaR_bbar_jet);
 histos_1d.push_back(h_DeltaPt_bbar_jet);
 histos_1d.push_back(h_nbmatch);   
 histos_1d.push_back(h_nbbarmatch);   
 histos_1d.push_back(h_Meb_corr); 
 histos_1d.push_back(h_Mmb_corr); 
 histos_1d.push_back(h_Mlplusb_corr); 
 histos_1d.push_back(h_Mlminb_corr); 
 histos_1d.push_back(h_Mlplusb_uncorr); 
 histos_1d.push_back(h_Mlminb_uncorr); 
 histos_1d.push_back(h_Mlplusjmatch_corr);
 histos_1d.push_back(h_DeltaR_lep_jet_corr);
 histos_1d.push_back(h_Mlplusjmatch_swap);
 histos_1d.push_back(h_DeltaR_lplusb);
 histos_1d.push_back(h_DeltaR_lplusbbar);
 histos_1d.push_back(h_xsec_norm);
 histos_1d.push_back(h_weight_mc);
 histos_1d.push_back(h_weight_pileup);
 histos_1d.push_back(h_weight_jvt);
 histos_1d.push_back(h_weight_leptonSF);
 histos_1d.push_back(h_weight_bTagSF_MV2c10_77);
 histos_1d.push_back(h_alphatruth);
 histos_1d.push_back(h_mistagrates);
 histos_1d.push_back(h_btageff);
 histos_1d.push_back(h_mistagrates_notop);
 histos_1d.push_back(h_btageff_notop);
 histos_1d.push_back(h_mistagrates_top);
 histos_1d.push_back(h_btageff_top);
 histos_1d.push_back(h_Fakes);
 histos_1d.push_back(h_Eta_jet1_top);
 histos_1d.push_back(h_Eta_jet2_top);
 histos_1d.push_back(h_Eta_jet1_bnotop);
 histos_1d.push_back(h_Eta_jet2_bnotop);
 histos_1d.push_back(h_Eta_jet1_qnotop);
 histos_1d.push_back(h_Eta_jet2_qnotop);
 histos_1d.push_back(h_DeltaR_j1j2_top);
 histos_1d.push_back(h_DeltaR_j1j2_bnotop);
 histos_1d.push_back(h_DeltaR_j1j2_qnotop);

 histos_1d.push_back(h_tagged_Pt_jet_charm);
 histos_1d.push_back(h_Pt_jet_charm);
 histos_1d.push_back(h_tagged_Pt_jet_light);
 histos_1d.push_back(h_Pt_jet_light);
 histos_1d.push_back(h_Pt_jet_btop);
 histos_1d.push_back(h_Pt_jet_qtop);
 histos_1d.push_back(h_Pt_jet_bnotop);
 histos_1d.push_back(h_Pt_jet_qnotop);
 histos_1d.push_back(h_tagged_Pt_jet_btop);
 histos_1d.push_back(h_tagged_Pt_jet_qtop);
 histos_1d.push_back(h_tagged_Pt_jet_bnotop);
 histos_1d.push_back(h_tagged_Pt_jet_qnotop);

 histos_1d.push_back(h_P_jet_btop);
 histos_1d.push_back(h_P_jet_qtop);
 histos_1d.push_back(h_P_jet_bnotop);
 histos_1d.push_back(h_P_jet_qnotop);
 histos_1d.push_back(h_tagged_P_jet_btop);
 histos_1d.push_back(h_tagged_P_jet_qtop);
 histos_1d.push_back(h_tagged_P_jet_bnotop);
 histos_1d.push_back(h_tagged_P_jet_qnotop);

 histos_1d.push_back(h_Pt_jet1_top);
 histos_1d.push_back(h_Pt_jet2_top);
 histos_1d.push_back(h_Pt_jet1_btop);
 histos_1d.push_back(h_Pt_jet2_btop);
 histos_1d.push_back(h_Pt_jet1_qtop);
 histos_1d.push_back(h_Pt_jet2_qtop);

 histos_1d.push_back(h_tagged_Pt_jet1_btop);
 histos_1d.push_back(h_tagged_Pt_jet2_btop);
 histos_1d.push_back(h_tagged_Pt_jet1_qtop);
 histos_1d.push_back(h_tagged_Pt_jet2_qtop);

 histos_1d.push_back(h_Pt_jet1_bnotop);
 histos_1d.push_back(h_Pt_jet2_bnotop);
 histos_1d.push_back(h_Pt_jet1_qnotop);
 histos_1d.push_back(h_Pt_jet2_qnotop);

 histos_1d.push_back(h_tagged_Pt_jet1_bnotop);
 histos_1d.push_back(h_tagged_Pt_jet2_bnotop);
 histos_1d.push_back(h_tagged_Pt_jet1_qnotop);
 histos_1d.push_back(h_tagged_Pt_jet2_qnotop);

 histos_1d.push_back(h_Mtop_bottom);
 histos_1d.push_back(h_Ml_jet_match_bottom);
 histos_1d.push_back(h_Mtop_jet_match_bottom);
 histos_1d.push_back(h_Ml_jet_notmatch_bottom);
 histos_1d.push_back(h_Mtop_jet_notmatch_bottom);
 histos_1d.push_back(h_Mtop_bjet_notmatch_bottom);
        
 histos_1d.push_back(h_truth_partonfromtop_pdgId);
 histos_1d.push_back(h_truth_partonfromtopbar_pdgId);

 histos_2d.push_back(h2_Ml1j1_Ml2j2);
 histos_2d.push_back(h2_Mlplusj);
 histos_2d.push_back(h2_alpha_0_jetpt);
 histos_2d.push_back(h2_alpha_1_jetpt);
 histos_2d.push_back(h2_alpha_2_jetpt);
 histos_2d.push_back(h2_alpha_1_leadjetpt);
 histos_2d.push_back(h2_alpha_1_subleadjetpt);

 histos_2d.push_back(h2_btagj_0_jetpt);
 histos_2d.push_back(h2_btagj_1_jetpt);
 histos_2d.push_back(h2_btagj_2_jetpt);
 histos_2d.push_back(h2_2jets_jetpt);
 histos_2d.push_back(h2_btagj_1_leadjetpt);
 histos_2d.push_back(h2_btagj_1_subleadjetpt);
 histos_2d.push_back(h2_tagged_jet_eta_jet_pt);
 histos_2d.push_back(h2_jet_eta_jet_pt);

 histos_3d.push_back(h3_Mlplusj);
 histos_3d.push_back(h3_Mlplusj_0);
 histos_3d.push_back(h3_Mlplusj_1);
 histos_3d.push_back(h3_Mlplusj_2);
 histos_3d.push_back(h3_Mlplusj_model);
 histos_3d.push_back(h3_Mlplusjmatch_corr);

 SaveMyHistos(histos_1d, histos_2d, histos_3d, m_sampleType, m_index_name, m_TreeName, m_isMC16a, m_isMC16d, m_isMC16e, region, OS_or_SS);
 delete tPL;
 m_inputFileListName = "";
 histos_1d.clear();
 histos_2d.clear();
 histos_3d.clear();

}



void Looper::SaveMyHistos(vector<TH1F*> vector_1dhisto, vector<TH2F*> vector_2dhisto, vector<TH3F*> vector_3dhisto, int sampleType, int index_name, TString treename, int m_isMC16a, int m_isMC16d, int m_isMC16e, TString region, TString OS_or_SS)
{
  std::cout <<""<<std::endl;
  std::cout << "=== Saving histos.." << std::endl;
    
  std::ostringstream dsid_stream;
  if(m_isMC16a)  dsid_stream << "./output_mc16a/";
  if(m_isMC16d)  dsid_stream << "./output_mc16d/";
  if(m_isMC16e)  dsid_stream << "./output_mc16e/";
  dsid_stream << treename;
  dsid_stream << "/sampleType_";
  dsid_stream << sampleType;
  dsid_stream << "/output_";
  dsid_stream << index_name;
  dsid_stream << "_";
  dsid_stream << region;
  dsid_stream << "_";
  dsid_stream << OS_or_SS;
  dsid_stream << ".root";
  std::string dsid_string = dsid_stream.str();
  char *dsid_char = const_cast<char*>(dsid_string.c_str());

  //this was causing lot of warnings, now sumw2 at the beginning of Loop() function
  //histos_sumw2(vector_1dhisto, vector_2dhisto, vector_3dhisto);

  TFile *gg = new TFile(dsid_char,"RECREATE");
  gg->cd();
  TDirectory* OS_or_SS_DIR = gg->mkdir(OS_or_SS);
  OS_or_SS_DIR->cd();
  TDirectory* region_dir = OS_or_SS_DIR->mkdir(region);
  region_dir->cd();
  
  for(UInt_t i =0; i<vector_1dhisto.size(); i++) vector_1dhisto.at(i)->Write();
  for(UInt_t i =0; i<vector_2dhisto.size(); i++) vector_2dhisto.at(i)->Write();
  for(UInt_t i =0; i<vector_3dhisto.size(); i++) vector_3dhisto.at(i)->Write();
  gg->Close();
  histos_clear(vector_1dhisto, vector_2dhisto, vector_3dhisto);
}


void Looper::histos_sumw2(vector<TH1F*> vector_1dhisto, vector<TH2F*> vector_2dhisto, vector<TH3F*> vector_3dhisto)
{
  
  for(UInt_t i =0; i<vector_1dhisto.size(); i++) vector_1dhisto.at(i)->Sumw2();
  for(UInt_t i =0; i<vector_2dhisto.size(); i++) vector_2dhisto.at(i)->Sumw2();
  for(UInt_t i =0; i<vector_3dhisto.size(); i++) vector_3dhisto.at(i)->Sumw2();

}

void Looper::histos_clear(vector<TH1F*> vector_1dhisto, vector<TH2F*> vector_2dhisto, vector<TH3F*> vector_3dhisto)
{
  for(UInt_t i =0; i<vector_1dhisto.size(); i++) vector_1dhisto.at(i)->Reset("ICESM");
  for(UInt_t i =0; i<vector_2dhisto.size(); i++) vector_2dhisto.at(i)->Reset("ICESM");
  for(UInt_t i =0; i<vector_3dhisto.size(); i++) vector_3dhisto.at(i)->Reset("ICESM");

}

Double_t Looper::xsecWeight(Int_t dsid, int isMC16a, int isMC16d, int isMC16e, TString treename)  //dsid stands for "DatasetID"       
{
  cout<<endl;
  cout<<"**************I'm running on dsid = "<<dsid<<endl;
  
  //https://twiki.cern.ch/twiki/bin/view/AtlasProtected/TopDerivationMC16aList
  Float_t luminosity = 1.;
  if(isMC16a) luminosity = 3219.56+32988.1;
  if(isMC16d) luminosity = 44307.4;
  if(isMC16e) luminosity = 58450.1;
  Float_t sampleNEvt = 1.0;
  Double_t sampleXSec = 1.0;
  Float_t weight_factor = 1.0;

  //NEED TO BE CHECK FOR EVERY NEW NTUPLE PRODUCTION
  //HARCODED IN ORDER TO SHORTEN RUNTIME
  //ATv98
  if(dsid == 410472){
    sampleNEvt = Retrieve_ttbar_nominal_sampleNEvt(isMC16a, isMC16d, isMC16e, treename);
    } else {
    string string_dsid = std::to_string(dsid);
    TString name_list_dsid = "";
    if(isMC16a) name_list_dsid = "data_MC16a/"+string_dsid;
    if(isMC16d) name_list_dsid = "data_MC16d/"+string_dsid;
    if(isMC16e) name_list_dsid = "data_MC16e/"+string_dsid;
    ifstream filein(name_list_dsid);
    if(!filein) cout << "Error opening input file " <<endl;
    if(dsid == 410482 && treename == "RadHi"){ //ttbar RadHi variation on special sample
      while(filein.good()){
      TString name_root;
      filein>>name_root;
      if(name_root=="") break;
       TFile *xsecf = TFile::Open(name_root);  
       TTree *totnorm = (TTree*)xsecf->Get("sumWeights");
       vector<float> *totalEventsWeighted_mc_generator_weights;
       TBranch *b_totalEventsWeighted_mc_generator_weights;   //!
       totalEventsWeighted_mc_generator_weights = 0;
       totnorm->SetBranchAddress("totalEventsWeighted_mc_generator_weights",&totalEventsWeighted_mc_generator_weights,&b_totalEventsWeighted_mc_generator_weights);
       for (int ent = 0; ent < totnorm->GetEntries(); ent++){
       totnorm->GetEntry(ent);
       sampleNEvt += totalEventsWeighted_mc_generator_weights->at(5)*totalEventsWeighted_mc_generator_weights->at(193)/totalEventsWeighted_mc_generator_weights->at(0);
       }
       delete totnorm;
       xsecf->Close();
      }
    } else if((dsid == 410648 || dsid == 410649) && treename == "RadHi"){ //singletop RadHi variation
      while(filein.good()){
      TString name_root;
      filein>>name_root;
      if(name_root=="") break;
       TFile *xsecf = TFile::Open(name_root);  
       TTree *totnorm = (TTree*)xsecf->Get("sumWeights");
       vector<float> *totalEventsWeighted_mc_generator_weights;
       TBranch *b_totalEventsWeighted_mc_generator_weights;   //!
       totalEventsWeighted_mc_generator_weights = 0;
       totnorm->SetBranchAddress("totalEventsWeighted_mc_generator_weights",&totalEventsWeighted_mc_generator_weights,&b_totalEventsWeighted_mc_generator_weights);
       for (int ent = 0; ent < totnorm->GetEntries(); ent++){
       totnorm->GetEntry(ent);
       sampleNEvt += totalEventsWeighted_mc_generator_weights->at(5)*totalEventsWeighted_mc_generator_weights->at(142)/totalEventsWeighted_mc_generator_weights->at(0);
       }
       delete totnorm;
       xsecf->Close();
      }
    } else if((dsid == 410648 || dsid == 410649) && treename == "RadLo"){ //singletop RadLo variation
      while(filein.good()){
      TString name_root;
      filein>>name_root;
      if(name_root=="") break;
       TFile *xsecf = TFile::Open(name_root);  
       TTree *totnorm = (TTree*)xsecf->Get("sumWeights");
       vector<float> *totalEventsWeighted_mc_generator_weights;
       TBranch *b_totalEventsWeighted_mc_generator_weights;   //!
       totalEventsWeighted_mc_generator_weights = 0;
       totnorm->SetBranchAddress("totalEventsWeighted_mc_generator_weights",&totalEventsWeighted_mc_generator_weights,&b_totalEventsWeighted_mc_generator_weights);
       for (int ent = 0; ent < totnorm->GetEntries(); ent++){
       totnorm->GetEntry(ent);
       sampleNEvt += totalEventsWeighted_mc_generator_weights->at(6)*totalEventsWeighted_mc_generator_weights->at(143)/totalEventsWeighted_mc_generator_weights->at(0);
       }
       delete totnorm;
       xsecf->Close();
      }
    } else if((dsid == 410658 || dsid == 410659) && treename == "RadHi"){ //singletop RadHi variation
      while(filein.good()){
      TString name_root;
      filein>>name_root;
      if(name_root=="") break;
       TFile *xsecf = TFile::Open(name_root);  
       TTree *totnorm = (TTree*)xsecf->Get("sumWeights");
       vector<float> *totalEventsWeighted_mc_generator_weights;
       TBranch *b_totalEventsWeighted_mc_generator_weights;   //!
       totalEventsWeighted_mc_generator_weights = 0;
       totnorm->SetBranchAddress("totalEventsWeighted_mc_generator_weights",&totalEventsWeighted_mc_generator_weights,&b_totalEventsWeighted_mc_generator_weights);
       for (int ent = 0; ent < totnorm->GetEntries(); ent++){
       totnorm->GetEntry(ent);
       sampleNEvt += totalEventsWeighted_mc_generator_weights->at(5)*totalEventsWeighted_mc_generator_weights->at(166)/totalEventsWeighted_mc_generator_weights->at(0);
       }
       delete totnorm;
       xsecf->Close();
      }
    } else if((dsid == 410658 || dsid == 410659) && treename == "RadLo"){ //singletop RadLo variation
      while(filein.good()){
      TString name_root;
      filein>>name_root;
      if(name_root=="") break;
       TFile *xsecf = TFile::Open(name_root);  
       TTree *totnorm = (TTree*)xsecf->Get("sumWeights");
       vector<float> *totalEventsWeighted_mc_generator_weights;
       TBranch *b_totalEventsWeighted_mc_generator_weights;   //!
       totalEventsWeighted_mc_generator_weights = 0;
       totnorm->SetBranchAddress("totalEventsWeighted_mc_generator_weights",&totalEventsWeighted_mc_generator_weights,&b_totalEventsWeighted_mc_generator_weights);
       for (int ent = 0; ent < totnorm->GetEntries(); ent++){
       totnorm->GetEntry(ent);
       sampleNEvt += totalEventsWeighted_mc_generator_weights->at(6)*totalEventsWeighted_mc_generator_weights->at(167)/totalEventsWeighted_mc_generator_weights->at(0);
       }
       delete totnorm;
       xsecf->Close();
      }
    } else {
      while(filein.good()){
      TString name_root;
      filein>>name_root;
      if(name_root=="") break;
       TFile *xsecf = TFile::Open(name_root);  
       TTree *totnorm = (TTree*)xsecf->Get("sumWeights");
       Float_t totalEventsWeighted;
       totnorm->SetBranchAddress("totalEventsWeighted",&totalEventsWeighted);
                 
       for (int ent = 0; ent < totnorm->GetEntries(); ent++){
       totnorm->GetEntry(ent);
       sampleNEvt += totalEventsWeighted;
       }
       delete totnorm;
       xsecf->Close();
      }
    }
 
    filein.close();
  }
 

  //NEW WAY
  SampleXsection tdp;
  std::string filename = "";
  filename = "../src/Looper/data/NEW_XSection-MC15-13TeV.data"; 
       
  if(!tdp.readFromFile(filename.c_str()))
    {
      cout<<"TopDataPreparation - could not read file"<<endl;
      cout<<"Maybe check it exists, first?"<<endl;
      exit(1);
    } 
  
  sampleXSec = tdp.getXsection(dsid);
  
  weight_factor = (luminosity*sampleXSec)/sampleNEvt;
  
  cout<<" ------------ " << endl;
  cout<<"**** DSID passed = " << dsid << endl;  
  cout << "I'm going to normalize to:  " << sampleNEvt << "   events " << endl;
  
  cout<<"luminosity = "      <<luminosity<< "  pb^-1" << endl;
  cout<<"sampleXSec = "      <<sampleXSec<< "  pb" << endl;
  cout<<"sampleNEvt = "      <<sampleNEvt<<endl;
  
  cout<<"scaling_factor (product) = "<<weight_factor<<endl;
  cout<<endl<<endl<< endl;  

  return weight_factor;
}



TLorentzVector Looper::RecoJet_TL(UInt_t RecoJet_idx){

if(RecoJet_idx>jet_pt->size()-1){
cout<<"=== ERROR IN Looper::RecoJet_TL"<<endl;
cout<<"=== INPUT INDEX GREATER THAN VECTOR SIZE"<<endl;
}

TLorentzVector recojet_TL;
recojet_TL.SetPtEtaPhiE((jet_pt->at(RecoJet_idx)),jet_eta->at(RecoJet_idx),jet_phi->at(RecoJet_idx),(jet_e->at(RecoJet_idx)));

return recojet_TL;


}

TLorentzVector Looper::RecoEle_TL(UInt_t RecoEle_idx){

if(RecoEle_idx>el_pt->size()-1){
cout<<"=== ERROR IN Looper::RecoEle_TL"<<endl;
cout<<"=== INPUT INDEX GREATER THAN VECTOR SIZE"<<endl;
}

TLorentzVector recoele_TL;
recoele_TL.SetPtEtaPhiE((el_pt->at(RecoEle_idx)),el_eta->at(RecoEle_idx),el_phi->at(RecoEle_idx),(el_e->at(RecoEle_idx)));

return recoele_TL;


}

TLorentzVector Looper::RecoMu_TL(UInt_t RecoMu_idx){

if(RecoMu_idx>mu_pt->size()-1){
cout<<"=== ERROR IN Looper::RecoMu_TL"<<endl;
cout<<"=== INPUT INDEX GREATER THAN VECTOR SIZE"<<endl;
}

TLorentzVector recomu_TL;
recomu_TL.SetPtEtaPhiE((mu_pt->at(RecoMu_idx)),mu_eta->at(RecoMu_idx),mu_phi->at(RecoMu_idx),(mu_e->at(RecoMu_idx)));

return recomu_TL;


}

TLorentzVector Looper::Rotated_Lep(TLorentzVector lep){

TRandom3 *r1 = new TRandom3(0);
double rand = r1->Uniform(-PI,PI);
double lep_phi = rand;
lep.SetPhi(lep_phi);

TRandom3 *r2 = new TRandom3(0);
double rand2 = r2->Uniform(14.3,180.);
double lep_theta = rand2;
lep.SetTheta(lep_theta);
while((lep.Pt()/1000.<25)||fabs(lep.Eta())>2.5){
rand2 = r2->Uniform(14.3,180.);
lep_theta = rand2;
lep.SetTheta(lep_theta);
}

delete r1;
delete r2;

return lep;

}


bool Looper::Pass_Goodemu(){

bool emu_signature = false;
if(emu_2015 || emu_2016) emu_signature = true;

vector<int> GoodEle = Pass_GoodEle();
int goodeleidx = GoodEle.at(0);
vector<int> GoodMu = Pass_GoodMu();
int goodmuidx = GoodMu.at(0);

if(el_charge->at(goodeleidx) == mu_charge->at(goodmuidx)) emu_signature = false;
return emu_signature;

}

vector<int> Looper::Pass_GoodJets(){

vector<int> good_jetindex;
for(UInt_t i = 0; i< jet_pt->size(); i++){
   if(jet_pt->at(i)/1000.<30.) continue;
   //if(fabs(jet_eta->at(i))>2.5) continue;
   //if(jet_pt->at(i)/1000.<50. && fabs(jet_eta->at(i))<2.4 && jet_jvt->at(i) < 0.64) continue;
   good_jetindex.push_back(i);
}

return good_jetindex;
}

vector<int> Looper::Pass_GoodEle(){

vector<int> good_eleindex;
for(UInt_t i = 0; i< el_pt->size(); i++){
   //if(el_pt->at(i)/1000.<25.)   continue;
   //if(fabs(el_eta->at(i))>2.47) continue;
   //if(fabs(el_eta->at(i))>1.37 && fabs(el_eta->at(i))<1.52) continue;
   //if(fabs(el_d0sig->at(i))>5.) continue;
   //if(fabs(el_delta_z0_sintheta->at(i))>0.5) continue; 
   good_eleindex.push_back(i);
}

return good_eleindex;
}

vector<int> Looper::Pass_GoodMu(){

vector<int> good_muindex;
for(UInt_t i = 0; i< mu_pt->size(); i++){
   //if(mu_pt->at(i)/1000.<25.)   continue;
   //if(fabs(mu_eta->at(i))>2.4) continue;
   //if(fabs(mu_d0sig->at(i))>3.) continue;
   //if(fabs(mu_delta_z0_sintheta->at(i))>0.5) continue; 
good_muindex.push_back(i);
}

return good_muindex;
}

TTree* Looper::SetTruthTree(int m_sampleType){

  TChain * chain = new TChain("truth","");
  chain->Add(m_inputFileListName+"/truth");

  // Create the hash table that connect
  //    runNumber (x) eventNumber
  // to the tree entry number. A return code less than 0 indicated failure.
  if (chain->BuildIndex("runNumber", "eventNumber") < 0){
    std::cerr << "Could not build particle level index!" << std::endl;
    std::abort();
  }


  if(m_sampleType==1){
  chain->SetBranchAddress("MC_b_from_t_pt", & t_b_pt);
  chain->SetBranchAddress("MC_b_from_t_eta", & t_b_eta);
  chain->SetBranchAddress("MC_b_from_t_phi", & t_b_phi);
  chain->SetBranchAddress("MC_b_from_t_m", & t_b_m);
  chain->SetBranchAddress("MC_b_from_tbar_pt", & t_bbar_pt);
  chain->SetBranchAddress("MC_b_from_tbar_eta", & t_bbar_eta);
  chain->SetBranchAddress("MC_b_from_tbar_phi", & t_bbar_phi);
  chain->SetBranchAddress("MC_b_from_tbar_m", & t_bbar_m);
  }
  if(m_sampleType==14){
  chain->SetBranchAddress("MC_q_from_t_pt", & t_b_pt);
  chain->SetBranchAddress("MC_q_from_t_eta", & t_b_eta);
  chain->SetBranchAddress("MC_q_from_t_phi", & t_b_phi);
  chain->SetBranchAddress("MC_q_from_t_m", & t_b_m);
  chain->SetBranchAddress("MC_q_from_t_pdgId", & t_b_pdgId);
  chain->SetBranchAddress("MC_q_from_tbar_pt", & t_bbar_pt);
  chain->SetBranchAddress("MC_q_from_tbar_eta", & t_bbar_eta);
  chain->SetBranchAddress("MC_q_from_tbar_phi", & t_bbar_phi);
  chain->SetBranchAddress("MC_q_from_tbar_m", & t_bbar_m);
  chain->SetBranchAddress("MC_q_from_tbar_pdgId", & t_bbar_pdgId);
  }

  chain->SetBranchAddress("MC_Wdecay2_from_t_pt", &t_lep_top_pt);
  chain->SetBranchAddress("MC_Wdecay2_from_t_eta", &t_lep_top_eta);
  chain->SetBranchAddress("MC_Wdecay2_from_t_phi", &t_lep_top_phi);
  chain->SetBranchAddress("MC_Wdecay2_from_t_m", &t_lep_top_m);
  chain->SetBranchAddress("MC_Wdecay1_from_tbar_pt", &t_lep_topbar_pt);
  chain->SetBranchAddress("MC_Wdecay1_from_tbar_eta", &t_lep_topbar_eta);
  chain->SetBranchAddress("MC_Wdecay1_from_tbar_phi", &t_lep_topbar_phi);
  chain->SetBranchAddress("MC_Wdecay1_from_tbar_m", &t_lep_topbar_m);
  chain->SetBranchAddress("MC_Wdecay1_from_t_pt", &t_nu_top_pt);
  chain->SetBranchAddress("MC_Wdecay1_from_t_eta", &t_nu_top_eta);
  chain->SetBranchAddress("MC_Wdecay1_from_t_phi", &t_nu_top_phi);
  chain->SetBranchAddress("MC_Wdecay1_from_t_m", &t_nu_top_m);
  chain->SetBranchAddress("MC_Wdecay2_from_tbar_pt", &t_nu_topbar_pt);
  chain->SetBranchAddress("MC_Wdecay2_from_tbar_eta", &t_nu_topbar_eta);
  chain->SetBranchAddress("MC_Wdecay2_from_tbar_phi", &t_nu_topbar_phi);
  chain->SetBranchAddress("MC_Wdecay2_from_tbar_m", &t_nu_topbar_m);
  chain->SetBranchAddress("MC_t_afterFSR_pt", & t_top_pt);
  chain->SetBranchAddress("MC_t_afterFSR_eta", & t_top_eta);
  chain->SetBranchAddress("MC_t_afterFSR_phi", & t_top_phi);
  chain->SetBranchAddress("MC_t_afterFSR_m", & t_top_m);
  chain->SetBranchAddress("MC_tbar_afterFSR_pt", & t_topbar_pt);
  chain->SetBranchAddress("MC_tbar_afterFSR_eta", & t_topbar_eta);
  chain->SetBranchAddress("MC_tbar_afterFSR_phi", & t_topbar_phi);
  chain->SetBranchAddress("MC_tbar_afterFSR_m", & t_topbar_m);

  TTree * tPL;
  tPL = chain;

return tPL;

}


TLorentzVector Looper::Return_PreviousEventJet(vector<int> GoodJets){


TLorentzVector Out_Jet;

//requiring at least 1 jet
Int_t njet = GoodJets.size();
TRandom3 *rand = new TRandom3(0);
Int_t rand_index = rand->Uniform(njet);
UInt_t kk = GoodJets.at(rand_index);
Out_Jet = RecoJet_TL(kk);

delete rand;
return Out_Jet;

}

double Looper::ReturnBJets_ScaleFactor(double jet_pt,  bool isNominal, bool isUp, bool isDown){
    //if(jet_pt>=20 && jet_pt<30)   kb = 1.011	* ( 1 + 0.01 * 1.756);
    //if(jet_pt>=30 && jet_pt<40)   kb = 1.016	* ( 1 + 0.01 * 1.709);
    //if(jet_pt>=40 && jet_pt<50)   kb = 1.002	* ( 1 + 0.01 * 1.886);
    //if(jet_pt>=50 && jet_pt<70)   kb = 1.016	* ( 1 + 0.01 * 2.045);
    //if(jet_pt>=70 && jet_pt<90)   kb = 0.9918	* ( 1 + 0.01 * 3.581);
    //if(jet_pt>=90 && jet_pt<110)  kb = 0.9676	* ( 1 + 0.01 * 5.088);
    //if(jet_pt>=110 && jet_pt<140) kb = 0.9589	* ( 1 + 0.01 * 4.027);
    //if(jet_pt>=140 && jet_pt<170) kb = 0.9798	* ( 1 + 0.01 * 6.889);
    //if(jet_pt>=170 && jet_pt<200) kb = 0.9477	* ( 1 + 0.01 * 5.914);
    //if(jet_pt>=200) 		  kb = 0.9477   * ( 1 + 0.01 * 50.00);

  double kb = 0.;
  if(isNominal){
    if(jet_pt>=20 && jet_pt<30)   kb = 1.011;
    if(jet_pt>=30 && jet_pt<40)   kb = 1.016;
    if(jet_pt>=40 && jet_pt<50)   kb = 1.002;
    if(jet_pt>=50 && jet_pt<70)   kb = 1.018;
    if(jet_pt>=70 && jet_pt<90)   kb = 1.008;
    if(jet_pt>=90 && jet_pt<110)  kb = 0.9561;
    if(jet_pt>=110 && jet_pt<140) kb = 0.965;
    if(jet_pt>=140 && jet_pt<170) kb = 0.9496;
    if(jet_pt>=170 && jet_pt<200) kb = 0.9477;
  } else if(isUp){
    if(jet_pt>=20 && jet_pt<30)   kb = 1.011	* ( 1 + 0.01 * 1.661);
    if(jet_pt>=30 && jet_pt<40)   kb = 1.016	* ( 1 + 0.01 * 1.679);
    if(jet_pt>=40 && jet_pt<50)   kb = 1.002	* ( 1 + 0.01 * 1.679);
    if(jet_pt>=50 && jet_pt<70)   kb = 1.008	* ( 1 + 0.01 * 1.800);
    if(jet_pt>=70 && jet_pt<90)   kb = 1.008	* ( 1 + 0.01 * 2.741);
    if(jet_pt>=90 && jet_pt<110)  kb = 0.9561	* ( 1 + 0.01 * 3.874);
    if(jet_pt>=110 && jet_pt<140) kb = 0.965	* ( 1 + 0.01 * 4.517);
    if(jet_pt>=140 && jet_pt<170) kb = 0.9496	* ( 1 + 0.01 * 4.700);
    if(jet_pt>=170 && jet_pt<200) kb = 0.9477	* ( 1 + 0.01 * 5.056);
  } else if(isDown){
    if(jet_pt>=20 && jet_pt<30)   kb = 1.011	* ( 1 - 0.01 * 1.661);
    if(jet_pt>=30 && jet_pt<40)   kb = 1.016	* ( 1 - 0.01 * 1.679);
    if(jet_pt>=40 && jet_pt<50)   kb = 1.002	* ( 1 - 0.01 * 1.679);
    if(jet_pt>=50 && jet_pt<70)   kb = 1.018	* ( 1 - 0.01 * 1.800);
    if(jet_pt>=70 && jet_pt<90)   kb = 1.008	* ( 1 - 0.01 * 2.741);
    if(jet_pt>=90 && jet_pt<110)  kb = 0.9561	* ( 1 - 0.01 * 3.874);
    if(jet_pt>=110 && jet_pt<140) kb = 0.965	* ( 1 - 0.01 * 4.517);
    if(jet_pt>=140 && jet_pt<170) kb = 0.9496	* ( 1 - 0.01 * 4.700);
    if(jet_pt>=170 && jet_pt<200) kb = 0.9477	* ( 1 - 0.01 * 5.056);
  }
  
  return kb;
}

double Looper::ReturnBJets_EffMC(double jet_pt){

  double effMC = 0.;
    if(jet_pt>=20 && jet_pt<30)  effMC = 0.724296;
    if(jet_pt>=30 && jet_pt<40)  effMC = 0.724296;
    if(jet_pt>=40 && jet_pt<50)  effMC = 0.764526;
    if(jet_pt>=50 && jet_pt<70)  effMC = 0.793632;
    if(jet_pt>=70 && jet_pt<90)  effMC = 0.813079;
    if(jet_pt>=90 && jet_pt<110) effMC = 0.820498;
    if(jet_pt>=110 && jet_pt<140)effMC = 0.824618;
    if(jet_pt>=140 && jet_pt<170)effMC = 0.830858;
    if(jet_pt>=170 && jet_pt<200)effMC = 0.822344;
    if(jet_pt>=200) 		 effMC = 0.794836;
  return effMC;
}

double Looper::ReturnCJets_ScaleFactor(double jet_pt,  bool isNominal, bool isUp, bool isDown){

  double kc = 0.;
  if(isNominal){
    if(jet_pt>=25 && jet_pt<40)   kc = 1.08;
    if(jet_pt>=40 && jet_pt<60)   kc = 1.00;
    if(jet_pt>=60 && jet_pt<140)  kc = 1.00;
    if(jet_pt>=140 && jet_pt<250) kc = 0.80;
  } else if(isUp){
    if(jet_pt>=25 && jet_pt<40)   kc = 1.08+0.17;
    if(jet_pt>=40 && jet_pt<60)   kc = 1.00+0.07;
    if(jet_pt>=60 && jet_pt<140)  kc = 1.00+0.06;
    if(jet_pt>=140 && jet_pt<250) kc = 0.80+0.24;
  } else if(isDown){
    if(jet_pt>=25 && jet_pt<40)   kc = 1.08-0.17;
    if(jet_pt>=40 && jet_pt<60)   kc = 1.00-0.07;
    if(jet_pt>=60 && jet_pt<140)  kc = 1.00-0.06;
    if(jet_pt>=140 && jet_pt<250) kc = 0.80-0.24;
  }
  
  return kc;
}

double Looper::ReturnCJets_EffMC(double jet_pt){

  double effMC = 0.;
    if(jet_pt>=25 && jet_pt<40)   effMC = 0.175000;
    if(jet_pt>=40 && jet_pt<60)   effMC = 0.187633;
    if(jet_pt>=60 && jet_pt<140)  effMC = 0.201963;
    if(jet_pt>=140 && jet_pt<250) effMC = 0.189816;
  return effMC;

}

double Looper::ReturnLJets_ScaleFactor(double jet_pt,  bool isNominal, bool isUp, bool isDown){

  double kl = 0.;
  if(isNominal){
    if(jet_pt>=20 && jet_pt<50)   kl = 1.00;
    if(jet_pt>=50 && jet_pt<100)  kl = 0.91;
    if(jet_pt>=100 && jet_pt<150) kl = 1.10;
    if(jet_pt>=150 && jet_pt<300) kl = 1.30;
  } else if(isUp){
    if(jet_pt>=20 && jet_pt<50)   kl = 1.00+0.35;
    if(jet_pt>=50 && jet_pt<100)  kl = 0.91+0.35;
    if(jet_pt>=100 && jet_pt<150) kl = 1.10+0.35;
    if(jet_pt>=150 && jet_pt<300) kl = 1.30+0.45;
  } else if(isDown){
    if(jet_pt>=20 && jet_pt<50)   kl = 1.00-0.35;
    if(jet_pt>=50 && jet_pt<100)  kl = 0.91-0.35;
    if(jet_pt>=100 && jet_pt<150) kl = 1.10-0.35;
    if(jet_pt>=150 && jet_pt<300) kl = 1.30-0.45;
  }
  
  return kl;
}

double Looper::ReturnLJets_EffMC(double jet_pt){

  double effMC = 0.;
    if(jet_pt>=20 && jet_pt<50)   effMC = 0.0101300;
    if(jet_pt>=50 && jet_pt<100)  effMC = 0.00948758;
    if(jet_pt>=100 && jet_pt<150) effMC = 0.0100253;
    if(jet_pt>=150 && jet_pt<300) effMC = 0.0104382;
  return effMC;

}


double Looper::ReturnFTAG_ScaleFactor(TString TREENAME){

bool isNominal = false;
bool isBUp     = false;
bool isBDown   = false;
bool isCUp     = false;
bool isCDown   = false;
bool isLUp     = false;
bool isLDown   = false;

if(TREENAME == "weight_FTAG_B_UP") 		isBUp 	  = true;
else if(TREENAME == "weight_FTAG_B_DOWN") 	isBDown   = true;
else if(TREENAME == "weight_FTAG_C_UP") 	isCUp     = true;
else if(TREENAME == "weight_FTAG_C_DOWN") 	isCDown   = true;
else if(TREENAME == "weight_FTAG_L_UP") 	isLUp     = true;
else if(TREENAME == "weight_FTAG_L_DOWN") 	isLDown   = true;
else 						isNominal = true;
//if(TREENAME == "nominal") 		isNominal = true;
if(isNominal+isBUp+isBDown+isCUp+isCDown+isLUp+isLDown>1) cerr<<"c'e' un problema con le variazioni ftag"<<endl;

  double Bjets_SF = 1.;
  double Cjets_SF = 1.;
  double Ljets_SF = 1.;
  for(int i = 0; i<jet_pt->size(); i++){
    double Jet_pt = jet_pt->at(i)/1000.;
    if(jet_truthflav->at(i) == 5){
      double kb     = ReturnBJets_ScaleFactor(Jet_pt, isNominal, isBUp, isBDown);
      double effMCb = ReturnBJets_EffMC(Jet_pt);
      double thisjet_weight = 0.;
      if(jet_mv2c10->at(i)>0.64) thisjet_weight = kb;
      else thisjet_weight = (1-effMCb*kb)/(1-effMCb);
      Bjets_SF = Bjets_SF*thisjet_weight;
    } else if(jet_truthflav->at(i) == 4){
      double kc     = ReturnCJets_ScaleFactor(Jet_pt, isNominal, isCUp, isCDown);
      double effMCb = ReturnCJets_EffMC(Jet_pt);
      double thisjet_weight = 0.;
      if(jet_mv2c10->at(i)>0.64) thisjet_weight = kc;
      else thisjet_weight = (1-effMCb*kc)/(1-effMCb);
      Cjets_SF = Cjets_SF*thisjet_weight;
    } else {
      double kl     = ReturnLJets_ScaleFactor(Jet_pt, isNominal, isLUp, isLDown);
      double effMCb = ReturnLJets_EffMC(Jet_pt);
      double thisjet_weight = 0.;
      if(jet_mv2c10->at(i)>0.64) thisjet_weight = kl;
      else thisjet_weight = (1-effMCb*kl)/(1-effMCb);
      Ljets_SF = Ljets_SF*thisjet_weight;
    }

  }
  
  return Bjets_SF*Cjets_SF*Ljets_SF;

}

double Looper::Retrieve_ttbar_nominal_sampleNEvt(bool isMC16a, bool isMC16d, bool isMC16e, TString treename){

double sampleNEvt = 0.;

  ///MC16a
  if(isMC16a)      sampleNEvt =	5.82139e+10;
  if(treename == "RadLo" && isMC16a)      sampleNEvt =	5.10651e+10;
  if(treename == "FSRHi" && isMC16a)      sampleNEvt =	5.82054e+10;
  if(treename == "FSRLo" && isMC16a)      sampleNEvt =	5.8219e+10;
  if(treename == "PDF_90900" && isMC16a)      sampleNEvt =	5.70765e+10;
  if(treename == "PDF_90901" && isMC16a)      sampleNEvt =	5.72121e+10;
  if(treename == "PDF_90902" && isMC16a)      sampleNEvt =	5.65096e+10;
  if(treename == "PDF_90903" && isMC16a)      sampleNEvt =	5.70097e+10;
  if(treename == "PDF_90904" && isMC16a)      sampleNEvt =	5.64086e+10;
  if(treename == "PDF_90905" && isMC16a)      sampleNEvt =	5.71275e+10;
  if(treename == "PDF_90906" && isMC16a)      sampleNEvt =	5.66482e+10;
  if(treename == "PDF_90907" && isMC16a)      sampleNEvt =	5.70646e+10;
  if(treename == "PDF_90908" && isMC16a)      sampleNEvt =	5.70618e+10;
  if(treename == "PDF_90909" && isMC16a)      sampleNEvt =	5.67912e+10;
  if(treename == "PDF_90910" && isMC16a)      sampleNEvt =	5.71528e+10;
  if(treename == "PDF_90911" && isMC16a)      sampleNEvt =	5.66553e+10;
  if(treename == "PDF_90912" && isMC16a)      sampleNEvt =	5.69446e+10;
  if(treename == "PDF_90913" && isMC16a)      sampleNEvt =	5.71589e+10;
  if(treename == "PDF_90914" && isMC16a)      sampleNEvt =	5.69907e+10;
  if(treename == "PDF_90915" && isMC16a)      sampleNEvt =	5.71059e+10;
  if(treename == "PDF_90916" && isMC16a)      sampleNEvt =	5.69272e+10;
  if(treename == "PDF_90917" && isMC16a)      sampleNEvt =	5.74758e+10;
  if(treename == "PDF_90918" && isMC16a)      sampleNEvt =	5.7232e+10;
  if(treename == "PDF_90919" && isMC16a)      sampleNEvt =	5.65948e+10;
  if(treename == "PDF_90920" && isMC16a)      sampleNEvt =	5.703e+10;
  if(treename == "PDF_90921" && isMC16a)      sampleNEvt =	5.70066e+10;
  if(treename == "PDF_90922" && isMC16a)      sampleNEvt =	5.73845e+10;
  if(treename == "PDF_90923" && isMC16a)      sampleNEvt =	5.71226e+10;
  if(treename == "PDF_90924" && isMC16a)      sampleNEvt =	5.70143e+10;
  if(treename == "PDF_90925" && isMC16a)      sampleNEvt =	5.70368e+10;
  if(treename == "PDF_90926" && isMC16a)      sampleNEvt =	5.72304e+10;
  if(treename == "PDF_90927" && isMC16a)      sampleNEvt =	5.71481e+10;
  if(treename == "PDF_90928" && isMC16a)      sampleNEvt =	5.76016e+10;
  if(treename == "PDF_90929" && isMC16a)      sampleNEvt =	5.71117e+10;
  if(treename == "PDF_90930" && isMC16a)      sampleNEvt =	5.70453e+10;
  if(treename == "PDF_260001" && isMC16a)      sampleNEvt =	5.6586e+10;
  if(treename == "PDF_260002" && isMC16a)      sampleNEvt =	5.88089e+10;
  if(treename == "PDF_260003" && isMC16a)      sampleNEvt =	5.88887e+10;
  if(treename == "PDF_260004" && isMC16a)      sampleNEvt =	5.71555e+10;
  if(treename == "PDF_260005" && isMC16a)      sampleNEvt =	5.52993e+10;
  if(treename == "PDF_260006" && isMC16a)      sampleNEvt =	5.87746e+10;
  if(treename == "PDF_260007" && isMC16a)      sampleNEvt =	5.80613e+10;
  if(treename == "PDF_260008" && isMC16a)      sampleNEvt =	5.88249e+10;
  if(treename == "PDF_260009" && isMC16a)      sampleNEvt =	5.8762e+10;
  if(treename == "PDF_260010" && isMC16a)      sampleNEvt =	5.73188e+10;
  if(treename == "PDF_260011" && isMC16a)      sampleNEvt =	5.65584e+10;
  if(treename == "PDF_260012" && isMC16a)      sampleNEvt =	5.86183e+10;
  if(treename == "PDF_260013" && isMC16a)      sampleNEvt =	5.90568e+10;
  if(treename == "PDF_260014" && isMC16a)      sampleNEvt =	5.88368e+10;
  if(treename == "PDF_260015" && isMC16a)      sampleNEvt =	5.92808e+10;
  if(treename == "PDF_260016" && isMC16a)      sampleNEvt =	5.87841e+10;
  if(treename == "PDF_260017" && isMC16a)      sampleNEvt =	6.02349e+10;
  if(treename == "PDF_260018" && isMC16a)      sampleNEvt =	5.91685e+10;
  if(treename == "PDF_260019" && isMC16a)      sampleNEvt =	5.81588e+10;
  if(treename == "PDF_260020" && isMC16a)      sampleNEvt =	5.93337e+10;
  if(treename == "PDF_260021" && isMC16a)      sampleNEvt =	5.78985e+10;
  if(treename == "PDF_260022" && isMC16a)      sampleNEvt =	5.65667e+10;
  if(treename == "PDF_260023" && isMC16a)      sampleNEvt =	5.97345e+10;
  if(treename == "PDF_260024" && isMC16a)      sampleNEvt =	5.69816e+10;
  if(treename == "PDF_260025" && isMC16a)      sampleNEvt =	5.81014e+10;
  if(treename == "PDF_260026" && isMC16a)      sampleNEvt =	5.75382e+10;
  if(treename == "PDF_260027" && isMC16a)      sampleNEvt =	6.04047e+10;
  if(treename == "PDF_260028" && isMC16a)      sampleNEvt =	5.86045e+10;
  if(treename == "PDF_260029" && isMC16a)      sampleNEvt =	5.91521e+10;
  if(treename == "PDF_260030" && isMC16a)      sampleNEvt =	5.85648e+10;
  if(treename == "PDF_260031" && isMC16a)      sampleNEvt =	5.7249e+10;
  if(treename == "PDF_260032" && isMC16a)      sampleNEvt =	5.7119e+10;
  if(treename == "PDF_260033" && isMC16a)      sampleNEvt =	5.75585e+10;
  if(treename == "PDF_260034" && isMC16a)      sampleNEvt =	5.66843e+10;
  if(treename == "PDF_260035" && isMC16a)      sampleNEvt =	6.04698e+10;
  if(treename == "PDF_260036" && isMC16a)      sampleNEvt =	5.83612e+10;
  if(treename == "PDF_260037" && isMC16a)      sampleNEvt =	5.93906e+10;
  if(treename == "PDF_260038" && isMC16a)      sampleNEvt =	5.79966e+10;
  if(treename == "PDF_260039" && isMC16a)      sampleNEvt =	5.70874e+10;
  if(treename == "PDF_260040" && isMC16a)      sampleNEvt =	5.75672e+10;
  if(treename == "PDF_260041" && isMC16a)      sampleNEvt =	5.77098e+10;
  if(treename == "PDF_260042" && isMC16a)      sampleNEvt =	5.78327e+10;
  if(treename == "PDF_260043" && isMC16a)      sampleNEvt =	5.69774e+10;
  if(treename == "PDF_260044" && isMC16a)      sampleNEvt =	5.7612e+10;
  if(treename == "PDF_260045" && isMC16a)      sampleNEvt =	5.76042e+10;
  if(treename == "PDF_260046" && isMC16a)      sampleNEvt =	5.74354e+10;
  if(treename == "PDF_260047" && isMC16a)      sampleNEvt =	5.83975e+10;
  if(treename == "PDF_260048" && isMC16a)      sampleNEvt =	5.84337e+10;
  if(treename == "PDF_260049" && isMC16a)      sampleNEvt =	5.87313e+10;
  if(treename == "PDF_260050" && isMC16a)      sampleNEvt =	5.79654e+10;
  if(treename == "PDF_260051" && isMC16a)      sampleNEvt =	5.76685e+10;
  if(treename == "PDF_260052" && isMC16a)      sampleNEvt =	5.76675e+10;
  if(treename == "PDF_260053" && isMC16a)      sampleNEvt =	5.77114e+10;
  if(treename == "PDF_260054" && isMC16a)      sampleNEvt =	5.86011e+10;
  if(treename == "PDF_260055" && isMC16a)      sampleNEvt =	5.77558e+10;
  if(treename == "PDF_260056" && isMC16a)      sampleNEvt =	5.86866e+10;
  if(treename == "PDF_260057" && isMC16a)      sampleNEvt =	5.88054e+10;
  if(treename == "PDF_260058" && isMC16a)      sampleNEvt =	5.76947e+10;
  if(treename == "PDF_260059" && isMC16a)      sampleNEvt =	5.90358e+10;
  if(treename == "PDF_260060" && isMC16a)      sampleNEvt =	5.77921e+10;
  if(treename == "PDF_260061" && isMC16a)      sampleNEvt =	5.8122e+10;
  if(treename == "PDF_260062" && isMC16a)      sampleNEvt =	5.81597e+10;
  if(treename == "PDF_260063" && isMC16a)      sampleNEvt =	5.81556e+10;
  if(treename == "PDF_260064" && isMC16a)      sampleNEvt =	5.88145e+10;
  if(treename == "PDF_260065" && isMC16a)      sampleNEvt =	5.88055e+10;
  if(treename == "PDF_260066" && isMC16a)      sampleNEvt =	5.76762e+10;
  if(treename == "PDF_260067" && isMC16a)      sampleNEvt =	6.06189e+10;
  if(treename == "PDF_260068" && isMC16a)      sampleNEvt =	5.81141e+10;
  if(treename == "PDF_260069" && isMC16a)      sampleNEvt =	5.77433e+10;
  if(treename == "PDF_260070" && isMC16a)      sampleNEvt =	5.80331e+10;
  if(treename == "PDF_260071" && isMC16a)      sampleNEvt =	5.71551e+10;
  if(treename == "PDF_260072" && isMC16a)      sampleNEvt =	5.79647e+10;
  if(treename == "PDF_260073" && isMC16a)      sampleNEvt =	5.72045e+10;
  if(treename == "PDF_260074" && isMC16a)      sampleNEvt =	5.87996e+10;
  if(treename == "PDF_260075" && isMC16a)      sampleNEvt =	5.82232e+10;
  if(treename == "PDF_260076" && isMC16a)      sampleNEvt =	5.59095e+10;
  if(treename == "PDF_260077" && isMC16a)      sampleNEvt =	5.7049e+10;
  if(treename == "PDF_260078" && isMC16a)      sampleNEvt =	5.92331e+10;
  if(treename == "PDF_260079" && isMC16a)      sampleNEvt =	5.78819e+10;
  if(treename == "PDF_260080" && isMC16a)      sampleNEvt =	5.78476e+10;
  if(treename == "PDF_260081" && isMC16a)      sampleNEvt =	5.77467e+10;
  if(treename == "PDF_260082" && isMC16a)      sampleNEvt =	5.73934e+10;
  if(treename == "PDF_260083" && isMC16a)      sampleNEvt =	5.94998e+10;
  if(treename == "PDF_260084" && isMC16a)      sampleNEvt =	5.81476e+10;
  if(treename == "PDF_260085" && isMC16a)      sampleNEvt =	5.82553e+10;
  if(treename == "PDF_260086" && isMC16a)      sampleNEvt =	5.83813e+10;
  if(treename == "PDF_260087" && isMC16a)      sampleNEvt =	5.89412e+10;
  if(treename == "PDF_260088" && isMC16a)      sampleNEvt =	5.88652e+10;
  if(treename == "PDF_260089" && isMC16a)      sampleNEvt =	5.96714e+10;
  if(treename == "PDF_260090" && isMC16a)      sampleNEvt =	5.7677e+10;
  if(treename == "PDF_260091" && isMC16a)      sampleNEvt =	5.79206e+10;
  if(treename == "PDF_260092" && isMC16a)      sampleNEvt =	5.77884e+10;
  if(treename == "PDF_260093" && isMC16a)      sampleNEvt =	5.89028e+10;
  if(treename == "PDF_260094" && isMC16a)      sampleNEvt =	5.8655e+10;
  if(treename == "PDF_260095" && isMC16a)      sampleNEvt =	5.92975e+10;
  if(treename == "PDF_260096" && isMC16a)      sampleNEvt =	5.86015e+10;
  if(treename == "PDF_260097" && isMC16a)      sampleNEvt =	5.84281e+10;
  if(treename == "PDF_260098" && isMC16a)      sampleNEvt =	5.79396e+10;
  if(treename == "PDF_260099" && isMC16a)      sampleNEvt =	5.94935e+10;
  if(treename == "PDF_260100" && isMC16a)      sampleNEvt =	5.97643e+10;
  ///MC16d 
  if(isMC16d) sampleNEvt = 7.2651e+10;
  if(treename == "RadLo" && isMC16d)  	  sampleNEvt =	6.3732e+10;
  if(treename == "FSRHi" && isMC16d)      sampleNEvt =	7.26521e+10;
  if(treename == "FSRLo" && isMC16d)      sampleNEvt =	7.26543e+10;
  if(treename == "PDF_90900" && isMC16d)  sampleNEvt =	7.12323e+10;
  if(treename == "PDF_90901" && isMC16d)  sampleNEvt =	7.14015e+10;
  if(treename == "PDF_90902" && isMC16d)  sampleNEvt =	7.05247e+10;
  if(treename == "PDF_90903" && isMC16d)  sampleNEvt =	7.11488e+10;
  if(treename == "PDF_90904" && isMC16d)  sampleNEvt =	7.03991e+10;
  if(treename == "PDF_90905" && isMC16d)  sampleNEvt =	7.1296e+10;
  if(treename == "PDF_90906" && isMC16d)  sampleNEvt =	7.06976e+10;
  if(treename == "PDF_90907" && isMC16d)  sampleNEvt =	7.12173e+10;
  if(treename == "PDF_90908" && isMC16d)  sampleNEvt =	7.1214e+10;
  if(treename == "PDF_90909" && isMC16d)  sampleNEvt =	7.08762e+10;
  if(treename == "PDF_90910" && isMC16d)  sampleNEvt =	7.13274e+10;
  if(treename == "PDF_90911" && isMC16d)  sampleNEvt =	7.07064e+10;
  if(treename == "PDF_90912" && isMC16d)  sampleNEvt =	7.10675e+10;
  if(treename == "PDF_90913" && isMC16d)  sampleNEvt =	7.13352e+10;
  if(treename == "PDF_90914" && isMC16d)  sampleNEvt =	7.11252e+10;
  if(treename == "PDF_90915" && isMC16d)  sampleNEvt =	7.1269e+10;
  if(treename == "PDF_90916" && isMC16d)  sampleNEvt =	7.10461e+10;
  if(treename == "PDF_90917" && isMC16d)  sampleNEvt =	7.17306e+10;
  if(treename == "PDF_90918" && isMC16d)  sampleNEvt =	7.14262e+10;
  if(treename == "PDF_90919" && isMC16d)  sampleNEvt =	7.0631e+10;
  if(treename == "PDF_90920" && isMC16d)  sampleNEvt =	7.11741e+10;
  if(treename == "PDF_90921" && isMC16d)  sampleNEvt =	7.11449e+10;
  if(treename == "PDF_90922" && isMC16d)  sampleNEvt =	7.16167e+10;
  if(treename == "PDF_90923" && isMC16d)  sampleNEvt =	7.12897e+10;
  if(treename == "PDF_90924" && isMC16d)  sampleNEvt =	7.11547e+10;
  if(treename == "PDF_90925" && isMC16d)  sampleNEvt =	7.11828e+10;
  if(treename == "PDF_90926" && isMC16d)  sampleNEvt =	7.14243e+10;
  if(treename == "PDF_90927" && isMC16d)  sampleNEvt =	7.13216e+10;
  if(treename == "PDF_90928" && isMC16d)  sampleNEvt =	7.18876e+10;
  if(treename == "PDF_90929" && isMC16d)  sampleNEvt =	7.1276e+10;
  if(treename == "PDF_90930" && isMC16d)  sampleNEvt =	7.11934e+10;
  if(treename == "PDF_260001" && isMC16d)  sampleNEvt =	7.06212e+10;
  if(treename == "PDF_260002" && isMC16d)  sampleNEvt =	7.33966e+10;
  if(treename == "PDF_260003" && isMC16d)  sampleNEvt =	7.34949e+10;
  if(treename == "PDF_260004" && isMC16d)  sampleNEvt =	7.1332e+10;
  if(treename == "PDF_260005" && isMC16d)  sampleNEvt =	6.90129e+10;
  if(treename == "PDF_260006" && isMC16d)  sampleNEvt =	7.33522e+10;
  if(treename == "PDF_260007" && isMC16d)  sampleNEvt =	7.24606e+10;
  if(treename == "PDF_260008" && isMC16d)  sampleNEvt =	7.34137e+10;
  if(treename == "PDF_260009" && isMC16d)  sampleNEvt =	7.33344e+10;
  if(treename == "PDF_260010" && isMC16d)  sampleNEvt =	7.15346e+10;
  if(treename == "PDF_260011" && isMC16d)  sampleNEvt =	7.05878e+10;
  if(treename == "PDF_260012" && isMC16d)  sampleNEvt =	7.31595e+10;
  if(treename == "PDF_260013" && isMC16d)  sampleNEvt =	7.37087e+10;
  if(treename == "PDF_260014" && isMC16d)  sampleNEvt =	7.34302e+10;
  if(treename == "PDF_260015" && isMC16d)  sampleNEvt =	7.39872e+10;
  if(treename == "PDF_260016" && isMC16d)  sampleNEvt =	7.3363e+10;
  if(treename == "PDF_260017" && isMC16d)  sampleNEvt =	7.5178e+10;
  if(treename == "PDF_260018" && isMC16d)  sampleNEvt =	7.38447e+10;
  if(treename == "PDF_260019" && isMC16d)  sampleNEvt =	7.25818e+10;
  if(treename == "PDF_260020" && isMC16d)  sampleNEvt =	7.40638e+10;
  if(treename == "PDF_260021" && isMC16d)  sampleNEvt =	7.22592e+10;
  if(treename == "PDF_260022" && isMC16d)  sampleNEvt =	7.05949e+10;
  if(treename == "PDF_260023" && isMC16d)  sampleNEvt =	7.45506e+10;
  if(treename == "PDF_260024" && isMC16d)  sampleNEvt =	7.11128e+10;
  if(treename == "PDF_260025" && isMC16d)  sampleNEvt =	7.25115e+10;
  if(treename == "PDF_260026" && isMC16d)  sampleNEvt =	7.18097e+10;
  if(treename == "PDF_260027" && isMC16d)  sampleNEvt =	7.53894e+10;
  if(treename == "PDF_260028" && isMC16d)  sampleNEvt =	7.31451e+10;
  if(treename == "PDF_260029" && isMC16d)  sampleNEvt =	7.38237e+10;
  if(treename == "PDF_260030" && isMC16d)  sampleNEvt =	7.30894e+10;
  if(treename == "PDF_260031" && isMC16d)  sampleNEvt =	7.1447e+10;
  if(treename == "PDF_260032" && isMC16d)  sampleNEvt =	7.1283e+10;
  if(treename == "PDF_260033" && isMC16d)  sampleNEvt =	7.18339e+10;
  if(treename == "PDF_260034" && isMC16d)  sampleNEvt =	7.074e+10;
  if(treename == "PDF_260035" && isMC16d)  sampleNEvt =	7.54694e+10;
  if(treename == "PDF_260036" && isMC16d)  sampleNEvt =	7.28366e+10;
  if(treename == "PDF_260037" && isMC16d)  sampleNEvt =	7.412e+10;
  if(treename == "PDF_260038" && isMC16d)  sampleNEvt =	7.23794e+10;
  if(treename == "PDF_260039" && isMC16d)  sampleNEvt =	7.12444e+10;
  if(treename == "PDF_260040" && isMC16d)  sampleNEvt =	7.18439e+10;
  if(treename == "PDF_260041" && isMC16d)  sampleNEvt =	7.20205e+10;
  if(treename == "PDF_260042" && isMC16d)  sampleNEvt =	7.21762e+10;
  if(treename == "PDF_260043" && isMC16d)  sampleNEvt =	7.11074e+10;
  if(treename == "PDF_260044" && isMC16d)  sampleNEvt =	7.18999e+10;
  if(treename == "PDF_260045" && isMC16d)  sampleNEvt =	7.18876e+10;
  if(treename == "PDF_260046" && isMC16d)  sampleNEvt =	7.16786e+10;
  if(treename == "PDF_260047" && isMC16d)  sampleNEvt =	7.28837e+10;
  if(treename == "PDF_260048" && isMC16d)  sampleNEvt =	7.29277e+10;
  if(treename == "PDF_260049" && isMC16d)  sampleNEvt =	7.32966e+10;
  if(treename == "PDF_260050" && isMC16d)  sampleNEvt =	7.2344e+10;
  if(treename == "PDF_260051" && isMC16d)  sampleNEvt =	7.19717e+10;
  if(treename == "PDF_260052" && isMC16d)  sampleNEvt =	7.19724e+10;
  if(treename == "PDF_260053" && isMC16d)  sampleNEvt =	7.20238e+10;
  if(treename == "PDF_260054" && isMC16d)  sampleNEvt =	7.31396e+10;
  if(treename == "PDF_260055" && isMC16d)  sampleNEvt =	7.20817e+10;
  if(treename == "PDF_260056" && isMC16d)  sampleNEvt =	7.32449e+10;
  if(treename == "PDF_260057" && isMC16d)  sampleNEvt =	7.33884e+10;
  if(treename == "PDF_260058" && isMC16d)  sampleNEvt =	7.2006e+10;
  if(treename == "PDF_260059" && isMC16d)  sampleNEvt =	7.3676e+10;
  if(treename == "PDF_260060" && isMC16d)  sampleNEvt =	7.21242e+10;
  if(treename == "PDF_260061" && isMC16d)  sampleNEvt =	7.25373e+10;
  if(treename == "PDF_260062" && isMC16d)  sampleNEvt =	7.25828e+10;
  if(treename == "PDF_260063" && isMC16d)  sampleNEvt =	7.25821e+10;
  if(treename == "PDF_260064" && isMC16d)  sampleNEvt =	7.34049e+10;
  if(treename == "PDF_260065" && isMC16d)  sampleNEvt =	7.33918e+10;
  if(treename == "PDF_260066" && isMC16d)  sampleNEvt =	7.19835e+10;
  if(treename == "PDF_260067" && isMC16d)  sampleNEvt =	7.56539e+10;
  if(treename == "PDF_260068" && isMC16d)  sampleNEvt =	7.25293e+10;
  if(treename == "PDF_260069" && isMC16d)  sampleNEvt =	7.20643e+10;
  if(treename == "PDF_260070" && isMC16d)  sampleNEvt =	7.24309e+10;
  if(treename == "PDF_260071" && isMC16d)  sampleNEvt =	7.13313e+10;
  if(treename == "PDF_260072" && isMC16d)  sampleNEvt =	7.23404e+10;
  if(treename == "PDF_260073" && isMC16d)  sampleNEvt =	7.13925e+10;
  if(treename == "PDF_260074" && isMC16d)  sampleNEvt =	7.33883e+10;
  if(treename == "PDF_260075" && isMC16d)  sampleNEvt =	7.26625e+10;
  if(treename == "PDF_260076" && isMC16d)  sampleNEvt =	6.9777e+10;
  if(treename == "PDF_260077" && isMC16d)  sampleNEvt =	7.11973e+10;
  if(treename == "PDF_260078" && isMC16d)  sampleNEvt =	7.39221e+10;
  if(treename == "PDF_260079" && isMC16d)  sampleNEvt =	7.22367e+10;
  if(treename == "PDF_260080" && isMC16d)  sampleNEvt =	7.21915e+10;
  if(treename == "PDF_260081" && isMC16d)  sampleNEvt =	7.20697e+10;
  if(treename == "PDF_260082" && isMC16d)  sampleNEvt =	7.1628e+10;
  if(treename == "PDF_260083" && isMC16d)  sampleNEvt =	7.42577e+10;
  if(treename == "PDF_260084" && isMC16d)  sampleNEvt =	7.25653e+10;
  if(treename == "PDF_260085" && isMC16d)  sampleNEvt =	7.27047e+10;
  if(treename == "PDF_260086" && isMC16d)  sampleNEvt =	7.28613e+10;
  if(treename == "PDF_260087" && isMC16d)  sampleNEvt =	7.35588e+10;
  if(treename == "PDF_260088" && isMC16d)  sampleNEvt =	7.3467e+10;
  if(treename == "PDF_260089" && isMC16d)  sampleNEvt =	7.4474e+10;
  if(treename == "PDF_260090" && isMC16d)  sampleNEvt =	7.19859e+10;
  if(treename == "PDF_260091" && isMC16d)  sampleNEvt =	7.22906e+10;
  if(treename == "PDF_260092" && isMC16d)  sampleNEvt =	7.21214e+10;
  if(treename == "PDF_260093" && isMC16d)  sampleNEvt =	7.35126e+10;
  if(treename == "PDF_260094" && isMC16d)  sampleNEvt =	7.32019e+10;
  if(treename == "PDF_260095" && isMC16d)  sampleNEvt =	7.40063e+10;
  if(treename == "PDF_260096" && isMC16d)  sampleNEvt =	7.3136e+10;
  if(treename == "PDF_260097" && isMC16d)  sampleNEvt =	7.29163e+10;
  if(treename == "PDF_260098" && isMC16d)  sampleNEvt =	7.23094e+10;
  if(treename == "PDF_260099" && isMC16d)  sampleNEvt =	7.42534e+10;
  if(treename == "PDF_260100" && isMC16d)  sampleNEvt =	7.45867e+10;
  //MC16e
  if(isMC16e)  sampleNEvt =1.01641e+11;
  if(treename == "RadLo" && isMC16e)  sampleNEvt =	8.91629e+10;
  if(treename == "FSRHi" && isMC16e)  sampleNEvt =	1.01629e+11;
  if(treename == "FSRLo" && isMC16e)  sampleNEvt =	1.0162e+11;
  if(treename == "PDF_90900" && isMC16e)  sampleNEvt =	9.96542e+10;
  if(treename == "PDF_90901" && isMC16e)  sampleNEvt =	9.98911e+10;
  if(treename == "PDF_90902" && isMC16e)  sampleNEvt =	9.86642e+10;
  if(treename == "PDF_90903" && isMC16e)  sampleNEvt =	9.95375e+10;
  if(treename == "PDF_90904" && isMC16e)  sampleNEvt =	9.84882e+10;
  if(treename == "PDF_90905" && isMC16e)  sampleNEvt =	9.97438e+10;
  if(treename == "PDF_90906" && isMC16e)  sampleNEvt =	9.89065e+10;
  if(treename == "PDF_90907" && isMC16e)  sampleNEvt =	9.96334e+10;
  if(treename == "PDF_90908" && isMC16e)  sampleNEvt =	9.96292e+10;
  if(treename == "PDF_90909" && isMC16e)  sampleNEvt =	9.91562e+10;
  if(treename == "PDF_90910" && isMC16e)  sampleNEvt =	9.97876e+10;
  if(treename == "PDF_90911" && isMC16e)  sampleNEvt =	9.89187e+10;
  if(treename == "PDF_90912" && isMC16e)  sampleNEvt =	9.9424e+10;
  if(treename == "PDF_90913" && isMC16e)  sampleNEvt =	9.97982e+10;
  if(treename == "PDF_90914" && isMC16e)  sampleNEvt =	9.95044e+10;
  if(treename == "PDF_90915" && isMC16e)  sampleNEvt =	9.97056e+10;
  if(treename == "PDF_90916" && isMC16e)  sampleNEvt =	9.93934e+10;
  if(treename == "PDF_90917" && isMC16e)  sampleNEvt =	1.00351e+11;
  if(treename == "PDF_90918" && isMC16e)  sampleNEvt =	9.99256e+10;
  if(treename == "PDF_90919" && isMC16e)  sampleNEvt =	9.88135e+10;
  if(treename == "PDF_90920" && isMC16e)  sampleNEvt =	9.9573e+10;
  if(treename == "PDF_90921" && isMC16e)  sampleNEvt =	9.95321e+10;
  if(treename == "PDF_90922" && isMC16e)  sampleNEvt =	1.00192e+11;
  if(treename == "PDF_90923" && isMC16e)  sampleNEvt =	9.97346e+10;
  if(treename == "PDF_90924" && isMC16e)  sampleNEvt =	9.95457e+10;
  if(treename == "PDF_90925" && isMC16e)  sampleNEvt =	9.95851e+10;
  if(treename == "PDF_90926" && isMC16e)  sampleNEvt =	9.99229e+10;
  if(treename == "PDF_90927" && isMC16e)  sampleNEvt =	9.97792e+10;
  if(treename == "PDF_90928" && isMC16e)  sampleNEvt =	1.00571e+11;
  if(treename == "PDF_90929" && isMC16e)  sampleNEvt =	9.97155e+10;
  if(treename == "PDF_90930" && isMC16e)  sampleNEvt =	9.95998e+10;
  if(treename == "PDF_260001" && isMC16e)  sampleNEvt =	9.87985e+10;
  if(treename == "PDF_260002" && isMC16e)  sampleNEvt =	1.0268e+11;
  if(treename == "PDF_260003" && isMC16e)  sampleNEvt =	1.0282e+11;
  if(treename == "PDF_260004" && isMC16e)  sampleNEvt =	9.97928e+10;
  if(treename == "PDF_260005" && isMC16e)  sampleNEvt =	9.65499e+10;
  if(treename == "PDF_260006" && isMC16e)  sampleNEvt =	1.02621e+11;
  if(treename == "PDF_260007" && isMC16e)  sampleNEvt =	1.01374e+11;
  if(treename == "PDF_260008" && isMC16e)  sampleNEvt =	1.0271e+11;
  if(treename == "PDF_260009" && isMC16e)  sampleNEvt =	1.02599e+11;
  if(treename == "PDF_260010" && isMC16e)  sampleNEvt =	1.00078e+11;
  if(treename == "PDF_260011" && isMC16e)  sampleNEvt =	9.87517e+10;
  if(treename == "PDF_260012" && isMC16e)  sampleNEvt =	1.02351e+11;
  if(treename == "PDF_260013" && isMC16e)  sampleNEvt =	1.03116e+11;
  if(treename == "PDF_260014" && isMC16e)  sampleNEvt =	1.02727e+11;
  if(treename == "PDF_260015" && isMC16e)  sampleNEvt =	1.03503e+11;
  if(treename == "PDF_260016" && isMC16e)  sampleNEvt =	1.02638e+11;
  if(treename == "PDF_260017" && isMC16e)  sampleNEvt =	1.0517e+11;
  if(treename == "PDF_260018" && isMC16e)  sampleNEvt =	1.03306e+11;
  if(treename == "PDF_260019" && isMC16e)  sampleNEvt =	1.01545e+11;
  if(treename == "PDF_260020" && isMC16e)  sampleNEvt =	1.03602e+11;
  if(treename == "PDF_260021" && isMC16e)  sampleNEvt =	1.01093e+11;
  if(treename == "PDF_260022" && isMC16e)  sampleNEvt =	9.87645e+10;
  if(treename == "PDF_260023" && isMC16e)  sampleNEvt =	1.04297e+11;
  if(treename == "PDF_260024" && isMC16e)  sampleNEvt =	9.94894e+10;
  if(treename == "PDF_260025" && isMC16e)  sampleNEvt =	1.01445e+11;
  if(treename == "PDF_260026" && isMC16e)  sampleNEvt =	1.00462e+11;
  if(treename == "PDF_260027" && isMC16e)  sampleNEvt =	1.05466e+11;
  if(treename == "PDF_260028" && isMC16e)  sampleNEvt =	1.02323e+11;
  if(treename == "PDF_260029" && isMC16e)  sampleNEvt =	1.03281e+11;
  if(treename == "PDF_260030" && isMC16e)  sampleNEvt =	1.02255e+11;
  if(treename == "PDF_260031" && isMC16e)  sampleNEvt =	9.99579e+10;
  if(treename == "PDF_260032" && isMC16e)  sampleNEvt =	9.9729e+10;
  if(treename == "PDF_260033" && isMC16e)  sampleNEvt =	1.00496e+11;
  if(treename == "PDF_260034" && isMC16e)  sampleNEvt =	9.8974e+10;
  if(treename == "PDF_260035" && isMC16e)  sampleNEvt =	1.0558e+11;
  if(treename == "PDF_260036" && isMC16e)  sampleNEvt =	1.01899e+11;
  if(treename == "PDF_260037" && isMC16e)  sampleNEvt =	1.03697e+11;
  if(treename == "PDF_260038" && isMC16e)  sampleNEvt =	1.01262e+11;
  if(treename == "PDF_260039" && isMC16e)  sampleNEvt =	9.96729e+10;
  if(treename == "PDF_260040" && isMC16e)  sampleNEvt =	1.00511e+11;
  if(treename == "PDF_260041" && isMC16e)  sampleNEvt =	1.0076e+11;
  if(treename == "PDF_260042" && isMC16e)  sampleNEvt =	1.00977e+11;
  if(treename == "PDF_260043" && isMC16e)  sampleNEvt =	9.94823e+10;
  if(treename == "PDF_260044" && isMC16e)  sampleNEvt =	1.0059e+11;
  if(treename == "PDF_260045" && isMC16e)  sampleNEvt =	1.00576e+11;
  if(treename == "PDF_260046" && isMC16e)  sampleNEvt =	1.00282e+11;
  if(treename == "PDF_260047" && isMC16e)  sampleNEvt =	1.01962e+11;
  if(treename == "PDF_260048" && isMC16e)  sampleNEvt =	1.02025e+11;
  if(treename == "PDF_260049" && isMC16e)  sampleNEvt =	1.02544e+11;
  if(treename == "PDF_260050" && isMC16e)  sampleNEvt =	1.01211e+11;
  if(treename == "PDF_260051" && isMC16e)  sampleNEvt =	1.00691e+11;
  if(treename == "PDF_260052" && isMC16e)  sampleNEvt =	1.00689e+11;
  if(treename == "PDF_260053" && isMC16e)  sampleNEvt =	1.00764e+11;
  if(treename == "PDF_260054" && isMC16e)  sampleNEvt =	1.02318e+11;
  if(treename == "PDF_260055" && isMC16e)  sampleNEvt =	1.00843e+11;
  if(treename == "PDF_260056" && isMC16e)  sampleNEvt =	1.02465e+11;
  if(treename == "PDF_260057" && isMC16e)  sampleNEvt =	1.02674e+11;
  if(treename == "PDF_260058" && isMC16e)  sampleNEvt =	1.00738e+11;
  if(treename == "PDF_260059" && isMC16e)  sampleNEvt =	1.03075e+11;
  if(treename == "PDF_260060" && isMC16e)  sampleNEvt =	1.00904e+11;
  if(treename == "PDF_260061" && isMC16e)  sampleNEvt =	1.0148e+11;
  if(treename == "PDF_260062" && isMC16e)  sampleNEvt =	1.01548e+11;
  if(treename == "PDF_260063" && isMC16e)  sampleNEvt =	1.01542e+11;
  if(treename == "PDF_260064" && isMC16e)  sampleNEvt =	1.02688e+11;
  if(treename == "PDF_260065" && isMC16e)  sampleNEvt =	1.02676e+11;
  if(treename == "PDF_260066" && isMC16e)  sampleNEvt =	1.00703e+11;
  if(treename == "PDF_260067" && isMC16e)  sampleNEvt =	1.05842e+11;
  if(treename == "PDF_260068" && isMC16e)  sampleNEvt =	1.01468e+11;
  if(treename == "PDF_260069" && isMC16e)  sampleNEvt =	1.0082e+11;
  if(treename == "PDF_260070" && isMC16e)  sampleNEvt =	1.01327e+11;
  if(treename == "PDF_260071" && isMC16e)  sampleNEvt =	9.97946e+10;
  if(treename == "PDF_260072" && isMC16e)  sampleNEvt =	1.01206e+11;
  if(treename == "PDF_260073" && isMC16e)  sampleNEvt =	9.98776e+10;
  if(treename == "PDF_260074" && isMC16e)  sampleNEvt =	1.02665e+11;
  if(treename == "PDF_260075" && isMC16e)  sampleNEvt =	1.01656e+11;
  if(treename == "PDF_260076" && isMC16e)  sampleNEvt =	9.76214e+10;
  if(treename == "PDF_260077" && isMC16e)  sampleNEvt =	9.96073e+10;
  if(treename == "PDF_260078" && isMC16e)  sampleNEvt =	1.03421e+11;
  if(treename == "PDF_260079" && isMC16e)  sampleNEvt =	1.01062e+11;
  if(treename == "PDF_260080" && isMC16e)  sampleNEvt =	1.01001e+11;
  if(treename == "PDF_260081" && isMC16e)  sampleNEvt =	1.00826e+11;
  if(treename == "PDF_260082" && isMC16e)  sampleNEvt =	1.00208e+11;
  if(treename == "PDF_260083" && isMC16e)  sampleNEvt =	1.03893e+11;
  if(treename == "PDF_260084" && isMC16e)  sampleNEvt =	1.01525e+11;
  if(treename == "PDF_260085" && isMC16e)  sampleNEvt =	1.01712e+11;
  if(treename == "PDF_260086" && isMC16e)  sampleNEvt =	1.01933e+11;
  if(treename == "PDF_260087" && isMC16e)  sampleNEvt =	1.0291e+11;
  if(treename == "PDF_260088" && isMC16e)  sampleNEvt =	1.02778e+11;
  if(treename == "PDF_260089" && isMC16e)  sampleNEvt =	1.04187e+11;
  if(treename == "PDF_260090" && isMC16e)  sampleNEvt =	1.00704e+11;
  if(treename == "PDF_260091" && isMC16e)  sampleNEvt =	1.01131e+11;
  if(treename == "PDF_260092" && isMC16e)  sampleNEvt =	1.00899e+11;
  if(treename == "PDF_260093" && isMC16e)  sampleNEvt =	1.02843e+11;
  if(treename == "PDF_260094" && isMC16e)  sampleNEvt =	1.02414e+11;
  if(treename == "PDF_260095" && isMC16e)  sampleNEvt =	1.03531e+11;
  if(treename == "PDF_260096" && isMC16e)  sampleNEvt =	1.02318e+11;
  if(treename == "PDF_260097" && isMC16e)  sampleNEvt =	1.02014e+11;
  if(treename == "PDF_260098" && isMC16e)  sampleNEvt =	1.01163e+11;
  if(treename == "PDF_260099" && isMC16e)  sampleNEvt =	1.03878e+11;
  if(treename == "PDF_260100" && isMC16e)  sampleNEvt =	1.04347e+11;

return sampleNEvt;
}

