#include "Looper/Looper.h"
#include<iostream>
#include<fstream>
using namespace std;

int main(int argc, char **argv)
{
  for (int i = 1; i<argc; i++) printf("Runtime commandline argument %d:%s\n",i,argv[i]);
  string str1 = argv[1];
  int sample = stoi(str1);
  std::cerr<<"--- Sample number: "<<sample<<endl;

  char * inputFileList = argv[2];
  string inputFileListName = string(inputFileList);
  cout<<"--- Input file list: "<<inputFileListName<<endl; 

  string isMC16achar = argv[3];
  int isMC16a = stoi(isMC16achar);
  cout<<"--- isMC16a: "<<isMC16a<<endl; 

  string isMC16dchar = argv[4];
  int isMC16d = stoi(isMC16dchar);
  cout<<"--- isMC16d: "<<isMC16d<<endl; 

  string isMC16echar = argv[5];
  int isMC16e = stoi(isMC16echar);
  cout<<"--- isMC16e: "<<isMC16e<<endl; 

  char * treechar = argv[6];
  string TreeName = string(treechar);
  cout<<"--- Tree name: "<<TreeName<<endl; 

  char * regionchar = argv[7];
  string RegionName = string(regionchar);
  cout<<"--- Region name: "<<RegionName<<endl; 
  TString which_region = RegionName;

  char * srorfakeschar = argv[8];
  string SRorFakesName = string(srorfakeschar);
  cout<<"--- OS or SS name: "<<SRorFakesName<<endl; 
  TString which_region_os_ss = srorfakeschar;

  int index_name = 0;
  std::ifstream filein(inputFileListName);
  if(!filein) { cout << "Error opening input file "<<inputFileListName<<endl;
  } else {
    while(filein.good()){
    TString name_root;
    filein>>name_root;
    cerr<<"--- Running on name_root:	"<<name_root<<endl;
    if(name_root=="") break;
    Looper LOOPER(sample, index_name , name_root, TreeName, isMC16a, isMC16d, isMC16e);
    LOOPER.Loop(which_region, which_region_os_ss);
    if(TreeName=="nominal") LOOPER.~Looper(); //THIS ALREADY HAPPENS SOMEWHERE
    index_name++;
    }
  }
  filein.close();
}
